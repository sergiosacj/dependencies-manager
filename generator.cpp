#include <bits/stdc++.h>
#define MAX_PACKAGES 1000
#define QTD_OF_INSTALLED_PACKAGES 10

using namespace std;

struct DSU{
	vector<int> p,sz;
	int N;
	
	DSU (int n){
		N=n;
		p.resize(N+1);
		sz.resize(N+1);
		for(int i=0;i<=N;++i){
      p[i]=i;
      sz[i]=1;
		}
	}
	
	int find_set(int x){
		return p[x] = (p[x] == x ? x : find_set(p[x]));
	}

  bool is_same_set(int a, int b){
    return find_set(a) == find_set(b);
  }
		
	void union_set(int a,int b){
		a=find_set(a);
		b=find_set(b);
		if(a==b)
			return;
		if(sz[a]>sz[b])swap(a,b);
		p[a]=b;
		sz[b]+=sz[a];
	}

  
};

int main() {
  
  srand((unsigned) time(NULL));

  int qtd_of_packages = 1 + rand() % (MAX_PACKAGES);

  int qtd_of_depedencies = 1 + rand() % (qtd_of_packages - 1);

  DSU dsu(qtd_of_packages);

  vector<pair<int,int>> dependencies;

  for(int i=0;i<qtd_of_depedencies;++i){
    int x = 1 + rand() % (qtd_of_packages);
    while(1) {
      int y = 1 + rand() % (qtd_of_packages);
      if(dsu.is_same_set(x,y)) continue;
      dsu.union_set(x,y);
      dependencies.push_back({x,y});
      break;
    }
  }

  vector<int> packages(qtd_of_packages+1);
  iota(packages.begin(),packages.end(), 1);
  random_shuffle(packages.begin(),packages.end());

  cout << "(define (problem install-debian-packages)" << endl;
  cout << "(:domain dependencies-manager)" << endl;
  cout << "(:objects" << endl;
  for(int i=1;i<=qtd_of_packages;++i) cout << "package" << i << endl;
  cout << ")" << endl;
  cout << "(:init" << endl;
  for(int i=1;i<=qtd_of_packages;++i)  cout << "(package package" << i << ")\n";
  for(auto x:dependencies) cout << "(depends-on package" << x.first << " package" << x.second  << ")\n";
  cout << "(= (total-cost) 0))" << endl;
  cout << "(:goal (and " << endl;
  for(int i=0;i<QTD_OF_INSTALLED_PACKAGES;++i){
    cout << "(installed package" << packages[i] << ")" << endl;
  }
  cout << "))" << endl;
  cout << "(:metric minimize (total-cost)))" << endl;

}

; Packages: 143 | Dependencies: 90
(define (problem install-debian-packages)
(:domain dependencies-manager)
(:objects
package1
package2
package3
package4
package5
package6
package7
package8
package9
package10
package11
package12
package13
package14
package15
package16
package17
package18
package19
package20
package21
package22
package23
package24
package25
package26
package27
package28
package29
package30
package31
package32
package33
package34
package35
package36
package37
package38
package39
package40
package41
package42
package43
package44
package45
package46
package47
package48
package49
package50
package51
package52
package53
package54
package55
package56
package57
package58
package59
package60
package61
package62
package63
package64
package65
package66
package67
package68
package69
package70
package71
package72
package73
package74
package75
package76
package77
package78
package79
package80
package81
package82
package83
package84
package85
package86
package87
package88
package89
package90
package91
package92
package93
package94
package95
package96
package97
package98
package99
package100
package101
package102
package103
package104
package105
package106
package107
package108
package109
package110
package111
package112
package113
package114
package115
package116
package117
package118
package119
package120
package121
package122
package123
package124
package125
package126
package127
package128
package129
package130
package131
package132
package133
package134
package135
package136
package137
package138
package139
package140
package141
package142
package143
)
(:init
(package package1)
(package package2)
(package package3)
(package package4)
(package package5)
(package package6)
(package package7)
(package package8)
(package package9)
(package package10)
(package package11)
(package package12)
(package package13)
(package package14)
(package package15)
(package package16)
(package package17)
(package package18)
(package package19)
(package package20)
(package package21)
(package package22)
(package package23)
(package package24)
(package package25)
(package package26)
(package package27)
(package package28)
(package package29)
(package package30)
(package package31)
(package package32)
(package package33)
(package package34)
(package package35)
(package package36)
(package package37)
(package package38)
(package package39)
(package package40)
(package package41)
(package package42)
(package package43)
(package package44)
(package package45)
(package package46)
(package package47)
(package package48)
(package package49)
(package package50)
(package package51)
(package package52)
(package package53)
(package package54)
(package package55)
(package package56)
(package package57)
(package package58)
(package package59)
(package package60)
(package package61)
(package package62)
(package package63)
(package package64)
(package package65)
(package package66)
(package package67)
(package package68)
(package package69)
(package package70)
(package package71)
(package package72)
(package package73)
(package package74)
(package package75)
(package package76)
(package package77)
(package package78)
(package package79)
(package package80)
(package package81)
(package package82)
(package package83)
(package package84)
(package package85)
(package package86)
(package package87)
(package package88)
(package package89)
(package package90)
(package package91)
(package package92)
(package package93)
(package package94)
(package package95)
(package package96)
(package package97)
(package package98)
(package package99)
(package package100)
(package package101)
(package package102)
(package package103)
(package package104)
(package package105)
(package package106)
(package package107)
(package package108)
(package package109)
(package package110)
(package package111)
(package package112)
(package package113)
(package package114)
(package package115)
(package package116)
(package package117)
(package package118)
(package package119)
(package package120)
(package package121)
(package package122)
(package package123)
(package package124)
(package package125)
(package package126)
(package package127)
(package package128)
(package package129)
(package package130)
(package package131)
(package package132)
(package package133)
(package package134)
(package package135)
(package package136)
(package package137)
(package package138)
(package package139)
(package package140)
(package package141)
(package package142)
(package package143)
(depends-on package41 package15)
(depends-on package112 package82)
(depends-on package117 package78)
(depends-on package35 package127)
(depends-on package5 package47)
(depends-on package135 package14)
(depends-on package127 package136)
(depends-on package37 package61)
(depends-on package72 package44)
(depends-on package36 package61)
(depends-on package35 package138)
(depends-on package12 package59)
(depends-on package112 package119)
(depends-on package47 package24)
(depends-on package46 package123)
(depends-on package127 package86)
(depends-on package113 package95)
(depends-on package143 package86)
(depends-on package29 package34)
(depends-on package46 package9)
(depends-on package81 package37)
(depends-on package22 package40)
(depends-on package5 package34)
(depends-on package100 package77)
(depends-on package53 package111)
(depends-on package113 package88)
(depends-on package105 package124)
(depends-on package122 package49)
(depends-on package76 package25)
(depends-on package73 package97)
(depends-on package5 package32)
(depends-on package39 package117)
(depends-on package126 package39)
(depends-on package36 package11)
(depends-on package48 package81)
(depends-on package138 package128)
(depends-on package117 package17)
(depends-on package25 package122)
(depends-on package26 package100)
(depends-on package31 package79)
(depends-on package68 package143)
(depends-on package142 package5)
(depends-on package100 package120)
(depends-on package30 package32)
(depends-on package1 package102)
(depends-on package128 package124)
(depends-on package133 package142)
(depends-on package74 package91)
(depends-on package13 package109)
(depends-on package77 package61)
(depends-on package46 package71)
(depends-on package21 package138)
(depends-on package63 package21)
(depends-on package92 package89)
(depends-on package121 package122)
(depends-on package143 package98)
(depends-on package141 package1)
(depends-on package54 package93)
(depends-on package30 package61)
(depends-on package69 package107)
(depends-on package45 package50)
(depends-on package72 package19)
(depends-on package123 package19)
(depends-on package32 package64)
(depends-on package95 package68)
(depends-on package109 package23)
(depends-on package88 package79)
(depends-on package61 package109)
(depends-on package28 package125)
(depends-on package62 package125)
(depends-on package124 package58)
(depends-on package79 package97)
(depends-on package58 package132)
(depends-on package46 package64)
(depends-on package49 package115)
(depends-on package27 package69)
(depends-on package140 package99)
(depends-on package64 package119)
(depends-on package117 package95)
(depends-on package39 package45)
(depends-on package138 package123)
(depends-on package43 package82)
(depends-on package58 package103)
(depends-on package23 package85)
(depends-on package60 package43)
(depends-on package41 package93)
(depends-on package97 package8)
(depends-on package62 package40)
(depends-on package71 package110)
(depends-on package130 package73)
(= (total-cost) 0))
(:goal (and 
(installed package27)
(installed package1)
(installed package83)
(installed package137)
(installed package60)
(installed package52)
(installed package121)
(installed package47)
(installed package69)
(installed package16)
))
(:metric minimize (total-cost)))

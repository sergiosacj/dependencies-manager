(define (problem install-debian-packages)
(:domain dependencies-manager)
(:objects
dbus-user-session
colord
dracut
libgstreamer-plugins-bad1.0-0
libopenal1
kcalc
python3-html5lib
libalgorithm-merge-perl
libsynctex2
javascript-common
frameworkintegration
node-clone
libevent-2.1-7
libfile-fcntllock-perl
debian-keyring
libatk-bridge2.0-0
libqt5waylandclient5
libcanberra-gtk3-0
node-yargs
libcom-err2
libsrt1.5-gnutls
libx11-protocol-perl
python3-cryptography
libpcre32-3
apt-config-icons-large-hidpi
libhttp-parser2.9
python3-pyparsing
libe-book-0.1-1
libkf5notifications-data
node-got
libcupsfilters1
libkwinglutils14
node-source-list-map
libkf5attica5
qml-module-qt-labs-qmlmodels
libhwloc-dev
libkf5service5
node-xtend
libxfixes3
libkf5completion5
libwww-mechanize-perl
docbook-dsssl
node-inherits
docbook-utils
libxcb-dri2-0
libsereal-encoder-perl
libjpeg62-turbo-dev
tex-common
libxcb-xfixes0
node-opener
libsys-cpuaffinity-perl
libvulkan1
libvlc5
sonnet-plugins
xdg-dbus-proxy
libfl2
libsphinxbase3
libperl4-corelibs-perl
fonts-noto-ui-core
r-cran-foreign
node-set-value
libkf5i18nlocaledata5
libduktape207
python3-pyqt5
firmware-cavium
libumfpack5
node-cacache
shim-signed-common
node-yargs-parser
breeze-icon-theme
node-fancy-log
xxd
libgdata22
liba52-0.7.4
node-json-buffer
libgstreamer-gl1.0-0
x11-apps
libayatana-appindicator3-1
node-sellside-emitter
tmate
node-is-extglob
python3-roman
libpng16-16
plasma-systemmonitor
python3-yaml
libfakechroot
node-get-stream
psmisc
firmware-samsung
libdevmapper1.02.1
xserver-xorg-input-libinput
libkf5config-data
libsub-exporter-progressive-perl
wireplumber
openjdk-17-jre-headless
git-buildpackage
kdepim-themeeditors
libfdisk1
gnustep-base-common
shared-mime-info
xfonts-base
openssh-client
update-inetd
libkf5syntaxhighlighting5
node-ansi-escapes
libgdbm-compat4
mesa-va-drivers
python3-distro
util-linux
libblockdev-fs2
perl-modules-5.36
ruby-tty-command
libvidstab1.1
accountwizard
libkf5solid5
libtext-iconv-perl
libwww-perl
node-has-values
libqt5qml5
polkit-kde-agent-1
wmctrl
task-brazilian-portuguese-kde-desktop
libpcsclite1
libibus-1.0-5
python3-dotenv
gir1.2-gtk-3.0
libaacs0
node-resumer
libgit2-1.5
libxcb-render0
libprotobuf-lite32
libgck-1-0
qml-module-org-kde-prison
schroot
libdevel-caller-perl
taskwarrior
desktop-base
libctf-nobfd0
texlive-latex-base
libkf5i18n-data
libdeflate0
juk
firmware-brcm80211
node-object-assign
gstreamer1.0-gl
libdevel-callchecker-perl
libgs10-common
libqmi-utils
locales
libtevent0
kitty-terminfo
libxv1
libegl-mesa0
golang-github-containers-image
doc-debian
libxmlrpc-lite-perl
asciidoctor
dmeventd
node-mimic-response
libkf5completion-data
libkf5iconthemes-bin
libfont-afm-perl
libkf5jsapi5
node-fs-readdir-recursive
libsdl1.2debian
libde265-0
libkf5mailtransportakonadi5
texlive-science
libsquashfuse0
libkf5textwidgets-data
libqt5widgets5
hwloc-nox
firmware-qlogic
libdrm-nouveau2
ruby-execjs
libgtk-4-bin
qml-module-org-kde-userfeedback
libzmf-0.0-0
libgfapi0
libkf5xmlgui-bin
whiptail
libtext-markdown-discount-perl
uuid-runtime
libkf5waylandclient5
libtinfo-dev
node-resolve
libmtp9
libgtk2.0-bin
librav1e0
libdecor-0-0
docbook-xsl
libkf5balooengine5
qml-module-org-kde-kquickcontrolsaddons
plasma-discover-backend-fwupd
libgnomekbd8
libmbim-proxy
libkf5modemmanagerqt6
libnet-domain-tld-perl
python3-magic
p7zip-full
libgtk2.0-0
libvorbisenc2
libmypaint-1.5-1
qml-module-qtquick-particles2
libwmf-0.2-7
knotes
sgml-data
libqt5sql5-mysql
node-doctrine
libmwaw-0.3-3
libqt5concurrent5
libx11-data
libsigsegv2
gwenview
libffi-dev
libcolamd2
mbox-importer
mutter-common
containernetworking-plugins
python3-zipp
libtommath1
kpeople-vcard
qml-module-qtquick2
node-prr
libexttextcat-data
node-get-caller-file
libsubid4
libblockdev2
libkfontinst5
golang-github-containers-common
libpci3
gdb-minimal
libqrtr-glib0
fonts-quicksand
laptop-detect
libcrypt-dev
libxft2
node-diff
smbclient
llvm-14-tools
node-fetch
libgnutls30
libperl5.36
phonon4qt5-backend-vlc
qml-module-qtquick-window2
libkf5config-bin
libqt5designer5
gcc-12-base
texlive-extra-utils
shellcheck
gstreamer1.0-plugins-good
node-make-dir
avahi-daemon
libnode-dev
konq-plugins
equivs
libpng-tools
adwaita-icon-theme
systemd-coredump
libkdecorations2private9
xserver-xorg-video-amdgpu
aspell-pt-br
alsa-topology-conf
libxcb-record0
libstd-rust-dev
dns-root-data
libglvnd0
node-tslib
at-spi2-common
node-end-of-stream
node-ws
python3-deprecated
libmpcdec6
libmailtools-perl
libqt5quickcontrols2-5
t1utils
libportaudio2
node-normalize-package-data
libio-pty-perl
node-ssri
kscreen
node-lodash-packages
libsemanage2
node-unicode-match-property-value-ecmascript
libreadline8
qml-module-org-kde-kholidays
libdrm-common
node-merge-stream
node-regenerator-transform
libquadmath0
cloud-image-utils
firmware-ipw2x00
python3-jeepney
pkgconf-bin
gnome-control-center
kmailtransport-akonadi
libmouse-perl
node-mixin-deep
node-source-map-support
libflatpak0
desktop-file-utils
libtermkey1
libpoppler-cpp0v5
python3-grpcio
libkf5itemmodels5
lxcfs
libz3-4
devscripts
npm
debconf-kde-data
libfontbox-java
libkf5configwidgets5
gpg-wks-client
libcrypt1
perl-base
node-osenv
libpython3.11-minimal
libblas-dev
libsnappy1v5
libevent-pthreads-2.1-7
libgimp2.0
libgps28
libtcl8.6
gtk2-engines-pixbuf
libdistro-info-perl
libindi-data
lmodern
usb-modeswitch-data
node-read-pkg
libcodec2-1.0
node-eslint-visitor-keys
qml-module-qtquick-controls
libstdc++6
libcap2-bin
liblab-gamut1
libkpimgapicontacts5
libcurl3-nss
libclone-perl
libexiv2-27
libharfbuzz-icu0
liblua5.2-0
fonts-font-awesome
python3-chardet
pinentry-curses
python3.10-minimal
node-babel-plugin-add-module-exports
node-ansi-regex
libyaml-0-2
plasma-nm
libkf5configwidgets-data
libkf5syndication5abi1
anacron
libreoffice-impress
qml-module-org-kde-kirigami2
libkf5ksieve-data
ruby-webrick
gnome-desktop3-data
plasma-thunderbolt
libcairo-perl
node-npmlog
liblzma-dev
libc-devtools
oxygen-sounds
firmware-siano
signon-plugin-oauth2
kate
ibus-gtk3
xmlto
malcontent
plasma-discover
ruby-mini-portile2
libmodplug1
libsub-exporter-perl
libdevel-stacktrace-perl
libslurm37
libgcab-1.0-0
tcl8.6
libportaudiocpp0
file
libaa1
libkf5globalaccel-data
dbus-system-bus-common
asciidoc-base
python3-ldb
libnova-0.16-0
node-cli-boxes
gstreamer1.0-plugins-bad
libllvm14
libmatroska7
inetutils-telnet
google-chrome-stable
libksysguardsensors1
libclass-c3-perl
libimport-into-perl
libkf5kcmutils5
node-inflight
libdw1
node-serialize-javascript
libpam-gnome-keyring
cups-filters
liblua5.4-0
libfl-dev
i3blocks
libtry-tiny-perl
milou
cups-common
libwildmidi2
texlive-luatex
texlive-binaries
kwayland-data
node-babel7
gnome-remote-desktop
libkf5messageviewer5abi1
libwebp7
libkf5declarative-data
node-postcss-modules-extract-imports
libnet-http-perl
ksystemstats
libglusterfs0
kalendarac
libcanberra-pulse
libkf5eventviews-data
libmodule-scandeps-perl
node-strip-json-comments
libvariable-magic-perl
ruby-dev
xml-core
libflashrom1
libfreerdp-server2-2
libkf5contacteditor5
libfreeaptx0
libpam0g
libdbus-glib-1-2
python3-typer
autopoint
libnuma1
opensc-pkcs11
cups-daemon
libk5crypto3
libpdfbox-java
node-wide-align
asciidoc-common
firefox-esr
libavc1394-0
libgupnp-dlna-2.0-4
linux-libc-dev
libgudev-1.0-0
libmpich-dev
libnice10
libassuan0
texlive-bibtex-extra
nftables
kgamma5
pipewire-pulse
libkf5akonadisearch-bin
netcat-traditional
libqt5positioning5
gir1.2-harfbuzz-0.0
libflac12
debconf
debconf-kde-helper
haveged
libkf5iconthemes5
kwrite
netbase
node-string-decoder
iso-codes
dosfstools
libxshmfence1
libgd3
libkf5textwidgets5
installation-report
pcscd
libcairo2
libkf5krossui5
node-babel-helper-define-polyfill-provider
libkf5kmanagesieve5
curl
libbrotli1
node-commander
node-jsonify
libmagickwand-6.q16-6
libstring-license-perl
llvm-14
libmoo-perl
xz-utils
firmware-netxen
libxau6
tzdata
libjs-async
libmng1
libglx0
lp-solve
node-es6-error
python3-debianbts
qml-module-qtquick-controls2
ruby-ddplugin
python3-markdown
libfile-which-perl
qml-module-qtquick-privatewidgets
libqt5test5
libkf5jobwidgets5
libpng-dev
libkf5wallet5
gnupg-l10n
kmod
qml-module-qtmultimedia
node-randombytes
python3-webencodings
python3-patiencediff
libz3-dev
libvlccore9
libgtk2.0-common
node-quick-lru
plymouth-label
node-wrappy
libmpdec3
python3-six
sddm
libkf5cddb5
libblockdev-loop2
ipp-usb
node-abbrev
asciidoc-dblatex
libkf5runner5
libgvc6
libuno-cppu3
libxkbfile1
node-set-blocking
libasyncns0
libspectre1
plymouth
i965-va-driver
libxcb-glx0
python3-tomlkit
node-promise-inflight
xbitmaps
libconfuse2
libpython3-dev
libemail-date-format-perl
libimath-3-1-29
node-archy
node-flat-cache
libkpimitinerary5
gpgv
drkonqi
node-camelcase
qml-module-org-kde-solid
node-console-control-strings
libsane-common
libffi8
cups-ipp-utils
libgnome-desktop-4-2
libk3b-extracodecs
libtwolame0
libvte-2.91-common
python3-dockerpty
python3-virtualenv
imagemagick
python3-minimal
libspa-0.2-bluetooth
mesa-vdpau-drivers
node-cjs-module-lexer
bluez-obexd
popularity-contest
libkf5akonadinotes5
libqt5multimedia5
node-interpret
libraptor2-0
libpackage-stash-xs-perl
libtext-xslate-perl
librygel-server-2.8-0
libdebhelper-perl
libkf5configgui5
libgraphene-1.0-0
node-decamelize
libxcb-present0
exfatprogs
node-resolve-from
python3-importlib-metadata
kmenuedit
libkeyutils1
libqt5quickparticles5
liblockfile1
liblapack-dev
libnode108
libplymouth5
libzxing2
libkf5pimcommonakonadi5abi1
mdadm
libjs-jquery
libkf5kcmutilscore5
libkf5calendarsupport-data
node-regexpu-core
python3-unidiff
libhtml-tokeparser-simple-perl
xserver-xorg-video-ati
ruby-public-suffix
libkf5widgetsaddons5
eslint
ibus
libxxf86dga1
unzip
build-essential
libdbusmenu-qt5-2
libkf5screen-data
libobjc4
libsnmp-base
libavahi-common-data
libexpat1
libkf5incidenceeditor-data
python3-pygments
libparted-fs-resize0
libkuserfeedbackcore1
libpcre2-posix3
libndp0
libkf5parts-data
node-wordwrap
node-arrify
python3-talloc
g++-12
kitty-shell-integration
libkpimsmtp5abi1
libparams-validationcompiler-perl
python3-pkg-resources
htop
node-regexpp
libatk-wrapper-java-jni
libavformat59
libcapture-tiny-perl
libregexp-pattern-license-perl
libpython3-stdlib
libintl-perl
libnma-gtk4-0
libluajit-5.1-common
0ad-data-common
libpackagekitqt5-1
libpam-kwallet5
python3-distlib
libspeechd2
libdata-messagepack-perl
node-json-schema-traverse
libkf5pimtextedit-plugins
libva2
runc
libvo-amrwbenc0
libkaccounts2
libgee-0.8-2
libqt5texttospeech5
libxcb-composite0
libgfxdr0
powerdevil-data
python3-more-itertools
debsums
libraw1394-11
fakechroot
r-cran-class
libfcgi0ldbl
qml-module-org-kde-kquickcontrols
fonts-freefont-ttf
readline-common
gpgsm
libruby3.0
libx11-6
libxcb-cursor0
node-util-deprecate
qml-module-qtwebengine
librdf0
libgstreamer1.0-0
kio
python3.10-venv
xsltproc
iucode-tool
libatkmm-1.6-1v5
libxinerama1
node-which
libkf5peoplewidgets5
libfakekey0
libqt5networkauth5
coinor-libclp-dev
libkf5parts-plugins
python3
unar
libslirp0
libregexp-pattern-perl
hunspell-en-us
libapt-pkg-perl
libuuid1
libkf5akonadisearch-data
libargon2-1
node-strip-ansi
python3-wadllib
node-string-width
libccid
libfuse2
libcap-ng0
libmime-types-perl
libcloudproviders0
libkf5mailtransport-data
libjson-glib-1.0-common
libupnp13
libksgrd9
libclass-inspector-perl
kpackagetool5
libkf5bookmarks5
xcvt
check-dfsg-status
node-define-properties
black
node-core-js-pure
libxpm4
sysvinit-utils
schroot-common
libva-x11-2
libkf5service-data
dnsmasq-base
libapache-pom-java
libio-string-perl
libsoundtouch1
coinor-libcbc-dev
node-map-visit
node-write-file-atomic
libfile-find-rule-perl
libxcb-render-util0
zsh
libbpf0
growisofs
librevenge-0.0-0
libncurses-dev
libkpimpkpass5
libdrm-intel1
libgdata-common
python3-olefile
xdg-desktop-portal-gtk
libtheora0
kde-cli-tools-data
libatomic1
python3-distutils
r-cran-cluster
libreoffice-writer
libdvbpsi10
python-apt-common
dput
libjs-regenerate
sshfs
libkf5plasma5
libqrencode4
libphonon4qt5-4
dconf-cli
libflac++10
libnss-myhostname
librdkafka1
libvorbis0a
k3b
node-core-js-compat
node-resolve-cwd
libva-wayland2
adduser
gstreamer1.0-plugins-base
libxcb-dpms0
libtss2-esys-3.0.2-0
gpg-agent
tini
libaspell15
texlive-xetex
ca-certificates-java
libestr0
libpagemaker-0.0-0
node-memfs
libsys-hostname-long-perl
libtss2-rc0
cups-core-drivers
libxml-libxml-perl
libsbig4
fonts-texgyre
libteamdctl0
libqt5multimediagsttools5
libsoup-3.0-common
libunwind8
lynx-common
node-strip-bom
libcolorhug2
libamd2
dblatex
libao-common
ruby3.1-doc
libopengl0
libterm-readkey-perl
libkf5gravatar-data
libxml-namespacesupport-perl
libsnmp40
node-babel-plugin-lodash
libhttp-date-perl
libparams-classify-perl
libkf5unitconversion-data
libclang1-14
libnsl-dev
usr-is-merged
qml-module-qtquick-dialogs
dragonplayer
libauthen-sasl-perl
libtext-glob-perl
libfont-ttf-perl
node-postcss-modules-values
socat
python3-semver
node-object-visit
liblist-compare-perl
libtexluajit2
libthai-data
libkf5auth5
apt
libnumbertext-data
python3-setuptools
gcc-12
python3-fastbencode
libspandsp2
libwpg-0.3-3
libblockdev-utils2
libkf5texteditor-bin
node-ajv
texlive-publishers
libunicode-map-perl
libzvbi0
libgpg-error0
libldacbt-abr2
libgc1
autodep8
libhttp-cookies-perl
dblatex-doc
node-core-js
libmp3lame0
libogg0
node-hosted-git-info
libkimageannotator-common
liblqr-1-0
libtext-levenshteinxs-perl
cmake-data
libvo-aacenc0
liblouisutdml9
libdiscover2
iproute2
libkf5messagecomposer5abi1
qml-module-org-kde-bluezqt
libmariadb3
libxi6
libruby3.1
node-mute-stream
udisks2
docker.io
libpipeline1
node-auto-bind
libstring-crc32-perl
liblcms2-2
node-anymatch
node-fast-levenshtein
libset-intspan-perl
node-babel7-runtime
plasma-dataengines-addons
gnome-icon-theme
libselinux1
plasma-framework
libgrantlee-templates5
ibrazilian
apache2-bin
libkf5people-data
libnet-ssleay-perl
node-repeat-string
nodejs
wpasupplicant
libtasn1-6
r-base-core
python3-dev
libappstreamqt2
cracklib-runtime
libkf5akonadimime5
node-is-descriptor
libkf5kontactinterface-data
pbuilder
libspreadsheet-parseexcel-perl
libctf0
libxss1
cowbuilder
node-ajv-keywords
firefox-esr-l10n-pt-br
libixml10
libsrtp2-1
node-assert
gnome-control-center-data
libtss2-tcti-device0
libkf5kdelibs4support5
tipa
node-watchpack
libgstreamer-plugins-base1.0-0
libkf5akonadicalendar-data
libnm0
libinput10
node-is-plain-object
libkf5iconthemes-data
node-commondir
sweeper
ruby-brandur-json-schema
node-pify
tcl
libflite1
libfftw3-double3
libmtp-common
qml-module-qt-labs-folderlistmodel
xdelta3
gnustep-common
cups-ppdc
ruby-erubis
node-is-primitive
ruby-hamster
vrms
libwbclient0
python3-pip-whl
node-p-cancelable
libkf5messagecore5abi1
libqalculate-data
libalgorithm-diff-xs-perl
libdc1394-25
libfeature-compat-try-perl
libkf5calendarcore5abi2
exim4-config
node-promise-retry
libavutil57
plasma-disks
samba-common
ruby-zeitwerk
libkf5mailimporterakonadi5
manpages-dev
netavark
libqt5multimedia5-plugins
libspeex1
amd64-microcode
node-spdx-expression-parse
qml-module-qtwebkit
libtypes-serialiser-perl
libconfig-tiny-perl
libasound2-data
dash
libxml-sax-base-perl
ruby-tty-platform
libqmi-glib5
pulseaudio-module-bluetooth
node-unicode-property-aliases-ecmascript
libpam-kwallet-common
libdav1d6
konsole-kpart
libqt5xml5
libkf5coreaddons-data
libarray-intspan-perl
passwd
qemu-utils
libkpipewire5
libvorbisfile3
libpcrecpp0v5
imagemagick-6-common
node-cli-table
libkf5configqml5
pass
gnome-keyring-pkcs11
ruby3.1-dev
libplacebo208
libpkcs11-helper1
libglib-object-introspection-perl
libnumbertext-1.0-0
abntex
libsemanage-common
ruby-nanoc-cli
libadwaita-1-0
libdmraid1.0.0.rc16
libsignon-qt5-1
containerd
python3-toml
libcddb2
systemsettings
qml-module-org-kde-syntaxhighlighting
libaliased-perl
libfreehand-0.1-1
libglib2.0-0
libxslt1.1
libdb5.3
node-braces
libcdio19
python3.11-minimal
libgbm1
libgs10
node-deep-is
kdepim-addons
tasksel-data
libpoppler126
kde-config-gtk-style
libjson-maybexs-perl
libkf5js5
node-chokidar
node-signal-exit
kde-style-breeze
libboost-thread1.74.0
libgnome-rr-4-2
node-escodegen
libkf5guiaddons-data
dbus-session-bus-common
node-once
qml-module-org-kde-purpose
rtkit
libtexlua53-5
node-spdx-exceptions
xfonts-100dpi
libspa-0.2-modules
dpkg-dev
node-pkg-dir
task-brazilian-portuguese-desktop
libcholmod3
libkfontinstui5
usbutils
python3-certifi
appstream
node-chownr
libncurses6
binutils-x86-64-linux-gnu
texlive-fonts-recommended
liblxc-common
libgsound0
apt-listchanges
libkf5dbusaddons-bin
python3-gpg
libkwalletbackend5-5
libkf5texteditor5
libkf5akonadisearchdebug5
node-busboy
r-cran-kernsmooth
libkf5akonadicalendar5abi1
node-growl
libopenexr-3-1-30
firmware-atheros
libbluetooth3
libetonyek-0.1-1
libtss2-tctildr0
kup-backup
libre-engine-re2-perl
ruby-slow-enumerator-tools
libblas3
libgl1-mesa-dri
libindirect-perl
ruby-haml
node-argparse
libqt5webchannel5
libanyevent-i3-perl
ghostscript
libmpeg2encpp-2.1-0
libqt5webview5
node-ci-info
mesa-vulkan-drivers
cpp
fonts-noto-unhinted
libmypaint-common
libserd-0-0
ruby-sexp-processor
node-read-package-json
libxcb-xinerama0
bridge-utils
node-terser
kde-config-screenlocker
libproxy-tools
node-functional-red-black-tree
libswresample4
node-acorn
node-icss-utils
va-driver-all
libasync-interrupt-perl
libxerces-c3.2
libjbig0
libepubgen-0.1-1
python3-debconf
console-setup
python3-attr
ruby-concurrent
ca-certificates
gvfs
qml-module-org-kde-ksysguard
libfribidi0
python3-fastimport
qtwayland5
libqt5script5
libreoffice-style-breeze
libqt5network5
node-async-each
cups
libkf5pimtextedit5abi2
libaccounts-glib0
libavfilter8
libshout3
xinit
libxcb-damage0
discover-data
colord-data
ruby-thor
sgmlspl
wl-clipboard
kde-config-updates
libiw30
r-cran-lattice
node-webpack-sources
libiso9660-11
fonts-lmodern
libreoffice-l10n-pt-br
libssh-4
python3.11
node-fast-deep-equal
node-readable-stream
gstreamer1.0-plugins-ugly
libparams-util-perl
kde-config-mailtransport
libpathplan4
bind9-libs
libb2-1
libpaper-utils
libxdamage1
node-esrecurse
libkf5akonadisearch-plugins
libksba8
python3-json-pointer
libkdecorations2-5v5
libnma-common
libqt5x11extras5
bluez-firmware
libgnome-desktop-3-20
libsgmls-perl
media-types
node-base64-js
node-decompress-response
fwupd
libnamespace-autoclean-perl
libiterator-util-perl
libkf5bookmarks-data
llvm-14-linker-tools
libksysguardformatter1
ktexteditor-katepart
libsdl2-2.0-0
libsignon-plugins-common1
sound-theme-freedesktop
plasma-workspace-data
fonts-noto-ui-extra
libavahi-glib1
docker-compose
libaccountsservice0
ibus-gtk
libostyle1c2
libkf5pulseaudioqt3
libsane1
ruby-maruku
libkf5notifyconfig5
libdv4
libkf5pimcommon5abi2
libmalcontent-ui-1-1
jq
libasound2-plugins
libnet1
libopenh264-7
libxmlsec1-nss
plasma-desktop
binutils-common
libjs-prettify
libtaskmanager6abi1
apg
libslang2
python3-ubuntutools
kdeplasma-addons-data
libmpfr6
libkf5style5
libgsm1
libpath-iterator-rule-perl
akonadi-contacts-data
libobject-pad-perl
libgprofng0
libkf5i18n5
python3-pysimplesoap
upower
libaudio2
libbsd0
libmodule-find-perl
libaprutil1-dbd-sqlite3
libjbig2dec0
libsasl2-modules-db
ruby-mime-types
libgdbm6
ispell
libgts-bin
libkf5calendarevents5
libemail-address-xs-perl
libkf5dbusaddons5
libreoffice-math
libu2f-udev
fonts-symbola
liblouisutdml-bin
libnl-route-3-200
libqt5quickshapes5
python3-setuptools-whl
liblz1
ixo-usb-jtag
libgtop-2.0-11
libconfuse-common
linux-image-amd64
libpciaccess0
qml-module-qtquick-virtualkeyboard
libkf5kirigami2-5
dmidecode
libproc2-0
libexception-class-perl
libnetaddr-ip-perl
node-collection-visit
libnss3
libimagequant0
kuserfeedback-doc
python3-pil
libcrack2
liblxc1
libsecret-common
libkf5parts5
libcolord-gtk4-1
libcgraph6
libomp-14-dev
bc
libmaxminddb0
node-espree
node-is-number
rofi
python3-configobj
libfreetype6
node-validate-npm-package-name
libimlib2
libphonenumber8
libipc-run-perl
zlib1g
rake
rpcsvc-proto
libpam-modules-bin
node-path-exists
ruby-terser
xkb-data
sddm-theme-debian-maui
python3-grpc-tools
systemd-sysv
libgegl-0.4-0
libb-hooks-op-check-perl
libpackage-stash-perl
libegl1
libhunspell-1.7-0
procps
libepoxy0
licensecheck
gfortran
libwacom-common
ruby-xmlrpc
libqt5webengine-data
libmodule-implementation-perl
libtdb1
libweather-ion7
libaudit1
libbox2d2
kdepim-runtime
libcommon-sense-perl
libnl-genl-3-200
libkf5holidays5
libxmuu1
libuno-sal3
coinor-libcgl1
xfonts-scalable
libltdl7
uno-libs-private
libsasl2-modules
libipc-shareable-perl
preview-latex-style
libresid-builder0c2a
libgweather-4-common
node-y18n
libgmp10
ruby-pkg-config
libdevel-size-perl
reportbug
libldb2
kpartx
libpython3.10-minimal
qml-module-org-kde-newstuff
libfuse3-3
xserver-common
libeditorconfig0
libip4tc2
mpich
libxcb-randr0
fonts-gfs-baskerville
libxml-twig-perl
node-is-stream
dunst
libcanberra-gtk3-module
console-setup-linux
coreutils
e2fsprogs
libspreadsheet-writeexcel-perl
ruby-mime-types-data
node-babel-plugin-polyfill-regenerator
libevdev2
i3status
libunbound8
node-object-inspect
fonts-droid-fallback
fonts-hack
glib-networking
golang-go
librsvg2-2
libqt5webkit5
node-are-we-there-yet
ruby-tty-color
usbmuxd
libpcre3-dev
python3-ibus-1.0
xserver-xorg-video-radeon
libgusb2
libiscsi7
libconst-fast-perl
libkf5doctools5
libsub-quote-perl
powertop
libglib2.0-bin
iputils-ping
node-yallist
liburing2
kactivitymanagerd
libice6
libxcb-icccm4
node-tapable
libxrandr2
libstdc++-12-dev
vcdimager
libharfbuzz0b
libkf5libkdepim-plugins
libkf5service-bin
node-parse-json
node-retry
ruby-nokogiri
libgupnp-1.6-0
libdate-calc-xs-perl
libkf5sonnetcore5
node-del
bison
ttf-freefont
ruby-nanoc-core
libkf5libkleo-data
libvcdinfo0
teckit
node-escape-string-regexp
plasma-runners-addons
node-stack-utils
libboost-chrono1.74.0
libkf5identitymanagementwidgets5
libssl-dev
libmount1
node-picocolors
libgail18
libkf5kexiv2-15.0.0
libapt-pkg6.0
libossp-uuid16
plasma-pa
libsort-versions-perl
libcairo-gobject2
node-locate-path
xfonts-utils
pulseaudio-utils
liblwp-mediatypes-perl
libsmbclient
libstd-rust-1.63
node-ini
fxload
default-mysql-server-core
node-nopt
kamera
plasma-workspace
libmspub-0.1-1
libnettle8
libmodule-runtime-perl
avahi-autoipd
libmarkdown2
libparted2
qemu-block-extra
node-webassemblyjs
libmediaart-2.0-0
libkpimgapitasks5
gstreamer1.0-libav
ruby-rb-inotify
python3-uritemplate
texlive-lang-greek
vlc-plugin-base
node-shebang-regex
xserver-xorg-input-all
libdata-validate-uri-perl
firmware-zd1211
node-is-binary-path
libcfitsio10
node-is-plain-obj
libxcb-shape0
libmjpegutils-2.1-0
fuse-overlayfs
libept1.6.0
node-regjsparser
apt-cacher-ng
libexporter-tiny-perl
lsb-release
libfile-stripnondeterminism-perl
libunicode-linebreak-perl
linux-image-6.1.0-1-amd64
libqt5printsupport5
qml-module-org-kde-people
dwz
libkf5kiontlm5
libnotificationmanager1
libwinpr2-2
libtie-ixhash-perl
librist4
librsync2
debhelper
aptitude-common
python3-click
libnghttp2-14
node-log-driver
libosp5
needrestart
librabbitmq4
python3-debian
grub2-common
python3-pycurl
libgtk3-perl
libgtkmm-3.0-1v5
node-jsesc
xss-lock
libgraphite2-3
zstd
libcbor0.8
xbacklight
libnftables1
libgme0
iptables
gcr
libkf5bluezqt6
python3-docopt
timgm6mb-soundfont
fonts-gfs-porson
node-browserslist
node-errno
node-widest-line
udev
libkf5identitymanagement5
libkmod2
libopencore-amrwb0
libqt5gui5
libdatrie1
ruby-temple
libudisks2-0
libcdt5
gtk-update-icon-cache
libspeexdsp1
node-yaml
powerdevil
libkpimgapicalendar5
fonts-noto-cjk
gir1.2-gdkpixbuf-2.0
libfontembed1
mount
libgit-wrapper-perl
libokular5core10
libkf5holidays-data
libnamespace-clean-perl
python3-texttable
kwin-style-breeze
libkf5globalaccel-bin
node-depd
libnpth0
plasma-browser-integration
kio-ldap
ruby-colored
fonts-noto-color-emoji
keyboard-configuration
libscim8v5
libaprutil1
base-passwd
gnome-settings-daemon-common
libsvtav1enc1
libblockdev-crypto2
qml-module-gsettings1.0
libefiboot1
libjs-is-typedarray
libjsoncpp25
libkf5notifications5
libpolkit-agent-1-0
libbs2b0
libuno-salhelpergcc3-3
libopenal-data
libclass-method-modifiers-perl
linux-base
libpcre2-dev
libmalcontent-0-0
openssl
openjdk-17-jre
terser
firmware-libertas
node-for-own
libgsettings-qt1
node-path-dirname
network-manager
libsyntax-keyword-try-perl
libmtp-runtime
grub-efi-amd64-bin
libtss2-mu0
fuse3
software-properties-gtk
firmware-sof-signed
pbzip2
imagemagick-6.q16
ruby-builder
libzvbi-common
nodejs-doc
tk
libkf5people5
texlive-base
liblmdb0
kwin-data
liblzo2-2
libbluray2
gkbd-capplet
gnupg-utils
manpages
libomp-dev
pkg-config
node-css-selector-tokenizer
libkf5mailtransport5
openvpn
libc-dev-bin
firmware-misc-nonfree
libprocesscore9
liburi-perl
mariadb-client-core-10.6
node-jest-debbundle
libreadonly-perl
qml-module-qt-labs-platform
fonts-lato
libxcb-keysyms1
node-chrome-trace-event
cmake
linux-image-6.0.0-6-amd64
libgnutls-dane0
lua-lgi
node-set-immediate-shim
cargo
libcommons-parent-java
java-common
power-profiles-daemon
libpowerdevilui5
libkf5networkmanagerqt6
node-coveralls
libxcb-sync1
libsub-name-perl
xdelta
unattended-upgrades
libdbusmenu-gtk3-4
kf5-messagelib-data
libdata-optlist-perl
firmware-intel-sound
ibus-gtk4
libgsl27
libldap-2.5-0
python3-merge3
libkf5kiofilewidgets5
dpkg
node-esprima
node-process-nextick-args
cups-filters-core-drivers
libudev1
libges-1.0-0
r-cran-survival
pipewire-bin
p7zip
polkitd
light
systemd-timesyncd
libnss-mdns
tpm-udev
docbook-xml
node-babel-plugin-polyfill-corejs3
libcaca0
libdist-checkconflicts-perl
kdialog
publicsuffix
fakeroot
dmsetup
libkf5windowsystem5
libkf5prisonscanner5
libfile-fnmatch-perl
libwpebackend-fdo-1.0-1
libatk1.0-0
aptitude
libjq1
libyuv0
ppp
qrencode
libarchive-zip-perl
node-globby
libkf5khtml5
i3-wm
cryfs
libgmime-3.0-0
gyp
liblog-any-adapter-screen-perl
bup
python3-wrapt
gnome-settings-daemon
libkworkspace5-5
xournalpp
libx11-xcb1
libkrb5-3
shim-unsigned
sane-utils
node-read
libatopology2
python3-mypy-extensions
node-cli-cursor
libdrm2
libdrm-radeon1
libgupnp-igd-1.0-4
libnet-dbus-perl
libxcb-xrm0
liblc3-0
libref-util-perl
node-sprintf-js
libblockdev-part2
libcairo-gobject-perl
rygel
firmware-realtek
libboost-iostreams1.74.0
texlive-formats-extra
libxkbcommon-x11-0
libdevel-globaldestruction-perl
kaddressbook
libalgorithm-diff-perl
libdigest-perl-md5-perl
kbd
kio-extras
fonts-liberation
libkf5dbusaddons-data
libid3tag0
libxcomposite1
libfmt9
libabsl20220623
libstaroffice-0.0-0
fonts-noto
libip6tc2
libgphoto2-port12
perl-doc
piuparts
node-validate-npm-package-license
libgpgmepp6
libjs-underscore
libxml2
bluez
libpcre2-32-0
libsndio7.0
node-ip
autoconf
libxcb1
node-is-arrayish
glib-networking-common
lsb-base
libjpeg62-turbo
gir1.2-freedesktop
libelf1
lvm2
apt-config-icons
node-restore-cursor
libsepol2
libpython3.11
xserver-xorg-legacy
libkscreenlocker5
fonts-noto-mono
hwdata
libjansson4
gir1.2-ibus-1.0
libzbar0
libomp5-14
python3-pathspec
libgtk-4-common
libabw-0.1-1
libunicode-utf8-perl
libqt5help5
libxdelta2
libproc-processtable-perl
libstartup-notification0
libyaml-tiny-perl
pim-data-exporter
libgssdp-1.6-0
libkf5pimcommonautocorrection5
partitionmanager
qtchooser
gpgconf
libkpimgapicore5abi1
libenchant-2-2
libkf5pimcommon-data
libk3b8
xserver-xorg-video-vmware
libpipewire-0.3-0
libayatana-ido3-0.4-0
libtirpc-dev
node-promzard
poppler-utils
xserver-xorg-video-intel
libnetfilter-conntrack3
libxdmcp6
ibverbs-providers
node-npm-run-path
python3-dulwich
node-optionator
libkf5pimtextedit-data
libpskc0
automake
alsa-ucm-conf
libmm-glib0
cpio
libkf5package5
libberkeleydb-perl
liboxygenstyleconfig5-5
gvfs-common
libsmartcols1
libtomcrypt1
python3-reportbug
git
adequate
python3-apt
libxkbcommon0
liblouis-data
libubsan1
ripgrep
libkf5codecs5
plasma-desktop-data
node-tar
r-cran-spatial
libkgantt2
sysstat
libstrictures-perl
libkgantt2-l10n
firmware-ti-connectivity
libprotobuf32
librygel-core-2.8-0
usb.ids
liblirc-client0
libntlm0
python3-samba
python3-upstream-ontologist
python3-wheel-whl
libjs-sprintf-js
default-jre-headless
samba-common-bin
node-spdx-license-ids
libsub-override-perl
libpangoxft-1.0-0
ruby-rubypants
texlive-pictures
libgcr-ui-3-1
libextutils-depends-perl
node-ignore
tasksel
libkf5contacts-data
libjs-sphinxdoc
kded5
libpwquality-common
libkf5imap-data
node-clone-deep
libgts-0.7-5
bash
libicu72
golang-1.19-go
libcwidget4
libplasma-geolocation-interface5
node-is-buffer
libusbmuxd6
ruby-rouge
ttf-dejavu-core
dictionaries-common
kwrited
libkimageannotator0
libraw20
qml-module-org-kde-draganddrop
texlive-lang-portuguese
bogofilter
libkf5akonadisearchpim5
conmon
libnftnl11
libpango-1.0-0
iw
libaccounts-qt5-1
node-encoding
node-electron-to-chromium
docutils-common
fonts-noto-core
libgslcblas0
libwavpack1
libcairomm-1.0-1v5
node-lru-cache
libkf5purpose-bin
samba-libs
librsvg2-common
coinor-libosi-dev
libtss2-tcti-swtpm0
libmime-tools-perl
hunspell-pt-br
libgcc-s1
dbus-bin
python3-requests
node-tap-mocha-reporter
qdbus-qt5
r-cran-boot
libxres1
node-p-limit
libcommons-logging-java
libxml2-dev
findutils
firmware-netronome
node-is-windows
libqpdf29
ncurses-base
libraqm0
libwayland-client0
libpolkit-gobject-1-0
qml-module-org-kde-pipewire
libregexp-assemble-perl
libio-sessiondata-perl
base-files
libnumber-compare-perl
libxdgutilsdesktopentry1.0.1
libxml-sax-expat-perl
python3-breezy
node-v8flags
exim4-daemon-light
libtalloc2
node-v8-compile-cache
net-tools
node-is-path-inside
libpackagekit-glib2-18
libipc-run3-perl
debian-archive-keyring
gsfonts
libgnome-bluetooth-ui-3.0-13
kinit
node-run-queue
libpod-constants-perl
libkf5tnef5
ffmpegthumbs
ed
libnotify4
kdeconnect
libwireplumber-0.4-0
fonts-noto-cjk-extra
libqmi-proxy
sbuild
libsidplay1v5
libgnustep-base1.28
node-write
node-for-in
libgmpxx4ldbl
libpcre3
r-cran-mass
librygel-db-2.8-0
ruby-ffi
libindidriver1
libpcap0.8
libqt5webenginecore5
libkf5grantleetheme5
libkf5mailcommon5abi2
libwebpdemux2
node-indent-string
libedit2
mailcap
libkf5dnssd5
qml-module-org-kde-activities
libkf5itemviews5
ruby-html2haml
ruby-rubygems
libfreerdp2-2
kfind
libvoikko1
p11-kit-modules
phonon4qt5
python3-colorama
liblockfile-bin
apt-rdepends
libdebuginfod-common
libssh2-1
ruby-multi-json
util-linux-extra
libdbus-1-3
task-laptop
brz-debian
libkf5configcore5
dirmngr
lzop
libgnomekbd-common
libpod-parser-perl
diffutils
node-supports-color
libfakeroot
libxatracker2
libgssglue1
vim-runtime
ksshaskpass
zathura-pdf-poppler
libgirepository-1.0-1
librygel-renderer-2.8-0
libtask-weaken-perl
mawk
libgcc-12-dev
python3-debmutate
libmbedx509-1
libkf5filemetadata-data
node-tape
ruby-net-telnet
fonts-fork-awesome
libpulse0
libkf5templateparser5
intel-microcode
libregexp-wildcards-perl
libkf5mailimporter5
libevent-core-2.1-7
libmagickcore-6.q16-6
libpoppler-glib8
kpackagelauncherqml
libfile-basedir-perl
libtimedate-perl
node-regjsgen
libprotobuf-c1
libgeoclue-2-0
libjson-xs-perl
libkf5windowsystem-data
libv4l-0
node-events
qml-module-org-kde-kio
dconf-service
libhtml-tree-perl
libidn2-0
python3-tr
distro-info
tree
libxklavier16
libass9
libeatmydata1
ocl-icd-libopencl1
libgoa-backend-1.0-1
policykit-1
libsmbios-c2
libdebuginfod1
libkf5kiowidgets5
libvolume-key1
liblist-utilsby-perl
libkf5akonadiwidgets5abi1
python3-platformdirs
eject
python3-lxml
python3-nacl
qml-module-org-kde-kconfig
node-core-util-is
libattr1
python3-lazr.uri
ruby-oj
libjs-source-map
libgsasl18
wireless-tools
libxmlsec1-openssl
libhtml-format-perl
libqgpgme15
logrotate
libkf5libkdepim5
libteckit0
akonadi-server
libpsl5
libcgi-fast-perl
lxc-templates
zsh-common
libfilesys-df-perl
libxcb-shm0
node-aproba
libpcre2-8-0
easy-rsa
liboxygenstyle5-5
node-undici
libkf5eventviews5abi1
node-has-unicode
libgssapi-krb5-2
zenity
libsort-naturally-perl
ruby-rdiscount
node-memory-fs
gnustep-base-runtime
libmanette-0.2-0
akonadi-backend-mysql
libipt2
libopenjp2-7
libnorm1
node-get-value
libxext6
libostree-1-1
x11-session-utils
libicu-dev
node-schema-utils
node-agent-base
node-path-is-inside
libhavege2
libkpmcore12
node-iconv-lite
python3-greenlet
libltdl-dev
libkf5guiaddons-bin
gimp
libqxp-0.0-0
python3-tornado
librole-tiny-perl
libkpipewirerecord5
node-builtins
libjack-jackd2-0
libpath-tiny-perl
libtk8.6
libgdk-pixbuf2.0-common
libre2-9
kimageformat-plugins
pipewire
libclass-data-inheritable-perl
libb-hooks-endofscope-perl
python3-docker
libp11-kit0
liblvm2cmd2.03
gnome-online-accounts
libfontenc1
dh-strip-nondeterminism
libfaad2
make
ruby-ruby-parser
icu-devtools
qt5-image-formats-plugins
kdegraphics-thumbnailers
libkf5crash5
enchant-2
libkf5xmlgui5
libtext-wrapi18n-perl
libxt6
libopenni2-0
libmime-charset-perl
libkf5dnssd-data
libtree-sitter0
librubberband2
libann0
r-cran-nnet
lynx
liblouis20
libiec61883-0
libpoppler-qt5-1
autopkgtest
node-wrap-ansi
ruby-addressable
node-debbundle-es-to-primitive
libxcursor1
node-cliui
python3-psycopg2
libpmem1
libpkgconf3
libopencore-amrnb0
libhttp-daemon-perl
graphviz
criu
libcpanel-json-xs-perl
libiterator-perl
librdkafka-dev
node-move-concurrently
libmbedcrypto7
libxcb-xkb1
debianutils
libkf5ldap-data
libkf5screen-bin
libkpathsea6
libqalculate22
libsub-identify-perl
libkf5widgetsaddons-data
traceroute
librdkafka++1
libreoffice-core
node-binary-extensions
libdmtx0b
liblog-dispatch-perl
grub-efi-amd64
libgeocode-glib-2-0
libgles2
libuno-purpenvhelpergcc3-3
ruby-diff-lcs
libx265-199
python3-xdg
polkitd-pkla
libmpg123-0
libuchardet0
fonts-liberation2
software-properties-kde
libnl-3-200
vim-tiny
bind9-dnsutils
firmware-bnx2
libkf5filemetadata-bin
libclucene-core1v5
libdvdread8
ruby-parallel
okular
task-desktop
node-estraverse
libzimg2
libexttextcat-2.0-0
libimobiledevice6
ruby-redcloth
libkate1
cups-client
libpam-systemd
libreoffice-calc
python3-webcolors
golang-1.19-src
cgroupfs-mount
libgfortran5
install-info
liblwp-protocol-https-perl
gnome-bluetooth-3-common
ruby-ddmetrics
libxcb-screensaver0
node-json5
texlive-latex-recommended
cryptsetup
libkf5globalaccel5
libsoxr0
libdjvulibre21
node-time-stamp
libyajl2
tk8.6
node-https-proxy-agent
libkf5akonadisearchxapian5
asciidoc
i3
node-tap-parser
init-system-helpers
libsombok3
libprocessui9
brz
plasma-wallpapers-addons
xserver-xorg-video-fbdev
bzr-builddeb
libwayland-cursor0
libtsan2
libkf5akonadicore5abi2
libdouble-conversion3
libmbedtls14
libldap-common
libfcgi-bin
node-gauge
gstreamer1.0-x
node-isobject
libkf5idletime5
python3-charset-normalizer
libparse-recdescent-perl
apt-file
libprocps8
libbdplus0
libref-util-xs-perl
libkf5purpose5
libwmflite-0.2-7
python3-oauthlib
node-ip-regex
libstring-shellquote-perl
ruby-mustache
libpipewire-0.3-modules
liblog-any-perl
libkf5akonadisearchcore5
libkf5su5
aspell
libxcb-res0
libkf5mailcommon-data
libhwy1
libcap2
libmutter-11-0
libpadwalker-perl
libqt5dbus5
python3-pyxattr
cpp-12
libgail-common
node-json-stable-stringify
libhwloc15
rustc
libatasmart4
node-pump
bubblewrap
apt-config-icons-large
shim-helpers-amd64-signed
libfile-desktopentry-perl
python3-httplib2
librdmacm1
libdjvulibre-text
libgnome-bluetooth-3.0-13
libgtk-3-bin
libtype-tiny-xs-perl
ruby-listen
libatk-wrapper-java
libpixman-1-0
librbd1
mokutil
node-negotiator
libonig5
libnotify-bin
glib-networking-services
libdata-dpath-perl
libkf5archive-data
node-npm-bundled
libgumbo1
node-define-property
libkf5quickaddons5
gnupg
ruby-memo-wise
liblist-someutils-perl
cdparanoia
node-isexe
r-cran-matrix
kdoctools5
fonts-opensymbol
catdoc
r-cran-codetools
xfonts-encodings
libayatana-indicator3-7
qml-module-org-kde-kcm
libdca0
libsecret-1-0
libsystemd-shared
dctrl-tools
node-glob-parent
libtool
malcontent-gui
patchutils
node-chalk
doxygen
libao4
libxmlsec1
firmware-realtek-rtl8723cs-bt
plasma-widgets-addons
coinor-libcoinmp1v5
node-p-map
ruby-redcarpet
libcryptsetup12
xorg
patch
python3-protobuf
baloo-kf5
node-regenerate-unicode-properties
libaudit-common
libkf5kdcraw5
libalgorithm-c3-perl
libbinutils
libvlc-bin
gir1.2-pango-1.0
libnma0
libsndfile1
libnsl2
firmware-bnx2x
libkf5calendarsupport5abi1
liblilv-0-0
libtss2-tcti-mssim0
libltc11
ruby3.1
libhtml-html5-entities-perl
fdisk
libhtml-form-perl
firmware-ath9k-htc
libchromaprint1
flex
breeze
pulseaudio
node-fill-range
python3-keyring
python3-pylibacl
libblkid1
libkf5kcmutils-data
node-deep-equal
libkf5baloowidgets-bin
libkf5codecs-data
node-is-path-cwd
coinor-libcoinutils3v5
zip
libio-socket-ssl-perl
node-uri-js
fonts-dejavu-extra
libmujs2
libjxr-tools
ruby-sass
libclucene-contribs1v5
node-source-map
libfile-touch-perl
libjavascriptcoregtk-4.1-0
libtext-charwidth-perl
libqt5core5a
libstring-copyright-perl
ruby-cri
libstring-escape-perl
libkf5newstuffcore5
plasma-vault
python3-lazr.restfulclient
libmsgpackc2
kde-cli-tools
libxcb-xinput0
libkf5contacts5
libinput-bin
libkf5coreaddons5
libxml2-utils
m4
k3b-i18n
arch-test
node-minipass
node-path-is-absolute
libgupnp-av-1.0-3
libkf5peoplebackend5
libacl1
libkf5notifyconfig-data
libplist3
perl
dbus
libxdgutilsbasedir1.0.1
synaptic
libgif7
breeze-cursor-theme
bzip2-doc
fzf
libpython3.11-stdlib
node-safe-buffer
liblouisutdml-data
node-lcov-parse
pavucontrol
libc-l10n
libgdk-pixbuf2.0-bin
libtag1v5-vanilla
libkf5dav-data
xdg-user-dirs
uidmap
libxrender1
racc
libqca-qt5-2
libperlio-gzip-perl
node-enhanced-resolve
node-n3
libcc1-0
libgpgme11
liblua5.3-0
coinor-libcoinutils-dev
libpulsedsp
libkuserfeedbackwidgets1
debconf-i18n
node-base
libsexp1
libxs-parse-keyword-perl
plasma-discover-common
libdata-dump-perl
libpangomm-1.4-1v5
korganizer
libapache2-mod-dnssd
libkpimaddressbookimportexport5
libxxf86vm1
isc-dhcp-client
ktexteditor-data
lintian
libdevmapper-event1.02.1
firmware-myricom
rsync
libglu1-mesa
libfstrm0
libtime-moment-perl
dex
ark
mariadb-server-core-10.6
breeze-gtk-theme
ruby-progressbar
usb-modeswitch
libqt5virtualkeyboard5
firmware-amd-graphics
node-fs-write-stream-atomic
libkf5plasmaquick5
node-async
libfile-mimeinfo-perl
node-css-loader
qml-module-qtquick-layouts
libssh-gcrypt-4
xserver-xorg-video-all
gir1.2-atk-1.0
libeval-closure-perl
qml-module-qt-labs-settings
libconvert-binhex-perl
libclass-xsaccessor-perl
libxtst6
libhttp-negotiate-perl
libkf5prison5
geoclue-2.0
python3-pyqt5.sip
akregator
libpocketsphinx3
hostname
libdata-validate-domain-perl
libidn12
libpq5
coinor-libosi1v5
libkf5messagelist5abi1
node-p-locate
node-is-typedarray
xorg-docs-core
libcdparanoia0
gdisk
libgomp1
libxmlb2
libepub0
libkf5sysguard-data
libmythes-1.2-0
intel-media-va-driver
indi-dsi
dh-autoreconf
libigdgmm12
libreoffice-draw
libxaw7
libkf5mime5abi1
isync
node-is-extendable
libfile-listing-perl
w3m
wdiff
handlebars
libaio1
libpgm-5.3-0
python3-distro-info
libnotmuch5
libcdr-0.1-1
libkf5declarative5
libtirpc3
libglibmm-2.4-1v5
shim-signed
python3-dns
node-color-convert
libdrm-amdgpu1
libstemmer0d
libtirpc-common
grub-efi-amd64-signed
liblzma5
libmd0
openjade
hdmi2usb-fx2-firmware
libpowerdevilcore2
librtmp1
node-lowercase-keys
gawk
libqaccessibilityclient-qt5-0
kwin-common
libjxr0
libfwupd2
libgcrypt20
libkf5activitiesstats1
ruby-asciidoctor
libxcb-ewmh2
autotools-dev
debootstrap
libsystemd0
rsyslog
node-unset-value
libaom3
libxml-sax-perl
librasqal3
libio-html-perl
libxapian30
lua-luv
libjxl0.7
python3-tomli
kaddressbook-data
libtss2-tcti-cmd0
node-micromatch
libopenmpt-modplug1
neovim-runtime
python3-lib2to3
libmbim-utils
python3-websocket
kitty-doc
libsoup2.4-common
node-gyp
libwrap0
libmpc3
libluajit-5.1-2
libbz2-1.0
libjs-typedarray-to-buffer
libical3
cups-server-common
gzip
libkf5akonadiagentbase5
parted
ranger
abook
libldacbt-enc2
ntfs-3g
libmetis5
libtype-tiny-perl
libwebkit2gtk-4.1-0
node-babel-plugin-polyfill-corejs2
bup-doc
libbz2-dev
libpaper1
node-has-value
bsd-mailx
libboost-program-options1.74.0
libgfrpc0
libx264-164
node-unique-filename
libavahi-common3
node-error-ex
libkf5kdelibs4support5-bin
libwayland-egl1
libpfm4
python3-software-properties
neomutt
libtokyocabinet9
ruby-nanoc-checking
libkf5auth-data
libwoff1
qml-module-qtquick-shapes
libccolamd2
libkf5activities5
libntirpc-dev
libgpm2
apparmor
node-cache-base
libkf5incidenceeditor5abi1
libsoup-3.0-0
eatmydata
libkf5guiaddons5
libkf5ksieve5
libglib2.0-data
libappimage1.0abi1
libmagic1
libopenmpt0
libqt5webengine5
libkf5su-bin
fonts-noto-extra
libkf5su-data
libdata-validate-ip-perl
libio-prompter-perl
node-concat-stream
libvisio-0.1-1
libkf5akonadiprivate5abi2
libhtml-parser-perl
node-punycode
libptexenc1
node-minimatch
tex-gyre
gsasl-common
libasound2
samba-dsdb-modules
vdpau-driver-all
gnome-user-share
libkf5screen7
mime-support
libfreezethaw-perl
node-find-cache-dir
exim4-base
libkf5filemetadata3
wamerican
xauth
libkcolorpicker0
libnss-systemd
libxfont2
libkf5bluezqt-data
busybox-static
node-has-flag
dvd+rw-tools
node-copy-concurrently
efibootmgr
libclang-cpp14
libmd4c0
libksysguardsystemstats1
libwww-robotrules-perl
git-man
isc-dhcp-common
python3-bs4
qml-module-org-kde-kcoreaddons
libintl-xs-perl
latexmk
fonts-dejavu-core
libmro-compat-perl
iio-sensor-proxy
node-find-up
node-flatted
node-require-directory
perl-openssl-defaults
python3-jsonschema
libkf5solid5-data
opensp
coinor-libclp1
gvfs-backends
node-convert-source-map
node-imurmurhash
libpolkit-qt5-1-1
liblapack3
gcc
libsm6
firmware-ivtv
groff-base
libfastjson4
tar
pixz
qml-module-qtquick-templates2
gsettings-desktop-schemas
qml-module-qtqml
libev-perl
node-istanbul
libkf5mbox5
dvisvgm
libqt5sensors5
libxcb-xv0
libkf5krosscore5
kwayland-integration
libwpd-0.10-10
libodfgen-0.1-1
node-mime-types
libregexp-ipv6-perl
libavcodec59
atmel-firmware
libcups2
x11-xserver-utils
libext2fs2
i3lock
libtss2-sys1
liblsan0
libwacom9
libkpimimportwizard5
libreoffice-style-colibre
libexpat1-dev
libmath-base85-perl
libpangocairo-1.0-0
libss2
liblangtag1
libgexiv2-2
default-jre
libsdl-image1.2
ruby-erubi
qml-module-org-kde-kcmutils
libclass-c3-xs-perl
libmbim-glib4
distro-info-data
pulseaudio-module-gsettings
libkf5baloowidgets5
libkf5ldap5abi1
libhtml-tagset-perl
python3-dateutil
libkdsoap1
python3-gitlab
node-foreground-child
libglapi-mesa
libjs-events
podman
libntfs-3g89
libpam-runtime
texlive
firmware-intelwimax
node-file-entry-cache
gpg
ruby-sdbm
default-mysql-client-core
node-iferr
libmtdev1
bind9-host
coinor-libcgl-dev
libsensors-config
libxml-xpathengine-perl
vlc-data
libqt5multimediaquick5
libasan8
libgphoto2-l10n
libjs-util
cups-browsed
libgtk-3-0
libpcre16-3
libsoap-lite-perl
libftdi1-2
libqt5keychain1
qml-module-org-kde-sonnet
libllvm15
firmware-iwlwifi
quilt
libbpf1
libdconf1
libkf5itemviews-data
libnuma-dev
pristine-tar
libsodium23
libpam-modules
libvterm0
coinor-libcbc3
libperlio-utf8-strict-perl
libglx-mesa0
libkf5wallet-data
node-ampproject-remapping
libxvmc1
qml-module-org-kde-qqc2desktopstyle
libarchive13
libboost-filesystem1.74.0
gnome-bluetooth-sendto
libfluidsynth3
pkgconf
libpython3.11-dev
qtspeech5-speechd-plugin
apt-config-icons-hidpi
libmail-sendmail-perl
node-columnify
par2
rpcbind
libdynaloader-functions-perl
node-typedarray-to-buffer
strace
fontconfig
acl
libntirpc4.0
libsoup2.4-1
libvpx7
libssl3
node-shell-quote
node-json-parse-better-errors
g++
packagekit-tools
gvfs-libs
wodim
qml-module-org-kde-runnermodel
libspdlog1.10
libkf5gravatar5abi2
node-util
libvte-2.91-0
libxxhash0
libkf5wallet-bin
libbabl-0.1-0
libgirara-gtk3-3
vim-common
packagekit
libev4
libipc-system-simple-perl
ibus-data
libfftw3-single3
bsdextrautils
libdevel-lexalias-perl
node-jsonparse
libdate-calc-perl
libpipewire-0.3-common
libxvidcore4
systemd
liblangtag-common
libgdk-pixbuf-2.0-0
keditbookmarks
akonadi-mime-data
dconf-gsettings-backend
libatspi2.0-0
ruby
libwayland-server0
w3m-img
libmpich12
node-to-fast-properties
node-unicode-canonical-property-names-ecmascript
mysql-common
libfcgi-perl
docker
libkf5unitconversion5
libdaemon0
librados2
mariadb-common
libxcb-util1
vim
p11-kit
bogofilter-bdb
libgphoto2-6
node-typedarray
libv4lconvert0
node-glob
libqt5sql5
grub-common
node-postcss
libqt5qmlmodels5
ure
slirp4netns
libsuitesparseconfig5
libcontextual-return-perl
node-ms
libqt5webenginewidgets5
libcurl3-gnutls
libmpeg2-4
plasma-integration
libisl23
libgegl-common
libcgi-pm-perl
ncurses-term
r-cran-rpart
libtinfo6
dbus-daemon
python3-launchpadlib
python3-jwt
liblerc4
libproxy1v5
python3-idna
init
node-type-check
libnspr4
python3-filelock
libmagickcore-6.q16-6-extra
libmnl0
piuparts-common
libmysofa1
libsidplay2
node-neo-async
node-union-value
libdolphinvcs5
python3-fuse
libsnapd-glib-2-1
opensc
libpulse-mainloop-glib0
libcairo-script-interpreter2
libibverbs1
node-slash
libmusicbrainz5cc2v5
libpython3.10-stdlib
python3-jaraco.classes
libkf5threadweaver5
libxkbregistry0
libxcb-image0
libkf5mime-data
cron
libgcr-base-3-1
libdecor-0-plugin-1-cairo
xfonts-75dpi
mmdebstrap
pkexec
libkf5webengineviewer5abi1
diffstat
libsord-0-0
node-color-name
libtag1v5
libkf5konq6
libsbc1
node-text-table
libaprutil1-ldap
node-brace-expansion
media-player-info
node-colors
libapparmor1
libseccomp2
node-json-schema
kactivities-bin
libsasl2-modules-kdexoauth2
xserver-xorg-video-nouveau
libkf5globalaccelprivate5
libfontconfig1
libucx0
libzzip-0-13
genisoimage
libguard-perl
libpangoft2-1.0-0
libgfortran-12-dev
libkf5newstuff-data
ubuntu-dev-tools
kde-style-oxygen-qt5
libnfs13
libmfx1
python3-dbus
node-to-regex-range
poppler-data
libgitlab-api-v4-perl
libkf5sonnet5-data
libqt5quicktemplates2-5
libncursesw6
node-defined
node-spdx-correct
libkf5authcore5
libjpeg-dev
libtiff6
dolphin
liblog-log4perl-perl
libxs-parse-sublike-perl
mupdf-tools
libxstring-perl
python3-secretstorage
libarchive-cpio-perl
vlc-plugin-video-output
libdvdnav4
hicolor-icon-theme
konqueror
node-regenerate
libebml5
libsub-install-perl
wbrazilian
gpg-wks-server
libqmobipocket2
gir1.2-malcontent-0
libkf5syntaxhighlighting-data
libc6
sensible-utils
node-postcss-value-parser
cdrdao
libsratom-0-0
node-normalize-path
libcdio-paranoia2
libapr1
pocketsphinx-en-us
liborc-0.4-0
node-graceful-fs
pci.ids
bluedevil
kinfocenter
acpi
kross
libdbusmenu-glib4
libfile-homedir-perl
libnfnetlink0
qt5-gtk-platformtheme
xclip
libgtk-4-1
python3-docutils
libdebconfclient0
software-properties-common
kitty
libglib-perl
llvm-14-dev
node-mkdirp
realmd
cron-daemon-common
kwin-x11
debian-faq
cowdancer
libva-drm2
libkwineffects14
libpopt0
kmail
node-esquery
sane-airscan
firmware-linux-free
libxcvt0
fonts-noto-hinted
libkf5kdelibs4support-data
libc-ares2
libqt5quick5
libkf5sonnetui5
texlive-latex-extra
libfile-dirlist-perl
libudfread0
libzmq5
libminizip1
kde-spectacle
libqt5svg5
libsasl2-2
r-base-dev
texlive-plain-generic
liblognorm5
cryptsetup-bin
libcarp-clan-perl
xserver-xorg
libkf5khtml-bin
libexif12
node-rimraf
node-npm-package-arg
libxtables12
libzip4
bzr
qml-module-org-kde-kwindowsystem
firmware-linux-nonfree
libqca-qt5-2-plugins
libgl1
node-prelude-ls
libspatialaudio0
node-isarray
x11-common
discover
libole-storage-lite-perl
pigz
libavif15
ruby-tty-which
netpbm
qml-module-qtqml-models2
libcjson1
libkf5xmlgui-data
fonts-texgyre-math
libkf5newstuff5
libijs-0.35
libc6-dev
libkf5pty-data
node-levn
python3.10
libkf5archive5
pinentry-qt
python3-cffi-backend
zathura
libstoken1
libsamplerate0
gnome-keyring
libcolord2
libpostproc56
libjs-inherits
libkf5khtml-data
libkf5package-data
node-defaults
libkolabxml1v5
gfortran-12
dracut-core
libkf5akonadicontact5
python3-pynvim
binutils
libspecio-perl
r-cran-nlme
wget
khotkeys
grep
libnewt0.52
libboost-locale1.74.0
libupower-glib3
node-delegates
kio-extras-data
libunistring2
python3-typing-extensions
libswscale6
libksysguardsensorfaces1
xsettingsd
firmware-qcom-soc
libkf5mimetreeparser5abi1
python3.11-dev
pim-sieve-editor
libkf5kcmutils-bin
krb5-locales
libthai0
bzip2
libsigc++-2.0-0v5
libxcb-dri3-0
libjcat1
libcurl4
python3-msgpack
libjemalloc2
libkf5kontactinterface5
librest-1.0-0
ncurses-bin
liblist-someutils-xs-perl
python3-gi
libeot0
cups-bsd
node-regenerator-runtime
libaribb24-0
node-lodash
fonts-urw-base35
libblockdev-swap2
virtualenv
python3-urllib3
node-pascalcase
node-js-tokens
libvdpau-va-gl1
liburcu8
node-es-abstract
python3-pcre
libcdio-cdda2
libwebpmux3
libwpe-1.0-1
modemmanager
libyaml-libyaml-perl
xdg-desktop-portal
ssl-cert
libunibilium4
libkf5grantleetheme-plugins
emacsen-common
r-cran-mgcv
node-tap
libpcre2-16-0
libqt5multimediawidgets5
node-wcwidth.js
libdpkg-perl
podman-compose
node-caniuse-lite
libucx-dev
libgnome-bg-4-2
libio-stringy-perl
node-fs.realpath
libblockdev-part-err2
libgoa-1.0-common
bogofilter-common
liblayershellqtinterface5
libmime-lite-perl
qml-module-org-kde-kitemmodels
task-brazilian-portuguese
libkf5libkleo5
ifupdown
network-manager-gnome
libkf5libkdepim-data
libavahi-core7
libnet-smtp-ssl-perl
mobile-broadband-provider-info
node-kind-of
ruby-tilt
neovim
python3-pyrsistent
node-readdirp
qml-module-qtgraphicaleffects
libhogweed6
ruby-nanoc-deploying
libkf5imap5
libndctl6
suckless-tools
gir1.2-glib-2.0
libhyphen0
libpwquality1
libmldbm-perl
less
libanyevent-perl
webp-pixbuf-loader
libmplex2-2.1-0
libavahi-client3
sed
libuv1
libuv1-dev
node-ieee754
fwupd-amd64-signed
fontconfig-config
bash-completion
libcamd2
libio-interactive-perl
libjson-c5
libgweather-4-0
node-js-yaml
python3-rfc3987
node-progress
libkf5ksieveui5
node-cli-truncate
gir1.2-packagekitglib-1.0
libkf5newstuffwidgets5
khotkeys-data
libmad0
libmhash2
libsqlite3-0
libvdpau1
manpages-pt-br
libkf5kiogui5
libjim0.81
libfido2-1
util-linux-locales
node-loader-runner
libossp-uuid-perl
libappstream4
node-optimist
libgav1-1
libkf5kiocore5
node-err-code
liborcus-0.17-0
libjson-glib-1.0-0
libxmu6
node-function-bind
po-debconf
libinstpatch-1.0-2
libitm1
libneon27-gnutls
liblocale-gettext-perl
libruby
libgtk-3-common
sudo
libgs-common
zlib1g-dev
libkrb5support0
libmagic-mgc
libfile-chdir-perl
node-shebang-command
nanoc
libwant-perl
accountsservice
intltool-debian
x11-utils
node-unicode-match-property-ecmascript
libhttp-tiny-multipart-perl
libqt5sql5-sqlite
libieee1284-3
liblz4-1
node-data-uri-to-buffer
lxc
libsocket6-perl
libqt5waylandcompositor5
libwebrtc-audio-processing1
libsbuild-perl
login
python3-uno
logsave
bsdutils
dmraid
gimp-data
xdg-utils
node-ansi-styles
libreoffice-common
r-doc-html
gettext-base
node-globals
libheif1
libpotrace0
node-eslint-scope
libcrypt-rc4-perl
gvfs-daemons
libkpimitinerary-data
libxml-parser-perl
libreoffice-base-core
node-es-module-lexer
libusb-1.0-0
scrot
kde-config-sddm
node-slice-ansi
liborcus-parser-0.17-0
llvm-14-runtime
node-balanced-match
bolt
libtime-duration-perl
firmware-ast
libqt5quickwidgets5
ruby-slim
libfeature-compat-class-perl
liberror-perl
libc-bin
node-through
xserver-xorg-video-vesa
libsensors5
python3-soupsieve
libkf5dav5
node-uuid
pinentry-gnome3
golang-src
libnetpbm11
libsereal-decoder-perl
node-is-glob
libgvpr2
node-eslint-utils
librhash0
libjcode-pm-perl
node-esutils
node-mime
libgtop2-common
dahdi-firmware-nonfree
python3-tdb
qttranslations5-l10n
wireless-regdb
libopenconnect5
alsa-utils
libopus0
libgoa-1.0-0b
libhttp-message-perl
python3-requests-toolbelt
qml-module-org-kde-quickcharts
x11-xkb-utils
buildah
libgmp-dev
libnet-netmask-perl
libvisual-0.4-0
sgml-base
xserver-xorg-input-wacom
libzstd1
libjson-perl
nano
libkf5baloo5
libencode-locale-perl
node-path-type
libkf5jobwidgets-data
node-semver
r-recommended
python3-github
libdebconf-kde1
node-rechoir
libfishcamp1
bluetooth
ruby-kramdown
zenity-common
rubygems-integration
smartmontools
libwps-0.4-4
plzip
libcanberra0
apt-utils
man-db
layer-shell-qt
libdate-manip-perl
node-debug
node-jest-worker
libdaxctl1
libcolorcorrect5
at-spi2-core
libshine3
im-config
libphonon4qt5-data
libhfstospell11
libkpimgapi-data
lsof
kate5-data
wmdocker
libuno-cppuhelpergcc3-3
libpam-cgfs
ruby-pastel
gettext
libnet-ipv6addr-perl
libkf5akonadi-data
webpack
kwalletmanager
libkf5calendarutils5
python3-blinker
thin-provisioning-tools
ucf
libmoox-aliases-perl
pciutils
node-minimist
os-prober
kaccounts-providers
libqt5qmlworkerscript5
libkf5pty5
xserver-xorg-core
libefivar1
libbit-vector-perl
k3b-data
khelpcenter
libreadline-dev
)
(:init
(package dbus-user-session)
(package colord)
(package dracut)
(package libgstreamer-plugins-bad1.0-0)
(package libopenal1)
(package kcalc)
(package python3-html5lib)
(package libalgorithm-merge-perl)
(package libsynctex2)
(package javascript-common)
(package frameworkintegration)
(package node-clone)
(package libevent-2.1-7)
(package libfile-fcntllock-perl)
(package debian-keyring)
(package libatk-bridge2.0-0)
(package libqt5waylandclient5)
(package libcanberra-gtk3-0)
(package node-yargs)
(package libcom-err2)
(package libsrt1.5-gnutls)
(package libx11-protocol-perl)
(package python3-cryptography)
(package libpcre32-3)
(package apt-config-icons-large-hidpi)
(package libhttp-parser2.9)
(package python3-pyparsing)
(package libe-book-0.1-1)
(package libkf5notifications-data)
(package node-got)
(package libcupsfilters1)
(package libkwinglutils14)
(package node-source-list-map)
(package libkf5attica5)
(package qml-module-qt-labs-qmlmodels)
(package libhwloc-dev)
(package libkf5service5)
(package node-xtend)
(package libxfixes3)
(package libkf5completion5)
(package libwww-mechanize-perl)
(package docbook-dsssl)
(package node-inherits)
(package docbook-utils)
(package libxcb-dri2-0)
(package libsereal-encoder-perl)
(package libjpeg62-turbo-dev)
(package tex-common)
(package libxcb-xfixes0)
(package node-opener)
(package libsys-cpuaffinity-perl)
(package libvulkan1)
(package libvlc5)
(package sonnet-plugins)
(package xdg-dbus-proxy)
(package libfl2)
(package libsphinxbase3)
(package libperl4-corelibs-perl)
(package fonts-noto-ui-core)
(package r-cran-foreign)
(package node-set-value)
(package libkf5i18nlocaledata5)
(package libduktape207)
(package python3-pyqt5)
(package firmware-cavium)
(package libumfpack5)
(package node-cacache)
(package shim-signed-common)
(package node-yargs-parser)
(package breeze-icon-theme)
(package node-fancy-log)
(package xxd)
(package libgdata22)
(package liba52-0.7.4)
(package node-json-buffer)
(package libgstreamer-gl1.0-0)
(package x11-apps)
(package libayatana-appindicator3-1)
(package node-sellside-emitter)
(package tmate)
(package node-is-extglob)
(package python3-roman)
(package libpng16-16)
(package plasma-systemmonitor)
(package python3-yaml)
(package libfakechroot)
(package node-get-stream)
(package psmisc)
(package firmware-samsung)
(package libdevmapper1.02.1)
(package xserver-xorg-input-libinput)
(package libkf5config-data)
(package libsub-exporter-progressive-perl)
(package wireplumber)
(package openjdk-17-jre-headless)
(package git-buildpackage)
(package kdepim-themeeditors)
(package libfdisk1)
(package gnustep-base-common)
(package shared-mime-info)
(package xfonts-base)
(package openssh-client)
(package update-inetd)
(package libkf5syntaxhighlighting5)
(package node-ansi-escapes)
(package libgdbm-compat4)
(package mesa-va-drivers)
(package python3-distro)
(package util-linux)
(package libblockdev-fs2)
(package perl-modules-5.36)
(package ruby-tty-command)
(package libvidstab1.1)
(package accountwizard)
(package libkf5solid5)
(package libtext-iconv-perl)
(package libwww-perl)
(package node-has-values)
(package libqt5qml5)
(package polkit-kde-agent-1)
(package wmctrl)
(package task-brazilian-portuguese-kde-desktop)
(package libpcsclite1)
(package libibus-1.0-5)
(package python3-dotenv)
(package gir1.2-gtk-3.0)
(package libaacs0)
(package node-resumer)
(package libgit2-1.5)
(package libxcb-render0)
(package libprotobuf-lite32)
(package libgck-1-0)
(package qml-module-org-kde-prison)
(package schroot)
(package libdevel-caller-perl)
(package taskwarrior)
(package desktop-base)
(package libctf-nobfd0)
(package texlive-latex-base)
(package libkf5i18n-data)
(package libdeflate0)
(package juk)
(package firmware-brcm80211)
(package node-object-assign)
(package gstreamer1.0-gl)
(package libdevel-callchecker-perl)
(package libgs10-common)
(package libqmi-utils)
(package locales)
(package libtevent0)
(package kitty-terminfo)
(package libxv1)
(package libegl-mesa0)
(package golang-github-containers-image)
(package doc-debian)
(package libxmlrpc-lite-perl)
(package asciidoctor)
(package dmeventd)
(package node-mimic-response)
(package libkf5completion-data)
(package libkf5iconthemes-bin)
(package libfont-afm-perl)
(package libkf5jsapi5)
(package node-fs-readdir-recursive)
(package libsdl1.2debian)
(package libde265-0)
(package libkf5mailtransportakonadi5)
(package texlive-science)
(package libsquashfuse0)
(package libkf5textwidgets-data)
(package libqt5widgets5)
(package hwloc-nox)
(package firmware-qlogic)
(package libdrm-nouveau2)
(package ruby-execjs)
(package libgtk-4-bin)
(package qml-module-org-kde-userfeedback)
(package libzmf-0.0-0)
(package libgfapi0)
(package libkf5xmlgui-bin)
(package whiptail)
(package libtext-markdown-discount-perl)
(package uuid-runtime)
(package libkf5waylandclient5)
(package libtinfo-dev)
(package node-resolve)
(package libmtp9)
(package libgtk2.0-bin)
(package librav1e0)
(package libdecor-0-0)
(package docbook-xsl)
(package libkf5balooengine5)
(package qml-module-org-kde-kquickcontrolsaddons)
(package plasma-discover-backend-fwupd)
(package libgnomekbd8)
(package libmbim-proxy)
(package libkf5modemmanagerqt6)
(package libnet-domain-tld-perl)
(package python3-magic)
(package p7zip-full)
(package libgtk2.0-0)
(package libvorbisenc2)
(package libmypaint-1.5-1)
(package qml-module-qtquick-particles2)
(package libwmf-0.2-7)
(package knotes)
(package sgml-data)
(package libqt5sql5-mysql)
(package node-doctrine)
(package libmwaw-0.3-3)
(package libqt5concurrent5)
(package libx11-data)
(package libsigsegv2)
(package gwenview)
(package libffi-dev)
(package libcolamd2)
(package mbox-importer)
(package mutter-common)
(package containernetworking-plugins)
(package python3-zipp)
(package libtommath1)
(package kpeople-vcard)
(package qml-module-qtquick2)
(package node-prr)
(package libexttextcat-data)
(package node-get-caller-file)
(package libsubid4)
(package libblockdev2)
(package libkfontinst5)
(package golang-github-containers-common)
(package libpci3)
(package gdb-minimal)
(package libqrtr-glib0)
(package fonts-quicksand)
(package laptop-detect)
(package libcrypt-dev)
(package libxft2)
(package node-diff)
(package smbclient)
(package llvm-14-tools)
(package node-fetch)
(package libgnutls30)
(package libperl5.36)
(package phonon4qt5-backend-vlc)
(package qml-module-qtquick-window2)
(package libkf5config-bin)
(package libqt5designer5)
(package gcc-12-base)
(package texlive-extra-utils)
(package shellcheck)
(package gstreamer1.0-plugins-good)
(package node-make-dir)
(package avahi-daemon)
(package libnode-dev)
(package konq-plugins)
(package equivs)
(package libpng-tools)
(package adwaita-icon-theme)
(package systemd-coredump)
(package libkdecorations2private9)
(package xserver-xorg-video-amdgpu)
(package aspell-pt-br)
(package alsa-topology-conf)
(package libxcb-record0)
(package libstd-rust-dev)
(package dns-root-data)
(package libglvnd0)
(package node-tslib)
(package at-spi2-common)
(package node-end-of-stream)
(package node-ws)
(package python3-deprecated)
(package libmpcdec6)
(package libmailtools-perl)
(package libqt5quickcontrols2-5)
(package t1utils)
(package libportaudio2)
(package node-normalize-package-data)
(package libio-pty-perl)
(package node-ssri)
(package kscreen)
(package node-lodash-packages)
(package libsemanage2)
(package node-unicode-match-property-value-ecmascript)
(package libreadline8)
(package qml-module-org-kde-kholidays)
(package libdrm-common)
(package node-merge-stream)
(package node-regenerator-transform)
(package libquadmath0)
(package cloud-image-utils)
(package firmware-ipw2x00)
(package python3-jeepney)
(package pkgconf-bin)
(package gnome-control-center)
(package kmailtransport-akonadi)
(package libmouse-perl)
(package node-mixin-deep)
(package node-source-map-support)
(package libflatpak0)
(package desktop-file-utils)
(package libtermkey1)
(package libpoppler-cpp0v5)
(package python3-grpcio)
(package libkf5itemmodels5)
(package lxcfs)
(package libz3-4)
(package devscripts)
(package npm)
(package debconf-kde-data)
(package libfontbox-java)
(package libkf5configwidgets5)
(package gpg-wks-client)
(package libcrypt1)
(package perl-base)
(package node-osenv)
(package libpython3.11-minimal)
(package libblas-dev)
(package libsnappy1v5)
(package libevent-pthreads-2.1-7)
(package libgimp2.0)
(package libgps28)
(package libtcl8.6)
(package gtk2-engines-pixbuf)
(package libdistro-info-perl)
(package libindi-data)
(package lmodern)
(package usb-modeswitch-data)
(package node-read-pkg)
(package libcodec2-1.0)
(package node-eslint-visitor-keys)
(package qml-module-qtquick-controls)
(package libstdc++6)
(package libcap2-bin)
(package liblab-gamut1)
(package libkpimgapicontacts5)
(package libcurl3-nss)
(package libclone-perl)
(package libexiv2-27)
(package libharfbuzz-icu0)
(package liblua5.2-0)
(package fonts-font-awesome)
(package python3-chardet)
(package pinentry-curses)
(package python3.10-minimal)
(package node-babel-plugin-add-module-exports)
(package node-ansi-regex)
(package libyaml-0-2)
(package plasma-nm)
(package libkf5configwidgets-data)
(package libkf5syndication5abi1)
(package anacron)
(package libreoffice-impress)
(package qml-module-org-kde-kirigami2)
(package libkf5ksieve-data)
(package ruby-webrick)
(package gnome-desktop3-data)
(package plasma-thunderbolt)
(package libcairo-perl)
(package node-npmlog)
(package liblzma-dev)
(package libc-devtools)
(package oxygen-sounds)
(package firmware-siano)
(package signon-plugin-oauth2)
(package kate)
(package ibus-gtk3)
(package xmlto)
(package malcontent)
(package plasma-discover)
(package ruby-mini-portile2)
(package libmodplug1)
(package libsub-exporter-perl)
(package libdevel-stacktrace-perl)
(package libslurm37)
(package libgcab-1.0-0)
(package tcl8.6)
(package libportaudiocpp0)
(package file)
(package libaa1)
(package libkf5globalaccel-data)
(package dbus-system-bus-common)
(package asciidoc-base)
(package python3-ldb)
(package libnova-0.16-0)
(package node-cli-boxes)
(package gstreamer1.0-plugins-bad)
(package libllvm14)
(package libmatroska7)
(package inetutils-telnet)
(package google-chrome-stable)
(package libksysguardsensors1)
(package libclass-c3-perl)
(package libimport-into-perl)
(package libkf5kcmutils5)
(package node-inflight)
(package libdw1)
(package node-serialize-javascript)
(package libpam-gnome-keyring)
(package cups-filters)
(package liblua5.4-0)
(package libfl-dev)
(package i3blocks)
(package libtry-tiny-perl)
(package milou)
(package cups-common)
(package libwildmidi2)
(package texlive-luatex)
(package texlive-binaries)
(package kwayland-data)
(package node-babel7)
(package gnome-remote-desktop)
(package libkf5messageviewer5abi1)
(package libwebp7)
(package libkf5declarative-data)
(package node-postcss-modules-extract-imports)
(package libnet-http-perl)
(package ksystemstats)
(package libglusterfs0)
(package kalendarac)
(package libcanberra-pulse)
(package libkf5eventviews-data)
(package libmodule-scandeps-perl)
(package node-strip-json-comments)
(package libvariable-magic-perl)
(package ruby-dev)
(package xml-core)
(package libflashrom1)
(package libfreerdp-server2-2)
(package libkf5contacteditor5)
(package libfreeaptx0)
(package libpam0g)
(package libdbus-glib-1-2)
(package python3-typer)
(package autopoint)
(package libnuma1)
(package opensc-pkcs11)
(package cups-daemon)
(package libk5crypto3)
(package libpdfbox-java)
(package node-wide-align)
(package asciidoc-common)
(package firefox-esr)
(package libavc1394-0)
(package libgupnp-dlna-2.0-4)
(package linux-libc-dev)
(package libgudev-1.0-0)
(package libmpich-dev)
(package libnice10)
(package libassuan0)
(package texlive-bibtex-extra)
(package nftables)
(package kgamma5)
(package pipewire-pulse)
(package libkf5akonadisearch-bin)
(package netcat-traditional)
(package libqt5positioning5)
(package gir1.2-harfbuzz-0.0)
(package libflac12)
(package debconf)
(package debconf-kde-helper)
(package haveged)
(package libkf5iconthemes5)
(package kwrite)
(package netbase)
(package node-string-decoder)
(package iso-codes)
(package dosfstools)
(package libxshmfence1)
(package libgd3)
(package libkf5textwidgets5)
(package installation-report)
(package pcscd)
(package libcairo2)
(package libkf5krossui5)
(package node-babel-helper-define-polyfill-provider)
(package libkf5kmanagesieve5)
(package curl)
(package libbrotli1)
(package node-commander)
(package node-jsonify)
(package libmagickwand-6.q16-6)
(package libstring-license-perl)
(package llvm-14)
(package libmoo-perl)
(package xz-utils)
(package firmware-netxen)
(package libxau6)
(package tzdata)
(package libjs-async)
(package libmng1)
(package libglx0)
(package lp-solve)
(package node-es6-error)
(package python3-debianbts)
(package qml-module-qtquick-controls2)
(package ruby-ddplugin)
(package python3-markdown)
(package libfile-which-perl)
(package qml-module-qtquick-privatewidgets)
(package libqt5test5)
(package libkf5jobwidgets5)
(package libpng-dev)
(package libkf5wallet5)
(package gnupg-l10n)
(package kmod)
(package qml-module-qtmultimedia)
(package node-randombytes)
(package python3-webencodings)
(package python3-patiencediff)
(package libz3-dev)
(package libvlccore9)
(package libgtk2.0-common)
(package node-quick-lru)
(package plymouth-label)
(package node-wrappy)
(package libmpdec3)
(package python3-six)
(package sddm)
(package libkf5cddb5)
(package libblockdev-loop2)
(package ipp-usb)
(package node-abbrev)
(package asciidoc-dblatex)
(package libkf5runner5)
(package libgvc6)
(package libuno-cppu3)
(package libxkbfile1)
(package node-set-blocking)
(package libasyncns0)
(package libspectre1)
(package plymouth)
(package i965-va-driver)
(package libxcb-glx0)
(package python3-tomlkit)
(package node-promise-inflight)
(package xbitmaps)
(package libconfuse2)
(package libpython3-dev)
(package libemail-date-format-perl)
(package libimath-3-1-29)
(package node-archy)
(package node-flat-cache)
(package libkpimitinerary5)
(package gpgv)
(package drkonqi)
(package node-camelcase)
(package qml-module-org-kde-solid)
(package node-console-control-strings)
(package libsane-common)
(package libffi8)
(package cups-ipp-utils)
(package libgnome-desktop-4-2)
(package libk3b-extracodecs)
(package libtwolame0)
(package libvte-2.91-common)
(package python3-dockerpty)
(package python3-virtualenv)
(package imagemagick)
(package python3-minimal)
(package libspa-0.2-bluetooth)
(package mesa-vdpau-drivers)
(package node-cjs-module-lexer)
(package bluez-obexd)
(package popularity-contest)
(package libkf5akonadinotes5)
(package libqt5multimedia5)
(package node-interpret)
(package libraptor2-0)
(package libpackage-stash-xs-perl)
(package libtext-xslate-perl)
(package librygel-server-2.8-0)
(package libdebhelper-perl)
(package libkf5configgui5)
(package libgraphene-1.0-0)
(package node-decamelize)
(package libxcb-present0)
(package exfatprogs)
(package node-resolve-from)
(package python3-importlib-metadata)
(package kmenuedit)
(package libkeyutils1)
(package libqt5quickparticles5)
(package liblockfile1)
(package liblapack-dev)
(package libnode108)
(package libplymouth5)
(package libzxing2)
(package libkf5pimcommonakonadi5abi1)
(package mdadm)
(package libjs-jquery)
(package libkf5kcmutilscore5)
(package libkf5calendarsupport-data)
(package node-regexpu-core)
(package python3-unidiff)
(package libhtml-tokeparser-simple-perl)
(package xserver-xorg-video-ati)
(package ruby-public-suffix)
(package libkf5widgetsaddons5)
(package eslint)
(package ibus)
(package libxxf86dga1)
(package unzip)
(package build-essential)
(package libdbusmenu-qt5-2)
(package libkf5screen-data)
(package libobjc4)
(package libsnmp-base)
(package libavahi-common-data)
(package libexpat1)
(package libkf5incidenceeditor-data)
(package python3-pygments)
(package libparted-fs-resize0)
(package libkuserfeedbackcore1)
(package libpcre2-posix3)
(package libndp0)
(package libkf5parts-data)
(package node-wordwrap)
(package node-arrify)
(package python3-talloc)
(package g++-12)
(package kitty-shell-integration)
(package libkpimsmtp5abi1)
(package libparams-validationcompiler-perl)
(package python3-pkg-resources)
(package htop)
(package node-regexpp)
(package libatk-wrapper-java-jni)
(package libavformat59)
(package libcapture-tiny-perl)
(package libregexp-pattern-license-perl)
(package libpython3-stdlib)
(package libintl-perl)
(package libnma-gtk4-0)
(package libluajit-5.1-common)
(package 0ad-data-common)
(package libpackagekitqt5-1)
(package libpam-kwallet5)
(package python3-distlib)
(package libspeechd2)
(package libdata-messagepack-perl)
(package node-json-schema-traverse)
(package libkf5pimtextedit-plugins)
(package libva2)
(package runc)
(package libvo-amrwbenc0)
(package libkaccounts2)
(package libgee-0.8-2)
(package libqt5texttospeech5)
(package libxcb-composite0)
(package libgfxdr0)
(package powerdevil-data)
(package python3-more-itertools)
(package debsums)
(package libraw1394-11)
(package fakechroot)
(package r-cran-class)
(package libfcgi0ldbl)
(package qml-module-org-kde-kquickcontrols)
(package fonts-freefont-ttf)
(package readline-common)
(package gpgsm)
(package libruby3.0)
(package libx11-6)
(package libxcb-cursor0)
(package node-util-deprecate)
(package qml-module-qtwebengine)
(package librdf0)
(package libgstreamer1.0-0)
(package kio)
(package python3.10-venv)
(package xsltproc)
(package iucode-tool)
(package libatkmm-1.6-1v5)
(package libxinerama1)
(package node-which)
(package libkf5peoplewidgets5)
(package libfakekey0)
(package libqt5networkauth5)
(package coinor-libclp-dev)
(package libkf5parts-plugins)
(package python3)
(package unar)
(package libslirp0)
(package libregexp-pattern-perl)
(package hunspell-en-us)
(package libapt-pkg-perl)
(package libuuid1)
(package libkf5akonadisearch-data)
(package libargon2-1)
(package node-strip-ansi)
(package python3-wadllib)
(package node-string-width)
(package libccid)
(package libfuse2)
(package libcap-ng0)
(package libmime-types-perl)
(package libcloudproviders0)
(package libkf5mailtransport-data)
(package libjson-glib-1.0-common)
(package libupnp13)
(package libksgrd9)
(package libclass-inspector-perl)
(package kpackagetool5)
(package libkf5bookmarks5)
(package xcvt)
(package check-dfsg-status)
(package node-define-properties)
(package black)
(package node-core-js-pure)
(package libxpm4)
(package sysvinit-utils)
(package schroot-common)
(package libva-x11-2)
(package libkf5service-data)
(package dnsmasq-base)
(package libapache-pom-java)
(package libio-string-perl)
(package libsoundtouch1)
(package coinor-libcbc-dev)
(package node-map-visit)
(package node-write-file-atomic)
(package libfile-find-rule-perl)
(package libxcb-render-util0)
(package zsh)
(package libbpf0)
(package growisofs)
(package librevenge-0.0-0)
(package libncurses-dev)
(package libkpimpkpass5)
(package libdrm-intel1)
(package libgdata-common)
(package python3-olefile)
(package xdg-desktop-portal-gtk)
(package libtheora0)
(package kde-cli-tools-data)
(package libatomic1)
(package python3-distutils)
(package r-cran-cluster)
(package libreoffice-writer)
(package libdvbpsi10)
(package python-apt-common)
(package dput)
(package libjs-regenerate)
(package sshfs)
(package libkf5plasma5)
(package libqrencode4)
(package libphonon4qt5-4)
(package dconf-cli)
(package libflac++10)
(package libnss-myhostname)
(package librdkafka1)
(package libvorbis0a)
(package k3b)
(package node-core-js-compat)
(package node-resolve-cwd)
(package libva-wayland2)
(package adduser)
(package gstreamer1.0-plugins-base)
(package libxcb-dpms0)
(package libtss2-esys-3.0.2-0)
(package gpg-agent)
(package tini)
(package libaspell15)
(package texlive-xetex)
(package ca-certificates-java)
(package libestr0)
(package libpagemaker-0.0-0)
(package node-memfs)
(package libsys-hostname-long-perl)
(package libtss2-rc0)
(package cups-core-drivers)
(package libxml-libxml-perl)
(package libsbig4)
(package fonts-texgyre)
(package libteamdctl0)
(package libqt5multimediagsttools5)
(package libsoup-3.0-common)
(package libunwind8)
(package lynx-common)
(package node-strip-bom)
(package libcolorhug2)
(package libamd2)
(package dblatex)
(package libao-common)
(package ruby3.1-doc)
(package libopengl0)
(package libterm-readkey-perl)
(package libkf5gravatar-data)
(package libxml-namespacesupport-perl)
(package libsnmp40)
(package node-babel-plugin-lodash)
(package libhttp-date-perl)
(package libparams-classify-perl)
(package libkf5unitconversion-data)
(package libclang1-14)
(package libnsl-dev)
(package usr-is-merged)
(package qml-module-qtquick-dialogs)
(package dragonplayer)
(package libauthen-sasl-perl)
(package libtext-glob-perl)
(package libfont-ttf-perl)
(package node-postcss-modules-values)
(package socat)
(package python3-semver)
(package node-object-visit)
(package liblist-compare-perl)
(package libtexluajit2)
(package libthai-data)
(package libkf5auth5)
(package apt)
(package libnumbertext-data)
(package python3-setuptools)
(package gcc-12)
(package python3-fastbencode)
(package libspandsp2)
(package libwpg-0.3-3)
(package libblockdev-utils2)
(package libkf5texteditor-bin)
(package node-ajv)
(package texlive-publishers)
(package libunicode-map-perl)
(package libzvbi0)
(package libgpg-error0)
(package libldacbt-abr2)
(package libgc1)
(package autodep8)
(package libhttp-cookies-perl)
(package dblatex-doc)
(package node-core-js)
(package libmp3lame0)
(package libogg0)
(package node-hosted-git-info)
(package libkimageannotator-common)
(package liblqr-1-0)
(package libtext-levenshteinxs-perl)
(package cmake-data)
(package libvo-aacenc0)
(package liblouisutdml9)
(package libdiscover2)
(package iproute2)
(package libkf5messagecomposer5abi1)
(package qml-module-org-kde-bluezqt)
(package libmariadb3)
(package libxi6)
(package libruby3.1)
(package node-mute-stream)
(package udisks2)
(package docker.io)
(package libpipeline1)
(package node-auto-bind)
(package libstring-crc32-perl)
(package liblcms2-2)
(package node-anymatch)
(package node-fast-levenshtein)
(package libset-intspan-perl)
(package node-babel7-runtime)
(package plasma-dataengines-addons)
(package gnome-icon-theme)
(package libselinux1)
(package plasma-framework)
(package libgrantlee-templates5)
(package ibrazilian)
(package apache2-bin)
(package libkf5people-data)
(package libnet-ssleay-perl)
(package node-repeat-string)
(package nodejs)
(package wpasupplicant)
(package libtasn1-6)
(package r-base-core)
(package python3-dev)
(package libappstreamqt2)
(package cracklib-runtime)
(package libkf5akonadimime5)
(package node-is-descriptor)
(package libkf5kontactinterface-data)
(package pbuilder)
(package libspreadsheet-parseexcel-perl)
(package libctf0)
(package libxss1)
(package cowbuilder)
(package node-ajv-keywords)
(package firefox-esr-l10n-pt-br)
(package libixml10)
(package libsrtp2-1)
(package node-assert)
(package gnome-control-center-data)
(package libtss2-tcti-device0)
(package libkf5kdelibs4support5)
(package tipa)
(package node-watchpack)
(package libgstreamer-plugins-base1.0-0)
(package libkf5akonadicalendar-data)
(package libnm0)
(package libinput10)
(package node-is-plain-object)
(package libkf5iconthemes-data)
(package node-commondir)
(package sweeper)
(package ruby-brandur-json-schema)
(package node-pify)
(package tcl)
(package libflite1)
(package libfftw3-double3)
(package libmtp-common)
(package qml-module-qt-labs-folderlistmodel)
(package xdelta3)
(package gnustep-common)
(package cups-ppdc)
(package ruby-erubis)
(package node-is-primitive)
(package ruby-hamster)
(package vrms)
(package libwbclient0)
(package python3-pip-whl)
(package node-p-cancelable)
(package libkf5messagecore5abi1)
(package libqalculate-data)
(package libalgorithm-diff-xs-perl)
(package libdc1394-25)
(package libfeature-compat-try-perl)
(package libkf5calendarcore5abi2)
(package exim4-config)
(package node-promise-retry)
(package libavutil57)
(package plasma-disks)
(package samba-common)
(package ruby-zeitwerk)
(package libkf5mailimporterakonadi5)
(package manpages-dev)
(package netavark)
(package libqt5multimedia5-plugins)
(package libspeex1)
(package amd64-microcode)
(package node-spdx-expression-parse)
(package qml-module-qtwebkit)
(package libtypes-serialiser-perl)
(package libconfig-tiny-perl)
(package libasound2-data)
(package dash)
(package libxml-sax-base-perl)
(package ruby-tty-platform)
(package libqmi-glib5)
(package pulseaudio-module-bluetooth)
(package node-unicode-property-aliases-ecmascript)
(package libpam-kwallet-common)
(package libdav1d6)
(package konsole-kpart)
(package libqt5xml5)
(package libkf5coreaddons-data)
(package libarray-intspan-perl)
(package passwd)
(package qemu-utils)
(package libkpipewire5)
(package libvorbisfile3)
(package libpcrecpp0v5)
(package imagemagick-6-common)
(package node-cli-table)
(package libkf5configqml5)
(package pass)
(package gnome-keyring-pkcs11)
(package ruby3.1-dev)
(package libplacebo208)
(package libpkcs11-helper1)
(package libglib-object-introspection-perl)
(package libnumbertext-1.0-0)
(package abntex)
(package libsemanage-common)
(package ruby-nanoc-cli)
(package libadwaita-1-0)
(package libdmraid1.0.0.rc16)
(package libsignon-qt5-1)
(package containerd)
(package python3-toml)
(package libcddb2)
(package systemsettings)
(package qml-module-org-kde-syntaxhighlighting)
(package libaliased-perl)
(package libfreehand-0.1-1)
(package libglib2.0-0)
(package libxslt1.1)
(package libdb5.3)
(package node-braces)
(package libcdio19)
(package python3.11-minimal)
(package libgbm1)
(package libgs10)
(package node-deep-is)
(package kdepim-addons)
(package tasksel-data)
(package libpoppler126)
(package kde-config-gtk-style)
(package libjson-maybexs-perl)
(package libkf5js5)
(package node-chokidar)
(package node-signal-exit)
(package kde-style-breeze)
(package libboost-thread1.74.0)
(package libgnome-rr-4-2)
(package node-escodegen)
(package libkf5guiaddons-data)
(package dbus-session-bus-common)
(package node-once)
(package qml-module-org-kde-purpose)
(package rtkit)
(package libtexlua53-5)
(package node-spdx-exceptions)
(package xfonts-100dpi)
(package libspa-0.2-modules)
(package dpkg-dev)
(package node-pkg-dir)
(package task-brazilian-portuguese-desktop)
(package libcholmod3)
(package libkfontinstui5)
(package usbutils)
(package python3-certifi)
(package appstream)
(package node-chownr)
(package libncurses6)
(package binutils-x86-64-linux-gnu)
(package texlive-fonts-recommended)
(package liblxc-common)
(package libgsound0)
(package apt-listchanges)
(package libkf5dbusaddons-bin)
(package python3-gpg)
(package libkwalletbackend5-5)
(package libkf5texteditor5)
(package libkf5akonadisearchdebug5)
(package node-busboy)
(package r-cran-kernsmooth)
(package libkf5akonadicalendar5abi1)
(package node-growl)
(package libopenexr-3-1-30)
(package firmware-atheros)
(package libbluetooth3)
(package libetonyek-0.1-1)
(package libtss2-tctildr0)
(package kup-backup)
(package libre-engine-re2-perl)
(package ruby-slow-enumerator-tools)
(package libblas3)
(package libgl1-mesa-dri)
(package libindirect-perl)
(package ruby-haml)
(package node-argparse)
(package libqt5webchannel5)
(package libanyevent-i3-perl)
(package ghostscript)
(package libmpeg2encpp-2.1-0)
(package libqt5webview5)
(package node-ci-info)
(package mesa-vulkan-drivers)
(package cpp)
(package fonts-noto-unhinted)
(package libmypaint-common)
(package libserd-0-0)
(package ruby-sexp-processor)
(package node-read-package-json)
(package libxcb-xinerama0)
(package bridge-utils)
(package node-terser)
(package kde-config-screenlocker)
(package libproxy-tools)
(package node-functional-red-black-tree)
(package libswresample4)
(package node-acorn)
(package node-icss-utils)
(package va-driver-all)
(package libasync-interrupt-perl)
(package libxerces-c3.2)
(package libjbig0)
(package libepubgen-0.1-1)
(package python3-debconf)
(package console-setup)
(package python3-attr)
(package ruby-concurrent)
(package ca-certificates)
(package gvfs)
(package qml-module-org-kde-ksysguard)
(package libfribidi0)
(package python3-fastimport)
(package qtwayland5)
(package libqt5script5)
(package libreoffice-style-breeze)
(package libqt5network5)
(package node-async-each)
(package cups)
(package libkf5pimtextedit5abi2)
(package libaccounts-glib0)
(package libavfilter8)
(package libshout3)
(package xinit)
(package libxcb-damage0)
(package discover-data)
(package colord-data)
(package ruby-thor)
(package sgmlspl)
(package wl-clipboard)
(package kde-config-updates)
(package libiw30)
(package r-cran-lattice)
(package node-webpack-sources)
(package libiso9660-11)
(package fonts-lmodern)
(package libreoffice-l10n-pt-br)
(package libssh-4)
(package python3.11)
(package node-fast-deep-equal)
(package node-readable-stream)
(package gstreamer1.0-plugins-ugly)
(package libparams-util-perl)
(package kde-config-mailtransport)
(package libpathplan4)
(package bind9-libs)
(package libb2-1)
(package libpaper-utils)
(package libxdamage1)
(package node-esrecurse)
(package libkf5akonadisearch-plugins)
(package libksba8)
(package python3-json-pointer)
(package libkdecorations2-5v5)
(package libnma-common)
(package libqt5x11extras5)
(package bluez-firmware)
(package libgnome-desktop-3-20)
(package libsgmls-perl)
(package media-types)
(package node-base64-js)
(package node-decompress-response)
(package fwupd)
(package libnamespace-autoclean-perl)
(package libiterator-util-perl)
(package libkf5bookmarks-data)
(package llvm-14-linker-tools)
(package libksysguardformatter1)
(package ktexteditor-katepart)
(package libsdl2-2.0-0)
(package libsignon-plugins-common1)
(package sound-theme-freedesktop)
(package plasma-workspace-data)
(package fonts-noto-ui-extra)
(package libavahi-glib1)
(package docker-compose)
(package libaccountsservice0)
(package ibus-gtk)
(package libostyle1c2)
(package libkf5pulseaudioqt3)
(package libsane1)
(package ruby-maruku)
(package libkf5notifyconfig5)
(package libdv4)
(package libkf5pimcommon5abi2)
(package libmalcontent-ui-1-1)
(package jq)
(package libasound2-plugins)
(package libnet1)
(package libopenh264-7)
(package libxmlsec1-nss)
(package plasma-desktop)
(package binutils-common)
(package libjs-prettify)
(package libtaskmanager6abi1)
(package apg)
(package libslang2)
(package python3-ubuntutools)
(package kdeplasma-addons-data)
(package libmpfr6)
(package libkf5style5)
(package libgsm1)
(package libpath-iterator-rule-perl)
(package akonadi-contacts-data)
(package libobject-pad-perl)
(package libgprofng0)
(package libkf5i18n5)
(package python3-pysimplesoap)
(package upower)
(package libaudio2)
(package libbsd0)
(package libmodule-find-perl)
(package libaprutil1-dbd-sqlite3)
(package libjbig2dec0)
(package libsasl2-modules-db)
(package ruby-mime-types)
(package libgdbm6)
(package ispell)
(package libgts-bin)
(package libkf5calendarevents5)
(package libemail-address-xs-perl)
(package libkf5dbusaddons5)
(package libreoffice-math)
(package libu2f-udev)
(package fonts-symbola)
(package liblouisutdml-bin)
(package libnl-route-3-200)
(package libqt5quickshapes5)
(package python3-setuptools-whl)
(package liblz1)
(package ixo-usb-jtag)
(package libgtop-2.0-11)
(package libconfuse-common)
(package linux-image-amd64)
(package libpciaccess0)
(package qml-module-qtquick-virtualkeyboard)
(package libkf5kirigami2-5)
(package dmidecode)
(package libproc2-0)
(package libexception-class-perl)
(package libnetaddr-ip-perl)
(package node-collection-visit)
(package libnss3)
(package libimagequant0)
(package kuserfeedback-doc)
(package python3-pil)
(package libcrack2)
(package liblxc1)
(package libsecret-common)
(package libkf5parts5)
(package libcolord-gtk4-1)
(package libcgraph6)
(package libomp-14-dev)
(package bc)
(package libmaxminddb0)
(package node-espree)
(package node-is-number)
(package rofi)
(package python3-configobj)
(package libfreetype6)
(package node-validate-npm-package-name)
(package libimlib2)
(package libphonenumber8)
(package libipc-run-perl)
(package zlib1g)
(package rake)
(package rpcsvc-proto)
(package libpam-modules-bin)
(package node-path-exists)
(package ruby-terser)
(package xkb-data)
(package sddm-theme-debian-maui)
(package python3-grpc-tools)
(package systemd-sysv)
(package libgegl-0.4-0)
(package libb-hooks-op-check-perl)
(package libpackage-stash-perl)
(package libegl1)
(package libhunspell-1.7-0)
(package procps)
(package libepoxy0)
(package licensecheck)
(package gfortran)
(package libwacom-common)
(package ruby-xmlrpc)
(package libqt5webengine-data)
(package libmodule-implementation-perl)
(package libtdb1)
(package libweather-ion7)
(package libaudit1)
(package libbox2d2)
(package kdepim-runtime)
(package libcommon-sense-perl)
(package libnl-genl-3-200)
(package libkf5holidays5)
(package libxmuu1)
(package libuno-sal3)
(package coinor-libcgl1)
(package xfonts-scalable)
(package libltdl7)
(package uno-libs-private)
(package libsasl2-modules)
(package libipc-shareable-perl)
(package preview-latex-style)
(package libresid-builder0c2a)
(package libgweather-4-common)
(package node-y18n)
(package libgmp10)
(package ruby-pkg-config)
(package libdevel-size-perl)
(package reportbug)
(package libldb2)
(package kpartx)
(package libpython3.10-minimal)
(package qml-module-org-kde-newstuff)
(package libfuse3-3)
(package xserver-common)
(package libeditorconfig0)
(package libip4tc2)
(package mpich)
(package libxcb-randr0)
(package fonts-gfs-baskerville)
(package libxml-twig-perl)
(package node-is-stream)
(package dunst)
(package libcanberra-gtk3-module)
(package console-setup-linux)
(package coreutils)
(package e2fsprogs)
(package libspreadsheet-writeexcel-perl)
(package ruby-mime-types-data)
(package node-babel-plugin-polyfill-regenerator)
(package libevdev2)
(package i3status)
(package libunbound8)
(package node-object-inspect)
(package fonts-droid-fallback)
(package fonts-hack)
(package glib-networking)
(package golang-go)
(package librsvg2-2)
(package libqt5webkit5)
(package node-are-we-there-yet)
(package ruby-tty-color)
(package usbmuxd)
(package libpcre3-dev)
(package python3-ibus-1.0)
(package xserver-xorg-video-radeon)
(package libgusb2)
(package libiscsi7)
(package libconst-fast-perl)
(package libkf5doctools5)
(package libsub-quote-perl)
(package powertop)
(package libglib2.0-bin)
(package iputils-ping)
(package node-yallist)
(package liburing2)
(package kactivitymanagerd)
(package libice6)
(package libxcb-icccm4)
(package node-tapable)
(package libxrandr2)
(package libstdc++-12-dev)
(package vcdimager)
(package libharfbuzz0b)
(package libkf5libkdepim-plugins)
(package libkf5service-bin)
(package node-parse-json)
(package node-retry)
(package ruby-nokogiri)
(package libgupnp-1.6-0)
(package libdate-calc-xs-perl)
(package libkf5sonnetcore5)
(package node-del)
(package bison)
(package ttf-freefont)
(package ruby-nanoc-core)
(package libkf5libkleo-data)
(package libvcdinfo0)
(package teckit)
(package node-escape-string-regexp)
(package plasma-runners-addons)
(package node-stack-utils)
(package libboost-chrono1.74.0)
(package libkf5identitymanagementwidgets5)
(package libssl-dev)
(package libmount1)
(package node-picocolors)
(package libgail18)
(package libkf5kexiv2-15.0.0)
(package libapt-pkg6.0)
(package libossp-uuid16)
(package plasma-pa)
(package libsort-versions-perl)
(package libcairo-gobject2)
(package node-locate-path)
(package xfonts-utils)
(package pulseaudio-utils)
(package liblwp-mediatypes-perl)
(package libsmbclient)
(package libstd-rust-1.63)
(package node-ini)
(package fxload)
(package default-mysql-server-core)
(package node-nopt)
(package kamera)
(package plasma-workspace)
(package libmspub-0.1-1)
(package libnettle8)
(package libmodule-runtime-perl)
(package avahi-autoipd)
(package libmarkdown2)
(package libparted2)
(package qemu-block-extra)
(package node-webassemblyjs)
(package libmediaart-2.0-0)
(package libkpimgapitasks5)
(package gstreamer1.0-libav)
(package ruby-rb-inotify)
(package python3-uritemplate)
(package texlive-lang-greek)
(package vlc-plugin-base)
(package node-shebang-regex)
(package xserver-xorg-input-all)
(package libdata-validate-uri-perl)
(package firmware-zd1211)
(package node-is-binary-path)
(package libcfitsio10)
(package node-is-plain-obj)
(package libxcb-shape0)
(package libmjpegutils-2.1-0)
(package fuse-overlayfs)
(package libept1.6.0)
(package node-regjsparser)
(package apt-cacher-ng)
(package libexporter-tiny-perl)
(package lsb-release)
(package libfile-stripnondeterminism-perl)
(package libunicode-linebreak-perl)
(package linux-image-6.1.0-1-amd64)
(package libqt5printsupport5)
(package qml-module-org-kde-people)
(package dwz)
(package libkf5kiontlm5)
(package libnotificationmanager1)
(package libwinpr2-2)
(package libtie-ixhash-perl)
(package librist4)
(package librsync2)
(package debhelper)
(package aptitude-common)
(package python3-click)
(package libnghttp2-14)
(package node-log-driver)
(package libosp5)
(package needrestart)
(package librabbitmq4)
(package python3-debian)
(package grub2-common)
(package python3-pycurl)
(package libgtk3-perl)
(package libgtkmm-3.0-1v5)
(package node-jsesc)
(package xss-lock)
(package libgraphite2-3)
(package zstd)
(package libcbor0.8)
(package xbacklight)
(package libnftables1)
(package libgme0)
(package iptables)
(package gcr)
(package libkf5bluezqt6)
(package python3-docopt)
(package timgm6mb-soundfont)
(package fonts-gfs-porson)
(package node-browserslist)
(package node-errno)
(package node-widest-line)
(package udev)
(package libkf5identitymanagement5)
(package libkmod2)
(package libopencore-amrwb0)
(package libqt5gui5)
(package libdatrie1)
(package ruby-temple)
(package libudisks2-0)
(package libcdt5)
(package gtk-update-icon-cache)
(package libspeexdsp1)
(package node-yaml)
(package powerdevil)
(package libkpimgapicalendar5)
(package fonts-noto-cjk)
(package gir1.2-gdkpixbuf-2.0)
(package libfontembed1)
(package mount)
(package libgit-wrapper-perl)
(package libokular5core10)
(package libkf5holidays-data)
(package libnamespace-clean-perl)
(package python3-texttable)
(package kwin-style-breeze)
(package libkf5globalaccel-bin)
(package node-depd)
(package libnpth0)
(package plasma-browser-integration)
(package kio-ldap)
(package ruby-colored)
(package fonts-noto-color-emoji)
(package keyboard-configuration)
(package libscim8v5)
(package libaprutil1)
(package base-passwd)
(package gnome-settings-daemon-common)
(package libsvtav1enc1)
(package libblockdev-crypto2)
(package qml-module-gsettings1.0)
(package libefiboot1)
(package libjs-is-typedarray)
(package libjsoncpp25)
(package libkf5notifications5)
(package libpolkit-agent-1-0)
(package libbs2b0)
(package libuno-salhelpergcc3-3)
(package libopenal-data)
(package libclass-method-modifiers-perl)
(package linux-base)
(package libpcre2-dev)
(package libmalcontent-0-0)
(package openssl)
(package openjdk-17-jre)
(package terser)
(package firmware-libertas)
(package node-for-own)
(package libgsettings-qt1)
(package node-path-dirname)
(package network-manager)
(package libsyntax-keyword-try-perl)
(package libmtp-runtime)
(package grub-efi-amd64-bin)
(package libtss2-mu0)
(package fuse3)
(package software-properties-gtk)
(package firmware-sof-signed)
(package pbzip2)
(package imagemagick-6.q16)
(package ruby-builder)
(package libzvbi-common)
(package nodejs-doc)
(package tk)
(package libkf5people5)
(package texlive-base)
(package liblmdb0)
(package kwin-data)
(package liblzo2-2)
(package libbluray2)
(package gkbd-capplet)
(package gnupg-utils)
(package manpages)
(package libomp-dev)
(package pkg-config)
(package node-css-selector-tokenizer)
(package libkf5mailtransport5)
(package openvpn)
(package libc-dev-bin)
(package firmware-misc-nonfree)
(package libprocesscore9)
(package liburi-perl)
(package mariadb-client-core-10.6)
(package node-jest-debbundle)
(package libreadonly-perl)
(package qml-module-qt-labs-platform)
(package fonts-lato)
(package libxcb-keysyms1)
(package node-chrome-trace-event)
(package cmake)
(package linux-image-6.0.0-6-amd64)
(package libgnutls-dane0)
(package lua-lgi)
(package node-set-immediate-shim)
(package cargo)
(package libcommons-parent-java)
(package java-common)
(package power-profiles-daemon)
(package libpowerdevilui5)
(package libkf5networkmanagerqt6)
(package node-coveralls)
(package libxcb-sync1)
(package libsub-name-perl)
(package xdelta)
(package unattended-upgrades)
(package libdbusmenu-gtk3-4)
(package kf5-messagelib-data)
(package libdata-optlist-perl)
(package firmware-intel-sound)
(package ibus-gtk4)
(package libgsl27)
(package libldap-2.5-0)
(package python3-merge3)
(package libkf5kiofilewidgets5)
(package dpkg)
(package node-esprima)
(package node-process-nextick-args)
(package cups-filters-core-drivers)
(package libudev1)
(package libges-1.0-0)
(package r-cran-survival)
(package pipewire-bin)
(package p7zip)
(package polkitd)
(package light)
(package systemd-timesyncd)
(package libnss-mdns)
(package tpm-udev)
(package docbook-xml)
(package node-babel-plugin-polyfill-corejs3)
(package libcaca0)
(package libdist-checkconflicts-perl)
(package kdialog)
(package publicsuffix)
(package fakeroot)
(package dmsetup)
(package libkf5windowsystem5)
(package libkf5prisonscanner5)
(package libfile-fnmatch-perl)
(package libwpebackend-fdo-1.0-1)
(package libatk1.0-0)
(package aptitude)
(package libjq1)
(package libyuv0)
(package ppp)
(package qrencode)
(package libarchive-zip-perl)
(package node-globby)
(package libkf5khtml5)
(package i3-wm)
(package cryfs)
(package libgmime-3.0-0)
(package gyp)
(package liblog-any-adapter-screen-perl)
(package bup)
(package python3-wrapt)
(package gnome-settings-daemon)
(package libkworkspace5-5)
(package xournalpp)
(package libx11-xcb1)
(package libkrb5-3)
(package shim-unsigned)
(package sane-utils)
(package node-read)
(package libatopology2)
(package python3-mypy-extensions)
(package node-cli-cursor)
(package libdrm2)
(package libdrm-radeon1)
(package libgupnp-igd-1.0-4)
(package libnet-dbus-perl)
(package libxcb-xrm0)
(package liblc3-0)
(package libref-util-perl)
(package node-sprintf-js)
(package libblockdev-part2)
(package libcairo-gobject-perl)
(package rygel)
(package firmware-realtek)
(package libboost-iostreams1.74.0)
(package texlive-formats-extra)
(package libxkbcommon-x11-0)
(package libdevel-globaldestruction-perl)
(package kaddressbook)
(package libalgorithm-diff-perl)
(package libdigest-perl-md5-perl)
(package kbd)
(package kio-extras)
(package fonts-liberation)
(package libkf5dbusaddons-data)
(package libid3tag0)
(package libxcomposite1)
(package libfmt9)
(package libabsl20220623)
(package libstaroffice-0.0-0)
(package fonts-noto)
(package libip6tc2)
(package libgphoto2-port12)
(package perl-doc)
(package piuparts)
(package node-validate-npm-package-license)
(package libgpgmepp6)
(package libjs-underscore)
(package libxml2)
(package bluez)
(package libpcre2-32-0)
(package libsndio7.0)
(package node-ip)
(package autoconf)
(package libxcb1)
(package node-is-arrayish)
(package glib-networking-common)
(package lsb-base)
(package libjpeg62-turbo)
(package gir1.2-freedesktop)
(package libelf1)
(package lvm2)
(package apt-config-icons)
(package node-restore-cursor)
(package libsepol2)
(package libpython3.11)
(package xserver-xorg-legacy)
(package libkscreenlocker5)
(package fonts-noto-mono)
(package hwdata)
(package libjansson4)
(package gir1.2-ibus-1.0)
(package libzbar0)
(package libomp5-14)
(package python3-pathspec)
(package libgtk-4-common)
(package libabw-0.1-1)
(package libunicode-utf8-perl)
(package libqt5help5)
(package libxdelta2)
(package libproc-processtable-perl)
(package libstartup-notification0)
(package libyaml-tiny-perl)
(package pim-data-exporter)
(package libgssdp-1.6-0)
(package libkf5pimcommonautocorrection5)
(package partitionmanager)
(package qtchooser)
(package gpgconf)
(package libkpimgapicore5abi1)
(package libenchant-2-2)
(package libkf5pimcommon-data)
(package libk3b8)
(package xserver-xorg-video-vmware)
(package libpipewire-0.3-0)
(package libayatana-ido3-0.4-0)
(package libtirpc-dev)
(package node-promzard)
(package poppler-utils)
(package xserver-xorg-video-intel)
(package libnetfilter-conntrack3)
(package libxdmcp6)
(package ibverbs-providers)
(package node-npm-run-path)
(package python3-dulwich)
(package node-optionator)
(package libkf5pimtextedit-data)
(package libpskc0)
(package automake)
(package alsa-ucm-conf)
(package libmm-glib0)
(package cpio)
(package libkf5package5)
(package libberkeleydb-perl)
(package liboxygenstyleconfig5-5)
(package gvfs-common)
(package libsmartcols1)
(package libtomcrypt1)
(package python3-reportbug)
(package git)
(package adequate)
(package python3-apt)
(package libxkbcommon0)
(package liblouis-data)
(package libubsan1)
(package ripgrep)
(package libkf5codecs5)
(package plasma-desktop-data)
(package node-tar)
(package r-cran-spatial)
(package libkgantt2)
(package sysstat)
(package libstrictures-perl)
(package libkgantt2-l10n)
(package firmware-ti-connectivity)
(package libprotobuf32)
(package librygel-core-2.8-0)
(package usb.ids)
(package liblirc-client0)
(package libntlm0)
(package python3-samba)
(package python3-upstream-ontologist)
(package python3-wheel-whl)
(package libjs-sprintf-js)
(package default-jre-headless)
(package samba-common-bin)
(package node-spdx-license-ids)
(package libsub-override-perl)
(package libpangoxft-1.0-0)
(package ruby-rubypants)
(package texlive-pictures)
(package libgcr-ui-3-1)
(package libextutils-depends-perl)
(package node-ignore)
(package tasksel)
(package libkf5contacts-data)
(package libjs-sphinxdoc)
(package kded5)
(package libpwquality-common)
(package libkf5imap-data)
(package node-clone-deep)
(package libgts-0.7-5)
(package bash)
(package libicu72)
(package golang-1.19-go)
(package libcwidget4)
(package libplasma-geolocation-interface5)
(package node-is-buffer)
(package libusbmuxd6)
(package ruby-rouge)
(package ttf-dejavu-core)
(package dictionaries-common)
(package kwrited)
(package libkimageannotator0)
(package libraw20)
(package qml-module-org-kde-draganddrop)
(package texlive-lang-portuguese)
(package bogofilter)
(package libkf5akonadisearchpim5)
(package conmon)
(package libnftnl11)
(package libpango-1.0-0)
(package iw)
(package libaccounts-qt5-1)
(package node-encoding)
(package node-electron-to-chromium)
(package docutils-common)
(package fonts-noto-core)
(package libgslcblas0)
(package libwavpack1)
(package libcairomm-1.0-1v5)
(package node-lru-cache)
(package libkf5purpose-bin)
(package samba-libs)
(package librsvg2-common)
(package coinor-libosi-dev)
(package libtss2-tcti-swtpm0)
(package libmime-tools-perl)
(package hunspell-pt-br)
(package libgcc-s1)
(package dbus-bin)
(package python3-requests)
(package node-tap-mocha-reporter)
(package qdbus-qt5)
(package r-cran-boot)
(package libxres1)
(package node-p-limit)
(package libcommons-logging-java)
(package libxml2-dev)
(package findutils)
(package firmware-netronome)
(package node-is-windows)
(package libqpdf29)
(package ncurses-base)
(package libraqm0)
(package libwayland-client0)
(package libpolkit-gobject-1-0)
(package qml-module-org-kde-pipewire)
(package libregexp-assemble-perl)
(package libio-sessiondata-perl)
(package base-files)
(package libnumber-compare-perl)
(package libxdgutilsdesktopentry1.0.1)
(package libxml-sax-expat-perl)
(package python3-breezy)
(package node-v8flags)
(package exim4-daemon-light)
(package libtalloc2)
(package node-v8-compile-cache)
(package net-tools)
(package node-is-path-inside)
(package libpackagekit-glib2-18)
(package libipc-run3-perl)
(package debian-archive-keyring)
(package gsfonts)
(package libgnome-bluetooth-ui-3.0-13)
(package kinit)
(package node-run-queue)
(package libpod-constants-perl)
(package libkf5tnef5)
(package ffmpegthumbs)
(package ed)
(package libnotify4)
(package kdeconnect)
(package libwireplumber-0.4-0)
(package fonts-noto-cjk-extra)
(package libqmi-proxy)
(package sbuild)
(package libsidplay1v5)
(package libgnustep-base1.28)
(package node-write)
(package node-for-in)
(package libgmpxx4ldbl)
(package libpcre3)
(package r-cran-mass)
(package librygel-db-2.8-0)
(package ruby-ffi)
(package libindidriver1)
(package libpcap0.8)
(package libqt5webenginecore5)
(package libkf5grantleetheme5)
(package libkf5mailcommon5abi2)
(package libwebpdemux2)
(package node-indent-string)
(package libedit2)
(package mailcap)
(package libkf5dnssd5)
(package qml-module-org-kde-activities)
(package libkf5itemviews5)
(package ruby-html2haml)
(package ruby-rubygems)
(package libfreerdp2-2)
(package kfind)
(package libvoikko1)
(package p11-kit-modules)
(package phonon4qt5)
(package python3-colorama)
(package liblockfile-bin)
(package apt-rdepends)
(package libdebuginfod-common)
(package libssh2-1)
(package ruby-multi-json)
(package util-linux-extra)
(package libdbus-1-3)
(package task-laptop)
(package brz-debian)
(package libkf5configcore5)
(package dirmngr)
(package lzop)
(package libgnomekbd-common)
(package libpod-parser-perl)
(package diffutils)
(package node-supports-color)
(package libfakeroot)
(package libxatracker2)
(package libgssglue1)
(package vim-runtime)
(package ksshaskpass)
(package zathura-pdf-poppler)
(package libgirepository-1.0-1)
(package librygel-renderer-2.8-0)
(package libtask-weaken-perl)
(package mawk)
(package libgcc-12-dev)
(package python3-debmutate)
(package libmbedx509-1)
(package libkf5filemetadata-data)
(package node-tape)
(package ruby-net-telnet)
(package fonts-fork-awesome)
(package libpulse0)
(package libkf5templateparser5)
(package intel-microcode)
(package libregexp-wildcards-perl)
(package libkf5mailimporter5)
(package libevent-core-2.1-7)
(package libmagickcore-6.q16-6)
(package libpoppler-glib8)
(package kpackagelauncherqml)
(package libfile-basedir-perl)
(package libtimedate-perl)
(package node-regjsgen)
(package libprotobuf-c1)
(package libgeoclue-2-0)
(package libjson-xs-perl)
(package libkf5windowsystem-data)
(package libv4l-0)
(package node-events)
(package qml-module-org-kde-kio)
(package dconf-service)
(package libhtml-tree-perl)
(package libidn2-0)
(package python3-tr)
(package distro-info)
(package tree)
(package libxklavier16)
(package libass9)
(package libeatmydata1)
(package ocl-icd-libopencl1)
(package libgoa-backend-1.0-1)
(package policykit-1)
(package libsmbios-c2)
(package libdebuginfod1)
(package libkf5kiowidgets5)
(package libvolume-key1)
(package liblist-utilsby-perl)
(package libkf5akonadiwidgets5abi1)
(package python3-platformdirs)
(package eject)
(package python3-lxml)
(package python3-nacl)
(package qml-module-org-kde-kconfig)
(package node-core-util-is)
(package libattr1)
(package python3-lazr.uri)
(package ruby-oj)
(package libjs-source-map)
(package libgsasl18)
(package wireless-tools)
(package libxmlsec1-openssl)
(package libhtml-format-perl)
(package libqgpgme15)
(package logrotate)
(package libkf5libkdepim5)
(package libteckit0)
(package akonadi-server)
(package libpsl5)
(package libcgi-fast-perl)
(package lxc-templates)
(package zsh-common)
(package libfilesys-df-perl)
(package libxcb-shm0)
(package node-aproba)
(package libpcre2-8-0)
(package easy-rsa)
(package liboxygenstyle5-5)
(package node-undici)
(package libkf5eventviews5abi1)
(package node-has-unicode)
(package libgssapi-krb5-2)
(package zenity)
(package libsort-naturally-perl)
(package ruby-rdiscount)
(package node-memory-fs)
(package gnustep-base-runtime)
(package libmanette-0.2-0)
(package akonadi-backend-mysql)
(package libipt2)
(package libopenjp2-7)
(package libnorm1)
(package node-get-value)
(package libxext6)
(package libostree-1-1)
(package x11-session-utils)
(package libicu-dev)
(package node-schema-utils)
(package node-agent-base)
(package node-path-is-inside)
(package libhavege2)
(package libkpmcore12)
(package node-iconv-lite)
(package python3-greenlet)
(package libltdl-dev)
(package libkf5guiaddons-bin)
(package gimp)
(package libqxp-0.0-0)
(package python3-tornado)
(package librole-tiny-perl)
(package libkpipewirerecord5)
(package node-builtins)
(package libjack-jackd2-0)
(package libpath-tiny-perl)
(package libtk8.6)
(package libgdk-pixbuf2.0-common)
(package libre2-9)
(package kimageformat-plugins)
(package pipewire)
(package libclass-data-inheritable-perl)
(package libb-hooks-endofscope-perl)
(package python3-docker)
(package libp11-kit0)
(package liblvm2cmd2.03)
(package gnome-online-accounts)
(package libfontenc1)
(package dh-strip-nondeterminism)
(package libfaad2)
(package make)
(package ruby-ruby-parser)
(package icu-devtools)
(package qt5-image-formats-plugins)
(package kdegraphics-thumbnailers)
(package libkf5crash5)
(package enchant-2)
(package libkf5xmlgui5)
(package libtext-wrapi18n-perl)
(package libxt6)
(package libopenni2-0)
(package libmime-charset-perl)
(package libkf5dnssd-data)
(package libtree-sitter0)
(package librubberband2)
(package libann0)
(package r-cran-nnet)
(package lynx)
(package liblouis20)
(package libiec61883-0)
(package libpoppler-qt5-1)
(package autopkgtest)
(package node-wrap-ansi)
(package ruby-addressable)
(package node-debbundle-es-to-primitive)
(package libxcursor1)
(package node-cliui)
(package python3-psycopg2)
(package libpmem1)
(package libpkgconf3)
(package libopencore-amrnb0)
(package libhttp-daemon-perl)
(package graphviz)
(package criu)
(package libcpanel-json-xs-perl)
(package libiterator-perl)
(package librdkafka-dev)
(package node-move-concurrently)
(package libmbedcrypto7)
(package libxcb-xkb1)
(package debianutils)
(package libkf5ldap-data)
(package libkf5screen-bin)
(package libkpathsea6)
(package libqalculate22)
(package libsub-identify-perl)
(package libkf5widgetsaddons-data)
(package traceroute)
(package librdkafka++1)
(package libreoffice-core)
(package node-binary-extensions)
(package libdmtx0b)
(package liblog-dispatch-perl)
(package grub-efi-amd64)
(package libgeocode-glib-2-0)
(package libgles2)
(package libuno-purpenvhelpergcc3-3)
(package ruby-diff-lcs)
(package libx265-199)
(package python3-xdg)
(package polkitd-pkla)
(package libmpg123-0)
(package libuchardet0)
(package fonts-liberation2)
(package software-properties-kde)
(package libnl-3-200)
(package vim-tiny)
(package bind9-dnsutils)
(package firmware-bnx2)
(package libkf5filemetadata-bin)
(package libclucene-core1v5)
(package libdvdread8)
(package ruby-parallel)
(package okular)
(package task-desktop)
(package node-estraverse)
(package libzimg2)
(package libexttextcat-2.0-0)
(package libimobiledevice6)
(package ruby-redcloth)
(package libkate1)
(package cups-client)
(package libpam-systemd)
(package libreoffice-calc)
(package python3-webcolors)
(package golang-1.19-src)
(package cgroupfs-mount)
(package libgfortran5)
(package install-info)
(package liblwp-protocol-https-perl)
(package gnome-bluetooth-3-common)
(package ruby-ddmetrics)
(package libxcb-screensaver0)
(package node-json5)
(package texlive-latex-recommended)
(package cryptsetup)
(package libkf5globalaccel5)
(package libsoxr0)
(package libdjvulibre21)
(package node-time-stamp)
(package libyajl2)
(package tk8.6)
(package node-https-proxy-agent)
(package libkf5akonadisearchxapian5)
(package asciidoc)
(package i3)
(package node-tap-parser)
(package init-system-helpers)
(package libsombok3)
(package libprocessui9)
(package brz)
(package plasma-wallpapers-addons)
(package xserver-xorg-video-fbdev)
(package bzr-builddeb)
(package libwayland-cursor0)
(package libtsan2)
(package libkf5akonadicore5abi2)
(package libdouble-conversion3)
(package libmbedtls14)
(package libldap-common)
(package libfcgi-bin)
(package node-gauge)
(package gstreamer1.0-x)
(package node-isobject)
(package libkf5idletime5)
(package python3-charset-normalizer)
(package libparse-recdescent-perl)
(package apt-file)
(package libprocps8)
(package libbdplus0)
(package libref-util-xs-perl)
(package libkf5purpose5)
(package libwmflite-0.2-7)
(package python3-oauthlib)
(package node-ip-regex)
(package libstring-shellquote-perl)
(package ruby-mustache)
(package libpipewire-0.3-modules)
(package liblog-any-perl)
(package libkf5akonadisearchcore5)
(package libkf5su5)
(package aspell)
(package libxcb-res0)
(package libkf5mailcommon-data)
(package libhwy1)
(package libcap2)
(package libmutter-11-0)
(package libpadwalker-perl)
(package libqt5dbus5)
(package python3-pyxattr)
(package cpp-12)
(package libgail-common)
(package node-json-stable-stringify)
(package libhwloc15)
(package rustc)
(package libatasmart4)
(package node-pump)
(package bubblewrap)
(package apt-config-icons-large)
(package shim-helpers-amd64-signed)
(package libfile-desktopentry-perl)
(package python3-httplib2)
(package librdmacm1)
(package libdjvulibre-text)
(package libgnome-bluetooth-3.0-13)
(package libgtk-3-bin)
(package libtype-tiny-xs-perl)
(package ruby-listen)
(package libatk-wrapper-java)
(package libpixman-1-0)
(package librbd1)
(package mokutil)
(package node-negotiator)
(package libonig5)
(package libnotify-bin)
(package glib-networking-services)
(package libdata-dpath-perl)
(package libkf5archive-data)
(package node-npm-bundled)
(package libgumbo1)
(package node-define-property)
(package libkf5quickaddons5)
(package gnupg)
(package ruby-memo-wise)
(package liblist-someutils-perl)
(package cdparanoia)
(package node-isexe)
(package r-cran-matrix)
(package kdoctools5)
(package fonts-opensymbol)
(package catdoc)
(package r-cran-codetools)
(package xfonts-encodings)
(package libayatana-indicator3-7)
(package qml-module-org-kde-kcm)
(package libdca0)
(package libsecret-1-0)
(package libsystemd-shared)
(package dctrl-tools)
(package node-glob-parent)
(package libtool)
(package malcontent-gui)
(package patchutils)
(package node-chalk)
(package doxygen)
(package libao4)
(package libxmlsec1)
(package firmware-realtek-rtl8723cs-bt)
(package plasma-widgets-addons)
(package coinor-libcoinmp1v5)
(package node-p-map)
(package ruby-redcarpet)
(package libcryptsetup12)
(package xorg)
(package patch)
(package python3-protobuf)
(package baloo-kf5)
(package node-regenerate-unicode-properties)
(package libaudit-common)
(package libkf5kdcraw5)
(package libalgorithm-c3-perl)
(package libbinutils)
(package libvlc-bin)
(package gir1.2-pango-1.0)
(package libnma0)
(package libsndfile1)
(package libnsl2)
(package firmware-bnx2x)
(package libkf5calendarsupport5abi1)
(package liblilv-0-0)
(package libtss2-tcti-mssim0)
(package libltc11)
(package ruby3.1)
(package libhtml-html5-entities-perl)
(package fdisk)
(package libhtml-form-perl)
(package firmware-ath9k-htc)
(package libchromaprint1)
(package flex)
(package breeze)
(package pulseaudio)
(package node-fill-range)
(package python3-keyring)
(package python3-pylibacl)
(package libblkid1)
(package libkf5kcmutils-data)
(package node-deep-equal)
(package libkf5baloowidgets-bin)
(package libkf5codecs-data)
(package node-is-path-cwd)
(package coinor-libcoinutils3v5)
(package zip)
(package libio-socket-ssl-perl)
(package node-uri-js)
(package fonts-dejavu-extra)
(package libmujs2)
(package libjxr-tools)
(package ruby-sass)
(package libclucene-contribs1v5)
(package node-source-map)
(package libfile-touch-perl)
(package libjavascriptcoregtk-4.1-0)
(package libtext-charwidth-perl)
(package libqt5core5a)
(package libstring-copyright-perl)
(package ruby-cri)
(package libstring-escape-perl)
(package libkf5newstuffcore5)
(package plasma-vault)
(package python3-lazr.restfulclient)
(package libmsgpackc2)
(package kde-cli-tools)
(package libxcb-xinput0)
(package libkf5contacts5)
(package libinput-bin)
(package libkf5coreaddons5)
(package libxml2-utils)
(package m4)
(package k3b-i18n)
(package arch-test)
(package node-minipass)
(package node-path-is-absolute)
(package libgupnp-av-1.0-3)
(package libkf5peoplebackend5)
(package libacl1)
(package libkf5notifyconfig-data)
(package libplist3)
(package perl)
(package dbus)
(package libxdgutilsbasedir1.0.1)
(package synaptic)
(package libgif7)
(package breeze-cursor-theme)
(package bzip2-doc)
(package fzf)
(package libpython3.11-stdlib)
(package node-safe-buffer)
(package liblouisutdml-data)
(package node-lcov-parse)
(package pavucontrol)
(package libc-l10n)
(package libgdk-pixbuf2.0-bin)
(package libtag1v5-vanilla)
(package libkf5dav-data)
(package xdg-user-dirs)
(package uidmap)
(package libxrender1)
(package racc)
(package libqca-qt5-2)
(package libperlio-gzip-perl)
(package node-enhanced-resolve)
(package node-n3)
(package libcc1-0)
(package libgpgme11)
(package liblua5.3-0)
(package coinor-libcoinutils-dev)
(package libpulsedsp)
(package libkuserfeedbackwidgets1)
(package debconf-i18n)
(package node-base)
(package libsexp1)
(package libxs-parse-keyword-perl)
(package plasma-discover-common)
(package libdata-dump-perl)
(package libpangomm-1.4-1v5)
(package korganizer)
(package libapache2-mod-dnssd)
(package libkpimaddressbookimportexport5)
(package libxxf86vm1)
(package isc-dhcp-client)
(package ktexteditor-data)
(package lintian)
(package libdevmapper-event1.02.1)
(package firmware-myricom)
(package rsync)
(package libglu1-mesa)
(package libfstrm0)
(package libtime-moment-perl)
(package dex)
(package ark)
(package mariadb-server-core-10.6)
(package breeze-gtk-theme)
(package ruby-progressbar)
(package usb-modeswitch)
(package libqt5virtualkeyboard5)
(package firmware-amd-graphics)
(package node-fs-write-stream-atomic)
(package libkf5plasmaquick5)
(package node-async)
(package libfile-mimeinfo-perl)
(package node-css-loader)
(package qml-module-qtquick-layouts)
(package libssh-gcrypt-4)
(package xserver-xorg-video-all)
(package gir1.2-atk-1.0)
(package libeval-closure-perl)
(package qml-module-qt-labs-settings)
(package libconvert-binhex-perl)
(package libclass-xsaccessor-perl)
(package libxtst6)
(package libhttp-negotiate-perl)
(package libkf5prison5)
(package geoclue-2.0)
(package python3-pyqt5.sip)
(package akregator)
(package libpocketsphinx3)
(package hostname)
(package libdata-validate-domain-perl)
(package libidn12)
(package libpq5)
(package coinor-libosi1v5)
(package libkf5messagelist5abi1)
(package node-p-locate)
(package node-is-typedarray)
(package xorg-docs-core)
(package libcdparanoia0)
(package gdisk)
(package libgomp1)
(package libxmlb2)
(package libepub0)
(package libkf5sysguard-data)
(package libmythes-1.2-0)
(package intel-media-va-driver)
(package indi-dsi)
(package dh-autoreconf)
(package libigdgmm12)
(package libreoffice-draw)
(package libxaw7)
(package libkf5mime5abi1)
(package isync)
(package node-is-extendable)
(package libfile-listing-perl)
(package w3m)
(package wdiff)
(package handlebars)
(package libaio1)
(package libpgm-5.3-0)
(package python3-distro-info)
(package libnotmuch5)
(package libcdr-0.1-1)
(package libkf5declarative5)
(package libtirpc3)
(package libglibmm-2.4-1v5)
(package shim-signed)
(package python3-dns)
(package node-color-convert)
(package libdrm-amdgpu1)
(package libstemmer0d)
(package libtirpc-common)
(package grub-efi-amd64-signed)
(package liblzma5)
(package libmd0)
(package openjade)
(package hdmi2usb-fx2-firmware)
(package libpowerdevilcore2)
(package librtmp1)
(package node-lowercase-keys)
(package gawk)
(package libqaccessibilityclient-qt5-0)
(package kwin-common)
(package libjxr0)
(package libfwupd2)
(package libgcrypt20)
(package libkf5activitiesstats1)
(package ruby-asciidoctor)
(package libxcb-ewmh2)
(package autotools-dev)
(package debootstrap)
(package libsystemd0)
(package rsyslog)
(package node-unset-value)
(package libaom3)
(package libxml-sax-perl)
(package librasqal3)
(package libio-html-perl)
(package libxapian30)
(package lua-luv)
(package libjxl0.7)
(package python3-tomli)
(package kaddressbook-data)
(package libtss2-tcti-cmd0)
(package node-micromatch)
(package libopenmpt-modplug1)
(package neovim-runtime)
(package python3-lib2to3)
(package libmbim-utils)
(package python3-websocket)
(package kitty-doc)
(package libsoup2.4-common)
(package node-gyp)
(package libwrap0)
(package libmpc3)
(package libluajit-5.1-2)
(package libbz2-1.0)
(package libjs-typedarray-to-buffer)
(package libical3)
(package cups-server-common)
(package gzip)
(package libkf5akonadiagentbase5)
(package parted)
(package ranger)
(package abook)
(package libldacbt-enc2)
(package ntfs-3g)
(package libmetis5)
(package libtype-tiny-perl)
(package libwebkit2gtk-4.1-0)
(package node-babel-plugin-polyfill-corejs2)
(package bup-doc)
(package libbz2-dev)
(package libpaper1)
(package node-has-value)
(package bsd-mailx)
(package libboost-program-options1.74.0)
(package libgfrpc0)
(package libx264-164)
(package node-unique-filename)
(package libavahi-common3)
(package node-error-ex)
(package libkf5kdelibs4support5-bin)
(package libwayland-egl1)
(package libpfm4)
(package python3-software-properties)
(package neomutt)
(package libtokyocabinet9)
(package ruby-nanoc-checking)
(package libkf5auth-data)
(package libwoff1)
(package qml-module-qtquick-shapes)
(package libccolamd2)
(package libkf5activities5)
(package libntirpc-dev)
(package libgpm2)
(package apparmor)
(package node-cache-base)
(package libkf5incidenceeditor5abi1)
(package libsoup-3.0-0)
(package eatmydata)
(package libkf5guiaddons5)
(package libkf5ksieve5)
(package libglib2.0-data)
(package libappimage1.0abi1)
(package libmagic1)
(package libopenmpt0)
(package libqt5webengine5)
(package libkf5su-bin)
(package fonts-noto-extra)
(package libkf5su-data)
(package libdata-validate-ip-perl)
(package libio-prompter-perl)
(package node-concat-stream)
(package libvisio-0.1-1)
(package libkf5akonadiprivate5abi2)
(package libhtml-parser-perl)
(package node-punycode)
(package libptexenc1)
(package node-minimatch)
(package tex-gyre)
(package gsasl-common)
(package libasound2)
(package samba-dsdb-modules)
(package vdpau-driver-all)
(package gnome-user-share)
(package libkf5screen7)
(package mime-support)
(package libfreezethaw-perl)
(package node-find-cache-dir)
(package exim4-base)
(package libkf5filemetadata3)
(package wamerican)
(package xauth)
(package libkcolorpicker0)
(package libnss-systemd)
(package libxfont2)
(package libkf5bluezqt-data)
(package busybox-static)
(package node-has-flag)
(package dvd+rw-tools)
(package node-copy-concurrently)
(package efibootmgr)
(package libclang-cpp14)
(package libmd4c0)
(package libksysguardsystemstats1)
(package libwww-robotrules-perl)
(package git-man)
(package isc-dhcp-common)
(package python3-bs4)
(package qml-module-org-kde-kcoreaddons)
(package libintl-xs-perl)
(package latexmk)
(package fonts-dejavu-core)
(package libmro-compat-perl)
(package iio-sensor-proxy)
(package node-find-up)
(package node-flatted)
(package node-require-directory)
(package perl-openssl-defaults)
(package python3-jsonschema)
(package libkf5solid5-data)
(package opensp)
(package coinor-libclp1)
(package gvfs-backends)
(package node-convert-source-map)
(package node-imurmurhash)
(package libpolkit-qt5-1-1)
(package liblapack3)
(package gcc)
(package libsm6)
(package firmware-ivtv)
(package groff-base)
(package libfastjson4)
(package tar)
(package pixz)
(package qml-module-qtquick-templates2)
(package gsettings-desktop-schemas)
(package qml-module-qtqml)
(package libev-perl)
(package node-istanbul)
(package libkf5mbox5)
(package dvisvgm)
(package libqt5sensors5)
(package libxcb-xv0)
(package libkf5krosscore5)
(package kwayland-integration)
(package libwpd-0.10-10)
(package libodfgen-0.1-1)
(package node-mime-types)
(package libregexp-ipv6-perl)
(package libavcodec59)
(package atmel-firmware)
(package libcups2)
(package x11-xserver-utils)
(package libext2fs2)
(package i3lock)
(package libtss2-sys1)
(package liblsan0)
(package libwacom9)
(package libkpimimportwizard5)
(package libreoffice-style-colibre)
(package libexpat1-dev)
(package libmath-base85-perl)
(package libpangocairo-1.0-0)
(package libss2)
(package liblangtag1)
(package libgexiv2-2)
(package default-jre)
(package libsdl-image1.2)
(package ruby-erubi)
(package qml-module-org-kde-kcmutils)
(package libclass-c3-xs-perl)
(package libmbim-glib4)
(package distro-info-data)
(package pulseaudio-module-gsettings)
(package libkf5baloowidgets5)
(package libkf5ldap5abi1)
(package libhtml-tagset-perl)
(package python3-dateutil)
(package libkdsoap1)
(package python3-gitlab)
(package node-foreground-child)
(package libglapi-mesa)
(package libjs-events)
(package podman)
(package libntfs-3g89)
(package libpam-runtime)
(package texlive)
(package firmware-intelwimax)
(package node-file-entry-cache)
(package gpg)
(package ruby-sdbm)
(package default-mysql-client-core)
(package node-iferr)
(package libmtdev1)
(package bind9-host)
(package coinor-libcgl-dev)
(package libsensors-config)
(package libxml-xpathengine-perl)
(package vlc-data)
(package libqt5multimediaquick5)
(package libasan8)
(package libgphoto2-l10n)
(package libjs-util)
(package cups-browsed)
(package libgtk-3-0)
(package libpcre16-3)
(package libsoap-lite-perl)
(package libftdi1-2)
(package libqt5keychain1)
(package qml-module-org-kde-sonnet)
(package libllvm15)
(package firmware-iwlwifi)
(package quilt)
(package libbpf1)
(package libdconf1)
(package libkf5itemviews-data)
(package libnuma-dev)
(package pristine-tar)
(package libsodium23)
(package libpam-modules)
(package libvterm0)
(package coinor-libcbc3)
(package libperlio-utf8-strict-perl)
(package libglx-mesa0)
(package libkf5wallet-data)
(package node-ampproject-remapping)
(package libxvmc1)
(package qml-module-org-kde-qqc2desktopstyle)
(package libarchive13)
(package libboost-filesystem1.74.0)
(package gnome-bluetooth-sendto)
(package libfluidsynth3)
(package pkgconf)
(package libpython3.11-dev)
(package qtspeech5-speechd-plugin)
(package apt-config-icons-hidpi)
(package libmail-sendmail-perl)
(package node-columnify)
(package par2)
(package rpcbind)
(package libdynaloader-functions-perl)
(package node-typedarray-to-buffer)
(package strace)
(package fontconfig)
(package acl)
(package libntirpc4.0)
(package libsoup2.4-1)
(package libvpx7)
(package libssl3)
(package node-shell-quote)
(package node-json-parse-better-errors)
(package g++)
(package packagekit-tools)
(package gvfs-libs)
(package wodim)
(package qml-module-org-kde-runnermodel)
(package libspdlog1.10)
(package libkf5gravatar5abi2)
(package node-util)
(package libvte-2.91-0)
(package libxxhash0)
(package libkf5wallet-bin)
(package libbabl-0.1-0)
(package libgirara-gtk3-3)
(package vim-common)
(package packagekit)
(package libev4)
(package libipc-system-simple-perl)
(package ibus-data)
(package libfftw3-single3)
(package bsdextrautils)
(package libdevel-lexalias-perl)
(package node-jsonparse)
(package libdate-calc-perl)
(package libpipewire-0.3-common)
(package libxvidcore4)
(package systemd)
(package liblangtag-common)
(package libgdk-pixbuf-2.0-0)
(package keditbookmarks)
(package akonadi-mime-data)
(package dconf-gsettings-backend)
(package libatspi2.0-0)
(package ruby)
(package libwayland-server0)
(package w3m-img)
(package libmpich12)
(package node-to-fast-properties)
(package node-unicode-canonical-property-names-ecmascript)
(package mysql-common)
(package libfcgi-perl)
(package docker)
(package libkf5unitconversion5)
(package libdaemon0)
(package librados2)
(package mariadb-common)
(package libxcb-util1)
(package vim)
(package p11-kit)
(package bogofilter-bdb)
(package libgphoto2-6)
(package node-typedarray)
(package libv4lconvert0)
(package node-glob)
(package libqt5sql5)
(package grub-common)
(package node-postcss)
(package libqt5qmlmodels5)
(package ure)
(package slirp4netns)
(package libsuitesparseconfig5)
(package libcontextual-return-perl)
(package node-ms)
(package libqt5webenginewidgets5)
(package libcurl3-gnutls)
(package libmpeg2-4)
(package plasma-integration)
(package libisl23)
(package libgegl-common)
(package libcgi-pm-perl)
(package ncurses-term)
(package r-cran-rpart)
(package libtinfo6)
(package dbus-daemon)
(package python3-launchpadlib)
(package python3-jwt)
(package liblerc4)
(package libproxy1v5)
(package python3-idna)
(package init)
(package node-type-check)
(package libnspr4)
(package python3-filelock)
(package libmagickcore-6.q16-6-extra)
(package libmnl0)
(package piuparts-common)
(package libmysofa1)
(package libsidplay2)
(package node-neo-async)
(package node-union-value)
(package libdolphinvcs5)
(package python3-fuse)
(package libsnapd-glib-2-1)
(package opensc)
(package libpulse-mainloop-glib0)
(package libcairo-script-interpreter2)
(package libibverbs1)
(package node-slash)
(package libmusicbrainz5cc2v5)
(package libpython3.10-stdlib)
(package python3-jaraco.classes)
(package libkf5threadweaver5)
(package libxkbregistry0)
(package libxcb-image0)
(package libkf5mime-data)
(package cron)
(package libgcr-base-3-1)
(package libdecor-0-plugin-1-cairo)
(package xfonts-75dpi)
(package mmdebstrap)
(package pkexec)
(package libkf5webengineviewer5abi1)
(package diffstat)
(package libsord-0-0)
(package node-color-name)
(package libtag1v5)
(package libkf5konq6)
(package libsbc1)
(package node-text-table)
(package libaprutil1-ldap)
(package node-brace-expansion)
(package media-player-info)
(package node-colors)
(package libapparmor1)
(package libseccomp2)
(package node-json-schema)
(package kactivities-bin)
(package libsasl2-modules-kdexoauth2)
(package xserver-xorg-video-nouveau)
(package libkf5globalaccelprivate5)
(package libfontconfig1)
(package libucx0)
(package libzzip-0-13)
(package genisoimage)
(package libguard-perl)
(package libpangoft2-1.0-0)
(package libgfortran-12-dev)
(package libkf5newstuff-data)
(package ubuntu-dev-tools)
(package kde-style-oxygen-qt5)
(package libnfs13)
(package libmfx1)
(package python3-dbus)
(package node-to-regex-range)
(package poppler-data)
(package libgitlab-api-v4-perl)
(package libkf5sonnet5-data)
(package libqt5quicktemplates2-5)
(package libncursesw6)
(package node-defined)
(package node-spdx-correct)
(package libkf5authcore5)
(package libjpeg-dev)
(package libtiff6)
(package dolphin)
(package liblog-log4perl-perl)
(package libxs-parse-sublike-perl)
(package mupdf-tools)
(package libxstring-perl)
(package python3-secretstorage)
(package libarchive-cpio-perl)
(package vlc-plugin-video-output)
(package libdvdnav4)
(package hicolor-icon-theme)
(package konqueror)
(package node-regenerate)
(package libebml5)
(package libsub-install-perl)
(package wbrazilian)
(package gpg-wks-server)
(package libqmobipocket2)
(package gir1.2-malcontent-0)
(package libkf5syntaxhighlighting-data)
(package libc6)
(package sensible-utils)
(package node-postcss-value-parser)
(package cdrdao)
(package libsratom-0-0)
(package node-normalize-path)
(package libcdio-paranoia2)
(package libapr1)
(package pocketsphinx-en-us)
(package liborc-0.4-0)
(package node-graceful-fs)
(package pci.ids)
(package bluedevil)
(package kinfocenter)
(package acpi)
(package kross)
(package libdbusmenu-glib4)
(package libfile-homedir-perl)
(package libnfnetlink0)
(package qt5-gtk-platformtheme)
(package xclip)
(package libgtk-4-1)
(package python3-docutils)
(package libdebconfclient0)
(package software-properties-common)
(package kitty)
(package libglib-perl)
(package llvm-14-dev)
(package node-mkdirp)
(package realmd)
(package cron-daemon-common)
(package kwin-x11)
(package debian-faq)
(package cowdancer)
(package libva-drm2)
(package libkwineffects14)
(package libpopt0)
(package kmail)
(package node-esquery)
(package sane-airscan)
(package firmware-linux-free)
(package libxcvt0)
(package fonts-noto-hinted)
(package libkf5kdelibs4support-data)
(package libc-ares2)
(package libqt5quick5)
(package libkf5sonnetui5)
(package texlive-latex-extra)
(package libfile-dirlist-perl)
(package libudfread0)
(package libzmq5)
(package libminizip1)
(package kde-spectacle)
(package libqt5svg5)
(package libsasl2-2)
(package r-base-dev)
(package texlive-plain-generic)
(package liblognorm5)
(package cryptsetup-bin)
(package libcarp-clan-perl)
(package xserver-xorg)
(package libkf5khtml-bin)
(package libexif12)
(package node-rimraf)
(package node-npm-package-arg)
(package libxtables12)
(package libzip4)
(package bzr)
(package qml-module-org-kde-kwindowsystem)
(package firmware-linux-nonfree)
(package libqca-qt5-2-plugins)
(package libgl1)
(package node-prelude-ls)
(package libspatialaudio0)
(package node-isarray)
(package x11-common)
(package discover)
(package libole-storage-lite-perl)
(package pigz)
(package libavif15)
(package ruby-tty-which)
(package netpbm)
(package qml-module-qtqml-models2)
(package libcjson1)
(package libkf5xmlgui-data)
(package fonts-texgyre-math)
(package libkf5newstuff5)
(package libijs-0.35)
(package libc6-dev)
(package libkf5pty-data)
(package node-levn)
(package python3.10)
(package libkf5archive5)
(package pinentry-qt)
(package python3-cffi-backend)
(package zathura)
(package libstoken1)
(package libsamplerate0)
(package gnome-keyring)
(package libcolord2)
(package libpostproc56)
(package libjs-inherits)
(package libkf5khtml-data)
(package libkf5package-data)
(package node-defaults)
(package libkolabxml1v5)
(package gfortran-12)
(package dracut-core)
(package libkf5akonadicontact5)
(package python3-pynvim)
(package binutils)
(package libspecio-perl)
(package r-cran-nlme)
(package wget)
(package khotkeys)
(package grep)
(package libnewt0.52)
(package libboost-locale1.74.0)
(package libupower-glib3)
(package node-delegates)
(package kio-extras-data)
(package libunistring2)
(package python3-typing-extensions)
(package libswscale6)
(package libksysguardsensorfaces1)
(package xsettingsd)
(package firmware-qcom-soc)
(package libkf5mimetreeparser5abi1)
(package python3.11-dev)
(package pim-sieve-editor)
(package libkf5kcmutils-bin)
(package krb5-locales)
(package libthai0)
(package bzip2)
(package libsigc++-2.0-0v5)
(package libxcb-dri3-0)
(package libjcat1)
(package libcurl4)
(package python3-msgpack)
(package libjemalloc2)
(package libkf5kontactinterface5)
(package librest-1.0-0)
(package ncurses-bin)
(package liblist-someutils-xs-perl)
(package python3-gi)
(package libeot0)
(package cups-bsd)
(package node-regenerator-runtime)
(package libaribb24-0)
(package node-lodash)
(package fonts-urw-base35)
(package libblockdev-swap2)
(package virtualenv)
(package python3-urllib3)
(package node-pascalcase)
(package node-js-tokens)
(package libvdpau-va-gl1)
(package liburcu8)
(package node-es-abstract)
(package python3-pcre)
(package libcdio-cdda2)
(package libwebpmux3)
(package libwpe-1.0-1)
(package modemmanager)
(package libyaml-libyaml-perl)
(package xdg-desktop-portal)
(package ssl-cert)
(package libunibilium4)
(package libkf5grantleetheme-plugins)
(package emacsen-common)
(package r-cran-mgcv)
(package node-tap)
(package libpcre2-16-0)
(package libqt5multimediawidgets5)
(package node-wcwidth.js)
(package libdpkg-perl)
(package podman-compose)
(package node-caniuse-lite)
(package libucx-dev)
(package libgnome-bg-4-2)
(package libio-stringy-perl)
(package node-fs.realpath)
(package libblockdev-part-err2)
(package libgoa-1.0-common)
(package bogofilter-common)
(package liblayershellqtinterface5)
(package libmime-lite-perl)
(package qml-module-org-kde-kitemmodels)
(package task-brazilian-portuguese)
(package libkf5libkleo5)
(package ifupdown)
(package network-manager-gnome)
(package libkf5libkdepim-data)
(package libavahi-core7)
(package libnet-smtp-ssl-perl)
(package mobile-broadband-provider-info)
(package node-kind-of)
(package ruby-tilt)
(package neovim)
(package python3-pyrsistent)
(package node-readdirp)
(package qml-module-qtgraphicaleffects)
(package libhogweed6)
(package ruby-nanoc-deploying)
(package libkf5imap5)
(package libndctl6)
(package suckless-tools)
(package gir1.2-glib-2.0)
(package libhyphen0)
(package libpwquality1)
(package libmldbm-perl)
(package less)
(package libanyevent-perl)
(package webp-pixbuf-loader)
(package libmplex2-2.1-0)
(package libavahi-client3)
(package sed)
(package libuv1)
(package libuv1-dev)
(package node-ieee754)
(package fwupd-amd64-signed)
(package fontconfig-config)
(package bash-completion)
(package libcamd2)
(package libio-interactive-perl)
(package libjson-c5)
(package libgweather-4-0)
(package node-js-yaml)
(package python3-rfc3987)
(package node-progress)
(package libkf5ksieveui5)
(package node-cli-truncate)
(package gir1.2-packagekitglib-1.0)
(package libkf5newstuffwidgets5)
(package khotkeys-data)
(package libmad0)
(package libmhash2)
(package libsqlite3-0)
(package libvdpau1)
(package manpages-pt-br)
(package libkf5kiogui5)
(package libjim0.81)
(package libfido2-1)
(package util-linux-locales)
(package node-loader-runner)
(package libossp-uuid-perl)
(package libappstream4)
(package node-optimist)
(package libgav1-1)
(package libkf5kiocore5)
(package node-err-code)
(package liborcus-0.17-0)
(package libjson-glib-1.0-0)
(package libxmu6)
(package node-function-bind)
(package po-debconf)
(package libinstpatch-1.0-2)
(package libitm1)
(package libneon27-gnutls)
(package liblocale-gettext-perl)
(package libruby)
(package libgtk-3-common)
(package sudo)
(package libgs-common)
(package zlib1g-dev)
(package libkrb5support0)
(package libmagic-mgc)
(package libfile-chdir-perl)
(package node-shebang-command)
(package nanoc)
(package libwant-perl)
(package accountsservice)
(package intltool-debian)
(package x11-utils)
(package node-unicode-match-property-ecmascript)
(package libhttp-tiny-multipart-perl)
(package libqt5sql5-sqlite)
(package libieee1284-3)
(package liblz4-1)
(package node-data-uri-to-buffer)
(package lxc)
(package libsocket6-perl)
(package libqt5waylandcompositor5)
(package libwebrtc-audio-processing1)
(package libsbuild-perl)
(package login)
(package python3-uno)
(package logsave)
(package bsdutils)
(package dmraid)
(package gimp-data)
(package xdg-utils)
(package node-ansi-styles)
(package libreoffice-common)
(package r-doc-html)
(package gettext-base)
(package node-globals)
(package libheif1)
(package libpotrace0)
(package node-eslint-scope)
(package libcrypt-rc4-perl)
(package gvfs-daemons)
(package libkpimitinerary-data)
(package libxml-parser-perl)
(package libreoffice-base-core)
(package node-es-module-lexer)
(package libusb-1.0-0)
(package scrot)
(package kde-config-sddm)
(package node-slice-ansi)
(package liborcus-parser-0.17-0)
(package llvm-14-runtime)
(package node-balanced-match)
(package bolt)
(package libtime-duration-perl)
(package firmware-ast)
(package libqt5quickwidgets5)
(package ruby-slim)
(package libfeature-compat-class-perl)
(package liberror-perl)
(package libc-bin)
(package node-through)
(package xserver-xorg-video-vesa)
(package libsensors5)
(package python3-soupsieve)
(package libkf5dav5)
(package node-uuid)
(package pinentry-gnome3)
(package golang-src)
(package libnetpbm11)
(package libsereal-decoder-perl)
(package node-is-glob)
(package libgvpr2)
(package node-eslint-utils)
(package librhash0)
(package libjcode-pm-perl)
(package node-esutils)
(package node-mime)
(package libgtop2-common)
(package dahdi-firmware-nonfree)
(package python3-tdb)
(package qttranslations5-l10n)
(package wireless-regdb)
(package libopenconnect5)
(package alsa-utils)
(package libopus0)
(package libgoa-1.0-0b)
(package libhttp-message-perl)
(package python3-requests-toolbelt)
(package qml-module-org-kde-quickcharts)
(package x11-xkb-utils)
(package buildah)
(package libgmp-dev)
(package libnet-netmask-perl)
(package libvisual-0.4-0)
(package sgml-base)
(package xserver-xorg-input-wacom)
(package libzstd1)
(package libjson-perl)
(package nano)
(package libkf5baloo5)
(package libencode-locale-perl)
(package node-path-type)
(package libkf5jobwidgets-data)
(package node-semver)
(package r-recommended)
(package python3-github)
(package libdebconf-kde1)
(package node-rechoir)
(package libfishcamp1)
(package bluetooth)
(package ruby-kramdown)
(package zenity-common)
(package rubygems-integration)
(package smartmontools)
(package libwps-0.4-4)
(package plzip)
(package libcanberra0)
(package apt-utils)
(package man-db)
(package layer-shell-qt)
(package libdate-manip-perl)
(package node-debug)
(package node-jest-worker)
(package libdaxctl1)
(package libcolorcorrect5)
(package at-spi2-core)
(package libshine3)
(package im-config)
(package libphonon4qt5-data)
(package libhfstospell11)
(package libkpimgapi-data)
(package lsof)
(package kate5-data)
(package wmdocker)
(package libuno-cppuhelpergcc3-3)
(package libpam-cgfs)
(package ruby-pastel)
(package gettext)
(package libnet-ipv6addr-perl)
(package libkf5akonadi-data)
(package webpack)
(package kwalletmanager)
(package libkf5calendarutils5)
(package python3-blinker)
(package thin-provisioning-tools)
(package ucf)
(package libmoox-aliases-perl)
(package pciutils)
(package node-minimist)
(package os-prober)
(package kaccounts-providers)
(package libqt5qmlworkerscript5)
(package libkf5pty5)
(package xserver-xorg-core)
(package libefivar1)
(package libbit-vector-perl)
(package k3b-data)
(package khelpcenter)
(package libreadline-dev)
(depends-on 0ad-data-common fonts-dejavu-core)
(depends-on 0ad-data-common fonts-freefont-ttf)
(depends-on 0ad-data-common fonts-texgyre)
(depends-on 0ad-data-common tex-gyre)
(depends-on 0ad-data-common ttf-dejavu-core)
(depends-on 0ad-data-common ttf-freefont)
(depends-on 0ad-data-common dpkg)
(depends-on tex-gyre tex-common)
(depends-on tex-gyre xfonts-utils)
(depends-on tex-common ucf)
(depends-on tex-common dpkg)
(depends-on ucf coreutils)
(depends-on ucf debconf)
(depends-on ucf sensible-utils)
(depends-on coreutils libacl1)
(depends-on coreutils libattr1)
(depends-on coreutils libc6)
(depends-on coreutils libgmp10)
(depends-on coreutils libselinux1)
(depends-on libacl1 libc6)
(depends-on libc6 libcrypt1)
(depends-on libc6 libgcc-s1)
(depends-on libcrypt1 libc6)
(depends-on libgcc-s1 gcc-12-base)
(depends-on libgcc-s1 libc6)
(depends-on libattr1 libc6)
(depends-on libgmp10 libc6)
(depends-on libselinux1 libc6)
(depends-on libselinux1 libpcre2-8-0)
(depends-on libpcre2-8-0 libc6)
(depends-on debconf perl-base)
(depends-on perl-base dpkg)
(depends-on perl-base libc6)
(depends-on perl-base libcrypt1)
(depends-on dpkg tar)
(depends-on dpkg libbz2-1.0)
(depends-on dpkg libc6)
(depends-on dpkg liblzma5)
(depends-on dpkg libselinux1)
(depends-on dpkg libzstd1)
(depends-on dpkg zlib1g)
(depends-on tar libacl1)
(depends-on tar libc6)
(depends-on tar libselinux1)
(depends-on libbz2-1.0 libc6)
(depends-on liblzma5 libc6)
(depends-on libzstd1 libc6)
(depends-on zlib1g libc6)
(depends-on xfonts-utils libc6)
(depends-on xfonts-utils libfontenc1)
(depends-on xfonts-utils libfreetype6)
(depends-on xfonts-utils x11-common)
(depends-on xfonts-utils xfonts-encodings)
(depends-on xfonts-utils zlib1g)
(depends-on libfontenc1 libc6)
(depends-on libfontenc1 zlib1g)
(depends-on libfreetype6 libbrotli1)
(depends-on libfreetype6 libc6)
(depends-on libfreetype6 libpng16-16)
(depends-on libfreetype6 zlib1g)
(depends-on libbrotli1 libc6)
(depends-on libpng16-16 libc6)
(depends-on libpng16-16 zlib1g)
(depends-on x11-common lsb-base)
(depends-on xfonts-encodings x11-common)
(installed dbus-user-session)
(installed colord)
(installed dracut)
(installed libgstreamer-plugins-bad1.0-0)
(installed libopenal1)
(installed kcalc)
(installed python3-html5lib)
(installed libalgorithm-merge-perl)
(installed libsynctex2)
(installed javascript-common)
(installed frameworkintegration)
(installed node-clone)
(installed libevent-2.1-7)
(installed libfile-fcntllock-perl)
(installed debian-keyring)
(installed libatk-bridge2.0-0)
(installed libqt5waylandclient5)
(installed libcanberra-gtk3-0)
(installed node-yargs)
(installed libcom-err2)
(installed libsrt1.5-gnutls)
(installed libx11-protocol-perl)
(installed python3-cryptography)
(installed libpcre32-3)
(installed apt-config-icons-large-hidpi)
(installed libhttp-parser2.9)
(installed python3-pyparsing)
(installed libe-book-0.1-1)
(installed libkf5notifications-data)
(installed node-got)
(installed libcupsfilters1)
(installed libkwinglutils14)
(installed node-source-list-map)
(installed libkf5attica5)
(installed qml-module-qt-labs-qmlmodels)
(installed libhwloc-dev)
(installed libkf5service5)
(installed node-xtend)
(installed libxfixes3)
(installed libkf5completion5)
(installed libwww-mechanize-perl)
(installed docbook-dsssl)
(installed node-inherits)
(installed docbook-utils)
(installed libxcb-dri2-0)
(installed libsereal-encoder-perl)
(installed libjpeg62-turbo-dev)
(installed libxcb-xfixes0)
(installed node-opener)
(installed tex-common)
(installed libsys-cpuaffinity-perl)
(installed libvulkan1)
(installed libvlc5)
(installed sonnet-plugins)
(installed xdg-dbus-proxy)
(installed libfl2)
(installed libsphinxbase3)
(installed libperl4-corelibs-perl)
(installed fonts-noto-ui-core)
(installed r-cran-foreign)
(installed node-set-value)
(installed libkf5i18nlocaledata5)
(installed libduktape207)
(installed python3-pyqt5)
(installed firmware-cavium)
(installed libumfpack5)
(installed node-cacache)
(installed shim-signed-common)
(installed node-yargs-parser)
(installed breeze-icon-theme)
(installed node-fancy-log)
(installed xxd)
(installed libgdata22)
(installed liba52-0.7.4)
(installed node-json-buffer)
(installed libgstreamer-gl1.0-0)
(installed x11-apps)
(installed libayatana-appindicator3-1)
(installed node-sellside-emitter)
(installed tmate)
(installed node-is-extglob)
(installed python3-roman)
(installed libpng16-16)
(installed plasma-systemmonitor)
(installed python3-yaml)
(installed libfakechroot)
(installed node-get-stream)
(installed psmisc)
(installed firmware-samsung)
(installed libdevmapper1.02.1)
(installed xserver-xorg-input-libinput)
(installed libkf5config-data)
(installed libsub-exporter-progressive-perl)
(installed wireplumber)
(installed openjdk-17-jre-headless)
(installed git-buildpackage)
(installed kdepim-themeeditors)
(installed libfdisk1)
(installed gnustep-base-common)
(installed shared-mime-info)
(installed xfonts-base)
(installed openssh-client)
(installed update-inetd)
(installed libkf5syntaxhighlighting5)
(installed node-ansi-escapes)
(installed libgdbm-compat4)
(installed mesa-va-drivers)
(installed python3-distro)
(installed util-linux)
(installed libblockdev-fs2)
(installed perl-modules-5.36)
(installed ruby-tty-command)
(installed libvidstab1.1)
(installed accountwizard)
(installed libkf5solid5)
(installed libtext-iconv-perl)
(installed libwww-perl)
(installed node-has-values)
(installed libqt5qml5)
(installed polkit-kde-agent-1)
(installed wmctrl)
(installed task-brazilian-portuguese-kde-desktop)
(installed libpcsclite1)
(installed libibus-1.0-5)
(installed python3-dotenv)
(installed gir1.2-gtk-3.0)
(installed libaacs0)
(installed node-resumer)
(installed libgit2-1.5)
(installed libxcb-render0)
(installed libprotobuf-lite32)
(installed libgck-1-0)
(installed qml-module-org-kde-prison)
(installed schroot)
(installed libdevel-caller-perl)
(installed taskwarrior)
(installed desktop-base)
(installed libctf-nobfd0)
(installed texlive-latex-base)
(installed libkf5i18n-data)
(installed libdeflate0)
(installed juk)
(installed firmware-brcm80211)
(installed node-object-assign)
(installed gstreamer1.0-gl)
(installed libdevel-callchecker-perl)
(installed libgs10-common)
(installed libqmi-utils)
(installed locales)
(installed libtevent0)
(installed kitty-terminfo)
(installed libxv1)
(installed libegl-mesa0)
(installed golang-github-containers-image)
(installed doc-debian)
(installed libxmlrpc-lite-perl)
(installed asciidoctor)
(installed dmeventd)
(installed node-mimic-response)
(installed libkf5completion-data)
(installed libkf5iconthemes-bin)
(installed libfont-afm-perl)
(installed libkf5jsapi5)
(installed node-fs-readdir-recursive)
(installed libsdl1.2debian)
(installed libde265-0)
(installed libkf5mailtransportakonadi5)
(installed texlive-science)
(installed libsquashfuse0)
(installed libkf5textwidgets-data)
(installed libqt5widgets5)
(installed hwloc-nox)
(installed firmware-qlogic)
(installed libdrm-nouveau2)
(installed ruby-execjs)
(installed libgtk-4-bin)
(installed qml-module-org-kde-userfeedback)
(installed libzmf-0.0-0)
(installed libgfapi0)
(installed libkf5xmlgui-bin)
(installed whiptail)
(installed libtext-markdown-discount-perl)
(installed uuid-runtime)
(installed libkf5waylandclient5)
(installed libtinfo-dev)
(installed node-resolve)
(installed libmtp9)
(installed libgtk2.0-bin)
(installed librav1e0)
(installed libdecor-0-0)
(installed docbook-xsl)
(installed libkf5balooengine5)
(installed qml-module-org-kde-kquickcontrolsaddons)
(installed plasma-discover-backend-fwupd)
(installed libgnomekbd8)
(installed libmbim-proxy)
(installed libkf5modemmanagerqt6)
(installed libnet-domain-tld-perl)
(installed python3-magic)
(installed p7zip-full)
(installed libgtk2.0-0)
(installed libvorbisenc2)
(installed libmypaint-1.5-1)
(installed qml-module-qtquick-particles2)
(installed libwmf-0.2-7)
(installed knotes)
(installed sgml-data)
(installed libqt5sql5-mysql)
(installed node-doctrine)
(installed libmwaw-0.3-3)
(installed libqt5concurrent5)
(installed libx11-data)
(installed libsigsegv2)
(installed gwenview)
(installed libffi-dev)
(installed libcolamd2)
(installed mbox-importer)
(installed mutter-common)
(installed containernetworking-plugins)
(installed python3-zipp)
(installed libtommath1)
(installed kpeople-vcard)
(installed qml-module-qtquick2)
(installed node-prr)
(installed libexttextcat-data)
(installed node-get-caller-file)
(installed libsubid4)
(installed libblockdev2)
(installed libkfontinst5)
(installed golang-github-containers-common)
(installed libpci3)
(installed gdb-minimal)
(installed libqrtr-glib0)
(installed fonts-quicksand)
(installed laptop-detect)
(installed libcrypt-dev)
(installed libxft2)
(installed node-diff)
(installed smbclient)
(installed llvm-14-tools)
(installed node-fetch)
(installed libgnutls30)
(installed libperl5.36)
(installed phonon4qt5-backend-vlc)
(installed qml-module-qtquick-window2)
(installed libkf5config-bin)
(installed libqt5designer5)
(installed gcc-12-base)
(installed texlive-extra-utils)
(installed shellcheck)
(installed gstreamer1.0-plugins-good)
(installed node-make-dir)
(installed avahi-daemon)
(installed libnode-dev)
(installed konq-plugins)
(installed equivs)
(installed libpng-tools)
(installed adwaita-icon-theme)
(installed systemd-coredump)
(installed libkdecorations2private9)
(installed xserver-xorg-video-amdgpu)
(installed aspell-pt-br)
(installed alsa-topology-conf)
(installed libxcb-record0)
(installed libstd-rust-dev)
(installed dns-root-data)
(installed libglvnd0)
(installed node-tslib)
(installed at-spi2-common)
(installed node-end-of-stream)
(installed node-ws)
(installed python3-deprecated)
(installed libmpcdec6)
(installed libmailtools-perl)
(installed libqt5quickcontrols2-5)
(installed t1utils)
(installed libportaudio2)
(installed node-normalize-package-data)
(installed libio-pty-perl)
(installed node-ssri)
(installed kscreen)
(installed node-lodash-packages)
(installed libsemanage2)
(installed node-unicode-match-property-value-ecmascript)
(installed libreadline8)
(installed qml-module-org-kde-kholidays)
(installed libdrm-common)
(installed node-merge-stream)
(installed node-regenerator-transform)
(installed libquadmath0)
(installed cloud-image-utils)
(installed firmware-ipw2x00)
(installed python3-jeepney)
(installed pkgconf-bin)
(installed gnome-control-center)
(installed kmailtransport-akonadi)
(installed libmouse-perl)
(installed node-mixin-deep)
(installed node-source-map-support)
(installed libflatpak0)
(installed desktop-file-utils)
(installed libtermkey1)
(installed libpoppler-cpp0v5)
(installed python3-grpcio)
(installed libkf5itemmodels5)
(installed lxcfs)
(installed libz3-4)
(installed devscripts)
(installed npm)
(installed debconf-kde-data)
(installed libfontbox-java)
(installed libkf5configwidgets5)
(installed gpg-wks-client)
(installed libcrypt1)
(installed perl-base)
(installed node-osenv)
(installed libpython3.11-minimal)
(installed libblas-dev)
(installed libsnappy1v5)
(installed libevent-pthreads-2.1-7)
(installed libgimp2.0)
(installed libgps28)
(installed libtcl8.6)
(installed gtk2-engines-pixbuf)
(installed libdistro-info-perl)
(installed libindi-data)
(installed lmodern)
(installed usb-modeswitch-data)
(installed node-read-pkg)
(installed libcodec2-1.0)
(installed node-eslint-visitor-keys)
(installed qml-module-qtquick-controls)
(installed libstdc++6)
(installed libcap2-bin)
(installed liblab-gamut1)
(installed libkpimgapicontacts5)
(installed libcurl3-nss)
(installed libclone-perl)
(installed libexiv2-27)
(installed libharfbuzz-icu0)
(installed liblua5.2-0)
(installed fonts-font-awesome)
(installed python3-chardet)
(installed pinentry-curses)
(installed python3.10-minimal)
(installed node-babel-plugin-add-module-exports)
(installed node-ansi-regex)
(installed libyaml-0-2)
(installed plasma-nm)
(installed libkf5configwidgets-data)
(installed libkf5syndication5abi1)
(installed anacron)
(installed libreoffice-impress)
(installed qml-module-org-kde-kirigami2)
(installed libkf5ksieve-data)
(installed ruby-webrick)
(installed gnome-desktop3-data)
(installed plasma-thunderbolt)
(installed libcairo-perl)
(installed node-npmlog)
(installed liblzma-dev)
(installed libc-devtools)
(installed oxygen-sounds)
(installed firmware-siano)
(installed signon-plugin-oauth2)
(installed kate)
(installed ibus-gtk3)
(installed xmlto)
(installed malcontent)
(installed plasma-discover)
(installed ruby-mini-portile2)
(installed libmodplug1)
(installed libsub-exporter-perl)
(installed libdevel-stacktrace-perl)
(installed libslurm37)
(installed libgcab-1.0-0)
(installed tcl8.6)
(installed libportaudiocpp0)
(installed file)
(installed libaa1)
(installed libkf5globalaccel-data)
(installed dbus-system-bus-common)
(installed asciidoc-base)
(installed python3-ldb)
(installed libnova-0.16-0)
(installed node-cli-boxes)
(installed gstreamer1.0-plugins-bad)
(installed libllvm14)
(installed libmatroska7)
(installed inetutils-telnet)
(installed google-chrome-stable)
(installed libksysguardsensors1)
(installed libclass-c3-perl)
(installed libimport-into-perl)
(installed libkf5kcmutils5)
(installed node-inflight)
(installed libdw1)
(installed node-serialize-javascript)
(installed libpam-gnome-keyring)
(installed cups-filters)
(installed liblua5.4-0)
(installed libfl-dev)
(installed i3blocks)
(installed libtry-tiny-perl)
(installed milou)
(installed cups-common)
(installed libwildmidi2)
(installed texlive-luatex)
(installed texlive-binaries)
(installed kwayland-data)
(installed node-babel7)
(installed gnome-remote-desktop)
(installed libkf5messageviewer5abi1)
(installed libwebp7)
(installed libkf5declarative-data)
(installed node-postcss-modules-extract-imports)
(installed libnet-http-perl)
(installed ksystemstats)
(installed libglusterfs0)
(installed kalendarac)
(installed libcanberra-pulse)
(installed libkf5eventviews-data)
(installed libmodule-scandeps-perl)
(installed node-strip-json-comments)
(installed libvariable-magic-perl)
(installed ruby-dev)
(installed xml-core)
(installed libflashrom1)
(installed libfreerdp-server2-2)
(installed libkf5contacteditor5)
(installed libfreeaptx0)
(installed libpam0g)
(installed libdbus-glib-1-2)
(installed python3-typer)
(installed autopoint)
(installed libnuma1)
(installed opensc-pkcs11)
(installed cups-daemon)
(installed libk5crypto3)
(installed libpdfbox-java)
(installed node-wide-align)
(installed asciidoc-common)
(installed firefox-esr)
(installed libavc1394-0)
(installed libgupnp-dlna-2.0-4)
(installed linux-libc-dev)
(installed libgudev-1.0-0)
(installed libmpich-dev)
(installed libnice10)
(installed libassuan0)
(installed texlive-bibtex-extra)
(installed nftables)
(installed kgamma5)
(installed pipewire-pulse)
(installed libkf5akonadisearch-bin)
(installed netcat-traditional)
(installed libqt5positioning5)
(installed gir1.2-harfbuzz-0.0)
(installed libflac12)
(installed debconf-kde-helper)
(installed debconf)
(installed haveged)
(installed libkf5iconthemes5)
(installed kwrite)
(installed netbase)
(installed node-string-decoder)
(installed iso-codes)
(installed dosfstools)
(installed libxshmfence1)
(installed libgd3)
(installed libkf5textwidgets5)
(installed installation-report)
(installed pcscd)
(installed libcairo2)
(installed libkf5krossui5)
(installed node-babel-helper-define-polyfill-provider)
(installed libkf5kmanagesieve5)
(installed curl)
(installed libbrotli1)
(installed node-commander)
(installed node-jsonify)
(installed libmagickwand-6.q16-6)
(installed libstring-license-perl)
(installed llvm-14)
(installed libmoo-perl)
(installed xz-utils)
(installed firmware-netxen)
(installed libxau6)
(installed tzdata)
(installed libjs-async)
(installed libmng1)
(installed libglx0)
(installed lp-solve)
(installed node-es6-error)
(installed python3-debianbts)
(installed qml-module-qtquick-controls2)
(installed ruby-ddplugin)
(installed python3-markdown)
(installed libfile-which-perl)
(installed qml-module-qtquick-privatewidgets)
(installed libqt5test5)
(installed libkf5jobwidgets5)
(installed libpng-dev)
(installed libkf5wallet5)
(installed gnupg-l10n)
(installed kmod)
(installed qml-module-qtmultimedia)
(installed node-randombytes)
(installed python3-webencodings)
(installed python3-patiencediff)
(installed libz3-dev)
(installed libvlccore9)
(installed libgtk2.0-common)
(installed node-quick-lru)
(installed plymouth-label)
(installed node-wrappy)
(installed libmpdec3)
(installed python3-six)
(installed sddm)
(installed libkf5cddb5)
(installed libblockdev-loop2)
(installed ipp-usb)
(installed node-abbrev)
(installed asciidoc-dblatex)
(installed libkf5runner5)
(installed libgvc6)
(installed libuno-cppu3)
(installed libxkbfile1)
(installed node-set-blocking)
(installed libasyncns0)
(installed libspectre1)
(installed plymouth)
(installed i965-va-driver)
(installed libxcb-glx0)
(installed python3-tomlkit)
(installed node-promise-inflight)
(installed xbitmaps)
(installed libconfuse2)
(installed libpython3-dev)
(installed libemail-date-format-perl)
(installed libimath-3-1-29)
(installed node-archy)
(installed node-flat-cache)
(installed libkpimitinerary5)
(installed gpgv)
(installed drkonqi)
(installed node-camelcase)
(installed qml-module-org-kde-solid)
(installed node-console-control-strings)
(installed libsane-common)
(installed libffi8)
(installed cups-ipp-utils)
(installed libgnome-desktop-4-2)
(installed libk3b-extracodecs)
(installed libtwolame0)
(installed libvte-2.91-common)
(installed python3-dockerpty)
(installed python3-virtualenv)
(installed imagemagick)
(installed python3-minimal)
(installed libspa-0.2-bluetooth)
(installed mesa-vdpau-drivers)
(installed node-cjs-module-lexer)
(installed bluez-obexd)
(installed popularity-contest)
(installed libkf5akonadinotes5)
(installed libqt5multimedia5)
(installed node-interpret)
(installed libraptor2-0)
(installed libpackage-stash-xs-perl)
(installed libtext-xslate-perl)
(installed librygel-server-2.8-0)
(installed libdebhelper-perl)
(installed libkf5configgui5)
(installed libgraphene-1.0-0)
(installed node-decamelize)
(installed libxcb-present0)
(installed exfatprogs)
(installed node-resolve-from)
(installed python3-importlib-metadata)
(installed kmenuedit)
(installed libkeyutils1)
(installed libqt5quickparticles5)
(installed liblockfile1)
(installed liblapack-dev)
(installed libnode108)
(installed libplymouth5)
(installed libzxing2)
(installed libkf5pimcommonakonadi5abi1)
(installed mdadm)
(installed libjs-jquery)
(installed libkf5kcmutilscore5)
(installed libkf5calendarsupport-data)
(installed node-regexpu-core)
(installed python3-unidiff)
(installed libhtml-tokeparser-simple-perl)
(installed xserver-xorg-video-ati)
(installed ruby-public-suffix)
(installed libkf5widgetsaddons5)
(installed eslint)
(installed ibus)
(installed libxxf86dga1)
(installed unzip)
(installed build-essential)
(installed libdbusmenu-qt5-2)
(installed libkf5screen-data)
(installed libobjc4)
(installed libsnmp-base)
(installed libavahi-common-data)
(installed libexpat1)
(installed libkf5incidenceeditor-data)
(installed python3-pygments)
(installed libparted-fs-resize0)
(installed libkuserfeedbackcore1)
(installed libpcre2-posix3)
(installed libndp0)
(installed libkf5parts-data)
(installed node-wordwrap)
(installed node-arrify)
(installed python3-talloc)
(installed g++-12)
(installed kitty-shell-integration)
(installed libkpimsmtp5abi1)
(installed libparams-validationcompiler-perl)
(installed python3-pkg-resources)
(installed htop)
(installed node-regexpp)
(installed libatk-wrapper-java-jni)
(installed libavformat59)
(installed libcapture-tiny-perl)
(installed libregexp-pattern-license-perl)
(installed libpython3-stdlib)
(installed libintl-perl)
(installed libnma-gtk4-0)
(installed libluajit-5.1-common)
(installed libpackagekitqt5-1)
(installed libpam-kwallet5)
(installed python3-distlib)
(installed libspeechd2)
(installed libdata-messagepack-perl)
(installed node-json-schema-traverse)
(installed libkf5pimtextedit-plugins)
(installed libva2)
(installed runc)
(installed libvo-amrwbenc0)
(installed libkaccounts2)
(installed libgee-0.8-2)
(installed libqt5texttospeech5)
(installed libxcb-composite0)
(installed libgfxdr0)
(installed powerdevil-data)
(installed python3-more-itertools)
(installed debsums)
(installed libraw1394-11)
(installed fakechroot)
(installed r-cran-class)
(installed libfcgi0ldbl)
(installed qml-module-org-kde-kquickcontrols)
(installed readline-common)
(installed gpgsm)
(installed libruby3.0)
(installed libx11-6)
(installed libxcb-cursor0)
(installed node-util-deprecate)
(installed qml-module-qtwebengine)
(installed librdf0)
(installed libgstreamer1.0-0)
(installed kio)
(installed python3.10-venv)
(installed xsltproc)
(installed iucode-tool)
(installed libatkmm-1.6-1v5)
(installed libxinerama1)
(installed node-which)
(installed libkf5peoplewidgets5)
(installed libfakekey0)
(installed libqt5networkauth5)
(installed coinor-libclp-dev)
(installed libkf5parts-plugins)
(installed python3)
(installed unar)
(installed libslirp0)
(installed libregexp-pattern-perl)
(installed hunspell-en-us)
(installed libapt-pkg-perl)
(installed libuuid1)
(installed libkf5akonadisearch-data)
(installed libargon2-1)
(installed node-strip-ansi)
(installed python3-wadllib)
(installed node-string-width)
(installed libccid)
(installed libfuse2)
(installed libcap-ng0)
(installed libmime-types-perl)
(installed libcloudproviders0)
(installed libkf5mailtransport-data)
(installed libjson-glib-1.0-common)
(installed libupnp13)
(installed libksgrd9)
(installed libclass-inspector-perl)
(installed kpackagetool5)
(installed libkf5bookmarks5)
(installed xcvt)
(installed check-dfsg-status)
(installed node-define-properties)
(installed black)
(installed node-core-js-pure)
(installed libxpm4)
(installed sysvinit-utils)
(installed schroot-common)
(installed libva-x11-2)
(installed libkf5service-data)
(installed dnsmasq-base)
(installed libapache-pom-java)
(installed libio-string-perl)
(installed libsoundtouch1)
(installed coinor-libcbc-dev)
(installed node-map-visit)
(installed node-write-file-atomic)
(installed libfile-find-rule-perl)
(installed libxcb-render-util0)
(installed zsh)
(installed libbpf0)
(installed growisofs)
(installed librevenge-0.0-0)
(installed libncurses-dev)
(installed libkpimpkpass5)
(installed libdrm-intel1)
(installed libgdata-common)
(installed python3-olefile)
(installed xdg-desktop-portal-gtk)
(installed libtheora0)
(installed kde-cli-tools-data)
(installed libatomic1)
(installed python3-distutils)
(installed r-cran-cluster)
(installed libreoffice-writer)
(installed libdvbpsi10)
(installed python-apt-common)
(installed dput)
(installed libjs-regenerate)
(installed sshfs)
(installed libkf5plasma5)
(installed libqrencode4)
(installed libphonon4qt5-4)
(installed dconf-cli)
(installed libflac++10)
(installed libnss-myhostname)
(installed librdkafka1)
(installed libvorbis0a)
(installed k3b)
(installed node-core-js-compat)
(installed node-resolve-cwd)
(installed libva-wayland2)
(installed adduser)
(installed gstreamer1.0-plugins-base)
(installed libxcb-dpms0)
(installed libtss2-esys-3.0.2-0)
(installed gpg-agent)
(installed tini)
(installed libaspell15)
(installed texlive-xetex)
(installed ca-certificates-java)
(installed libestr0)
(installed libpagemaker-0.0-0)
(installed node-memfs)
(installed libsys-hostname-long-perl)
(installed libtss2-rc0)
(installed cups-core-drivers)
(installed libxml-libxml-perl)
(installed libsbig4)
(installed fonts-texgyre)
(installed libteamdctl0)
(installed libqt5multimediagsttools5)
(installed libsoup-3.0-common)
(installed libunwind8)
(installed lynx-common)
(installed node-strip-bom)
(installed libcolorhug2)
(installed libamd2)
(installed dblatex)
(installed libao-common)
(installed ruby3.1-doc)
(installed libopengl0)
(installed libterm-readkey-perl)
(installed libkf5gravatar-data)
(installed libxml-namespacesupport-perl)
(installed libsnmp40)
(installed node-babel-plugin-lodash)
(installed libhttp-date-perl)
(installed libparams-classify-perl)
(installed libkf5unitconversion-data)
(installed libclang1-14)
(installed libnsl-dev)
(installed usr-is-merged)
(installed qml-module-qtquick-dialogs)
(installed dragonplayer)
(installed libauthen-sasl-perl)
(installed libtext-glob-perl)
(installed libfont-ttf-perl)
(installed node-postcss-modules-values)
(installed socat)
(installed python3-semver)
(installed node-object-visit)
(installed liblist-compare-perl)
(installed libtexluajit2)
(installed libthai-data)
(installed libkf5auth5)
(installed apt)
(installed libnumbertext-data)
(installed python3-setuptools)
(installed gcc-12)
(installed python3-fastbencode)
(installed libspandsp2)
(installed libwpg-0.3-3)
(installed libblockdev-utils2)
(installed libkf5texteditor-bin)
(installed node-ajv)
(installed texlive-publishers)
(installed libunicode-map-perl)
(installed libzvbi0)
(installed libgpg-error0)
(installed libldacbt-abr2)
(installed libgc1)
(installed autodep8)
(installed libhttp-cookies-perl)
(installed dblatex-doc)
(installed node-core-js)
(installed libmp3lame0)
(installed libogg0)
(installed node-hosted-git-info)
(installed libkimageannotator-common)
(installed liblqr-1-0)
(installed libtext-levenshteinxs-perl)
(installed cmake-data)
(installed libvo-aacenc0)
(installed liblouisutdml9)
(installed libdiscover2)
(installed iproute2)
(installed libkf5messagecomposer5abi1)
(installed qml-module-org-kde-bluezqt)
(installed libmariadb3)
(installed libxi6)
(installed libruby3.1)
(installed node-mute-stream)
(installed udisks2)
(installed docker.io)
(installed libpipeline1)
(installed node-auto-bind)
(installed libstring-crc32-perl)
(installed liblcms2-2)
(installed node-anymatch)
(installed node-fast-levenshtein)
(installed libset-intspan-perl)
(installed node-babel7-runtime)
(installed plasma-dataengines-addons)
(installed gnome-icon-theme)
(installed libselinux1)
(installed plasma-framework)
(installed libgrantlee-templates5)
(installed ibrazilian)
(installed apache2-bin)
(installed libkf5people-data)
(installed libnet-ssleay-perl)
(installed node-repeat-string)
(installed nodejs)
(installed wpasupplicant)
(installed libtasn1-6)
(installed r-base-core)
(installed python3-dev)
(installed libappstreamqt2)
(installed cracklib-runtime)
(installed libkf5akonadimime5)
(installed node-is-descriptor)
(installed libkf5kontactinterface-data)
(installed pbuilder)
(installed libspreadsheet-parseexcel-perl)
(installed libctf0)
(installed libxss1)
(installed cowbuilder)
(installed node-ajv-keywords)
(installed firefox-esr-l10n-pt-br)
(installed libixml10)
(installed libsrtp2-1)
(installed node-assert)
(installed gnome-control-center-data)
(installed libtss2-tcti-device0)
(installed libkf5kdelibs4support5)
(installed tipa)
(installed node-watchpack)
(installed libgstreamer-plugins-base1.0-0)
(installed libkf5akonadicalendar-data)
(installed libnm0)
(installed libinput10)
(installed node-is-plain-object)
(installed libkf5iconthemes-data)
(installed node-commondir)
(installed sweeper)
(installed ruby-brandur-json-schema)
(installed node-pify)
(installed tcl)
(installed libflite1)
(installed libfftw3-double3)
(installed libmtp-common)
(installed qml-module-qt-labs-folderlistmodel)
(installed xdelta3)
(installed gnustep-common)
(installed cups-ppdc)
(installed ruby-erubis)
(installed node-is-primitive)
(installed ruby-hamster)
(installed vrms)
(installed libwbclient0)
(installed python3-pip-whl)
(installed node-p-cancelable)
(installed libkf5messagecore5abi1)
(installed libqalculate-data)
(installed libalgorithm-diff-xs-perl)
(installed libdc1394-25)
(installed libfeature-compat-try-perl)
(installed libkf5calendarcore5abi2)
(installed exim4-config)
(installed node-promise-retry)
(installed libavutil57)
(installed plasma-disks)
(installed samba-common)
(installed ruby-zeitwerk)
(installed libkf5mailimporterakonadi5)
(installed manpages-dev)
(installed netavark)
(installed libqt5multimedia5-plugins)
(installed libspeex1)
(installed amd64-microcode)
(installed node-spdx-expression-parse)
(installed qml-module-qtwebkit)
(installed libtypes-serialiser-perl)
(installed libconfig-tiny-perl)
(installed libasound2-data)
(installed dash)
(installed libxml-sax-base-perl)
(installed ruby-tty-platform)
(installed libqmi-glib5)
(installed pulseaudio-module-bluetooth)
(installed node-unicode-property-aliases-ecmascript)
(installed libpam-kwallet-common)
(installed libdav1d6)
(installed konsole-kpart)
(installed libqt5xml5)
(installed libkf5coreaddons-data)
(installed libarray-intspan-perl)
(installed passwd)
(installed qemu-utils)
(installed libkpipewire5)
(installed libvorbisfile3)
(installed libpcrecpp0v5)
(installed imagemagick-6-common)
(installed node-cli-table)
(installed libkf5configqml5)
(installed pass)
(installed gnome-keyring-pkcs11)
(installed ruby3.1-dev)
(installed libplacebo208)
(installed libpkcs11-helper1)
(installed libglib-object-introspection-perl)
(installed libnumbertext-1.0-0)
(installed abntex)
(installed libsemanage-common)
(installed ruby-nanoc-cli)
(installed libadwaita-1-0)
(installed libdmraid1.0.0.rc16)
(installed libsignon-qt5-1)
(installed containerd)
(installed python3-toml)
(installed libcddb2)
(installed systemsettings)
(installed qml-module-org-kde-syntaxhighlighting)
(installed libaliased-perl)
(installed libfreehand-0.1-1)
(installed libglib2.0-0)
(installed libxslt1.1)
(installed libdb5.3)
(installed node-braces)
(installed libcdio19)
(installed python3.11-minimal)
(installed libgbm1)
(installed libgs10)
(installed node-deep-is)
(installed kdepim-addons)
(installed tasksel-data)
(installed libpoppler126)
(installed kde-config-gtk-style)
(installed libjson-maybexs-perl)
(installed libkf5js5)
(installed node-chokidar)
(installed node-signal-exit)
(installed kde-style-breeze)
(installed libboost-thread1.74.0)
(installed libgnome-rr-4-2)
(installed node-escodegen)
(installed libkf5guiaddons-data)
(installed dbus-session-bus-common)
(installed node-once)
(installed qml-module-org-kde-purpose)
(installed rtkit)
(installed libtexlua53-5)
(installed node-spdx-exceptions)
(installed xfonts-100dpi)
(installed libspa-0.2-modules)
(installed dpkg-dev)
(installed node-pkg-dir)
(installed task-brazilian-portuguese-desktop)
(installed libcholmod3)
(installed libkfontinstui5)
(installed usbutils)
(installed python3-certifi)
(installed appstream)
(installed node-chownr)
(installed libncurses6)
(installed binutils-x86-64-linux-gnu)
(installed texlive-fonts-recommended)
(installed liblxc-common)
(installed libgsound0)
(installed apt-listchanges)
(installed libkf5dbusaddons-bin)
(installed python3-gpg)
(installed libkwalletbackend5-5)
(installed libkf5texteditor5)
(installed libkf5akonadisearchdebug5)
(installed node-busboy)
(installed r-cran-kernsmooth)
(installed libkf5akonadicalendar5abi1)
(installed node-growl)
(installed libopenexr-3-1-30)
(installed firmware-atheros)
(installed libbluetooth3)
(installed libetonyek-0.1-1)
(installed libtss2-tctildr0)
(installed kup-backup)
(installed libre-engine-re2-perl)
(installed ruby-slow-enumerator-tools)
(installed libblas3)
(installed libgl1-mesa-dri)
(installed libindirect-perl)
(installed ruby-haml)
(installed node-argparse)
(installed libqt5webchannel5)
(installed libanyevent-i3-perl)
(installed ghostscript)
(installed libmpeg2encpp-2.1-0)
(installed libqt5webview5)
(installed node-ci-info)
(installed mesa-vulkan-drivers)
(installed cpp)
(installed fonts-noto-unhinted)
(installed libmypaint-common)
(installed libserd-0-0)
(installed ruby-sexp-processor)
(installed node-read-package-json)
(installed libxcb-xinerama0)
(installed bridge-utils)
(installed node-terser)
(installed kde-config-screenlocker)
(installed libproxy-tools)
(installed node-functional-red-black-tree)
(installed libswresample4)
(installed node-acorn)
(installed node-icss-utils)
(installed va-driver-all)
(installed libasync-interrupt-perl)
(installed libxerces-c3.2)
(installed libjbig0)
(installed libepubgen-0.1-1)
(installed python3-debconf)
(installed console-setup)
(installed python3-attr)
(installed ruby-concurrent)
(installed ca-certificates)
(installed gvfs)
(installed qml-module-org-kde-ksysguard)
(installed libfribidi0)
(installed python3-fastimport)
(installed qtwayland5)
(installed libqt5script5)
(installed libreoffice-style-breeze)
(installed libqt5network5)
(installed node-async-each)
(installed cups)
(installed libkf5pimtextedit5abi2)
(installed libaccounts-glib0)
(installed libavfilter8)
(installed libshout3)
(installed xinit)
(installed libxcb-damage0)
(installed discover-data)
(installed colord-data)
(installed ruby-thor)
(installed sgmlspl)
(installed wl-clipboard)
(installed kde-config-updates)
(installed libiw30)
(installed r-cran-lattice)
(installed node-webpack-sources)
(installed libiso9660-11)
(installed fonts-lmodern)
(installed libreoffice-l10n-pt-br)
(installed libssh-4)
(installed python3.11)
(installed node-fast-deep-equal)
(installed node-readable-stream)
(installed gstreamer1.0-plugins-ugly)
(installed libparams-util-perl)
(installed kde-config-mailtransport)
(installed libpathplan4)
(installed bind9-libs)
(installed libb2-1)
(installed libpaper-utils)
(installed libxdamage1)
(installed node-esrecurse)
(installed libkf5akonadisearch-plugins)
(installed libksba8)
(installed python3-json-pointer)
(installed libkdecorations2-5v5)
(installed libnma-common)
(installed libqt5x11extras5)
(installed bluez-firmware)
(installed libgnome-desktop-3-20)
(installed libsgmls-perl)
(installed media-types)
(installed node-base64-js)
(installed node-decompress-response)
(installed fwupd)
(installed libnamespace-autoclean-perl)
(installed libiterator-util-perl)
(installed libkf5bookmarks-data)
(installed llvm-14-linker-tools)
(installed libksysguardformatter1)
(installed ktexteditor-katepart)
(installed libsdl2-2.0-0)
(installed libsignon-plugins-common1)
(installed sound-theme-freedesktop)
(installed plasma-workspace-data)
(installed fonts-noto-ui-extra)
(installed libavahi-glib1)
(installed docker-compose)
(installed libaccountsservice0)
(installed ibus-gtk)
(installed libostyle1c2)
(installed libkf5pulseaudioqt3)
(installed libsane1)
(installed ruby-maruku)
(installed libkf5notifyconfig5)
(installed libdv4)
(installed libkf5pimcommon5abi2)
(installed libmalcontent-ui-1-1)
(installed jq)
(installed libasound2-plugins)
(installed libnet1)
(installed libopenh264-7)
(installed libxmlsec1-nss)
(installed plasma-desktop)
(installed binutils-common)
(installed libjs-prettify)
(installed libtaskmanager6abi1)
(installed apg)
(installed libslang2)
(installed python3-ubuntutools)
(installed kdeplasma-addons-data)
(installed libmpfr6)
(installed libkf5style5)
(installed libgsm1)
(installed libpath-iterator-rule-perl)
(installed akonadi-contacts-data)
(installed libobject-pad-perl)
(installed libgprofng0)
(installed libkf5i18n5)
(installed python3-pysimplesoap)
(installed upower)
(installed libaudio2)
(installed libbsd0)
(installed libmodule-find-perl)
(installed libaprutil1-dbd-sqlite3)
(installed libjbig2dec0)
(installed libsasl2-modules-db)
(installed ruby-mime-types)
(installed libgdbm6)
(installed ispell)
(installed libgts-bin)
(installed libkf5calendarevents5)
(installed libemail-address-xs-perl)
(installed libkf5dbusaddons5)
(installed libreoffice-math)
(installed libu2f-udev)
(installed fonts-symbola)
(installed liblouisutdml-bin)
(installed libnl-route-3-200)
(installed libqt5quickshapes5)
(installed python3-setuptools-whl)
(installed liblz1)
(installed ixo-usb-jtag)
(installed libgtop-2.0-11)
(installed libconfuse-common)
(installed linux-image-amd64)
(installed libpciaccess0)
(installed qml-module-qtquick-virtualkeyboard)
(installed libkf5kirigami2-5)
(installed dmidecode)
(installed libproc2-0)
(installed libexception-class-perl)
(installed libnetaddr-ip-perl)
(installed node-collection-visit)
(installed libnss3)
(installed libimagequant0)
(installed kuserfeedback-doc)
(installed python3-pil)
(installed libcrack2)
(installed liblxc1)
(installed libsecret-common)
(installed libkf5parts5)
(installed libcolord-gtk4-1)
(installed libcgraph6)
(installed libomp-14-dev)
(installed bc)
(installed libmaxminddb0)
(installed node-espree)
(installed node-is-number)
(installed rofi)
(installed python3-configobj)
(installed libfreetype6)
(installed node-validate-npm-package-name)
(installed libimlib2)
(installed libphonenumber8)
(installed libipc-run-perl)
(installed rake)
(installed zlib1g)
(installed rpcsvc-proto)
(installed libpam-modules-bin)
(installed node-path-exists)
(installed ruby-terser)
(installed xkb-data)
(installed sddm-theme-debian-maui)
(installed python3-grpc-tools)
(installed systemd-sysv)
(installed libgegl-0.4-0)
(installed libb-hooks-op-check-perl)
(installed libpackage-stash-perl)
(installed libegl1)
(installed libhunspell-1.7-0)
(installed procps)
(installed libepoxy0)
(installed licensecheck)
(installed gfortran)
(installed libwacom-common)
(installed ruby-xmlrpc)
(installed libqt5webengine-data)
(installed libmodule-implementation-perl)
(installed libtdb1)
(installed libweather-ion7)
(installed libaudit1)
(installed libbox2d2)
(installed kdepim-runtime)
(installed libcommon-sense-perl)
(installed libnl-genl-3-200)
(installed libkf5holidays5)
(installed libxmuu1)
(installed libuno-sal3)
(installed coinor-libcgl1)
(installed xfonts-scalable)
(installed libltdl7)
(installed uno-libs-private)
(installed libsasl2-modules)
(installed libipc-shareable-perl)
(installed preview-latex-style)
(installed libresid-builder0c2a)
(installed libgweather-4-common)
(installed node-y18n)
(installed libgmp10)
(installed ruby-pkg-config)
(installed libdevel-size-perl)
(installed reportbug)
(installed libldb2)
(installed kpartx)
(installed libpython3.10-minimal)
(installed qml-module-org-kde-newstuff)
(installed libfuse3-3)
(installed xserver-common)
(installed libeditorconfig0)
(installed libip4tc2)
(installed mpich)
(installed libxcb-randr0)
(installed fonts-gfs-baskerville)
(installed libxml-twig-perl)
(installed node-is-stream)
(installed dunst)
(installed libcanberra-gtk3-module)
(installed console-setup-linux)
(installed coreutils)
(installed e2fsprogs)
(installed libspreadsheet-writeexcel-perl)
(installed ruby-mime-types-data)
(installed node-babel-plugin-polyfill-regenerator)
(installed libevdev2)
(installed i3status)
(installed libunbound8)
(installed node-object-inspect)
(installed fonts-droid-fallback)
(installed fonts-hack)
(installed glib-networking)
(installed golang-go)
(installed librsvg2-2)
(installed libqt5webkit5)
(installed node-are-we-there-yet)
(installed ruby-tty-color)
(installed usbmuxd)
(installed libpcre3-dev)
(installed python3-ibus-1.0)
(installed xserver-xorg-video-radeon)
(installed libgusb2)
(installed libiscsi7)
(installed libconst-fast-perl)
(installed libkf5doctools5)
(installed libsub-quote-perl)
(installed powertop)
(installed libglib2.0-bin)
(installed iputils-ping)
(installed node-yallist)
(installed liburing2)
(installed kactivitymanagerd)
(installed libice6)
(installed libxcb-icccm4)
(installed node-tapable)
(installed libxrandr2)
(installed libstdc++-12-dev)
(installed vcdimager)
(installed libharfbuzz0b)
(installed libkf5libkdepim-plugins)
(installed libkf5service-bin)
(installed node-parse-json)
(installed node-retry)
(installed ruby-nokogiri)
(installed libgupnp-1.6-0)
(installed libdate-calc-xs-perl)
(installed libkf5sonnetcore5)
(installed node-del)
(installed bison)
(installed ruby-nanoc-core)
(installed libkf5libkleo-data)
(installed libvcdinfo0)
(installed teckit)
(installed node-escape-string-regexp)
(installed plasma-runners-addons)
(installed node-stack-utils)
(installed libboost-chrono1.74.0)
(installed libkf5identitymanagementwidgets5)
(installed libssl-dev)
(installed libmount1)
(installed node-picocolors)
(installed libgail18)
(installed libkf5kexiv2-15.0.0)
(installed libapt-pkg6.0)
(installed libossp-uuid16)
(installed plasma-pa)
(installed libsort-versions-perl)
(installed libcairo-gobject2)
(installed node-locate-path)
(installed xfonts-utils)
(installed pulseaudio-utils)
(installed liblwp-mediatypes-perl)
(installed libsmbclient)
(installed libstd-rust-1.63)
(installed node-ini)
(installed fxload)
(installed default-mysql-server-core)
(installed node-nopt)
(installed kamera)
(installed plasma-workspace)
(installed libmspub-0.1-1)
(installed libnettle8)
(installed libmodule-runtime-perl)
(installed avahi-autoipd)
(installed libmarkdown2)
(installed libparted2)
(installed qemu-block-extra)
(installed node-webassemblyjs)
(installed libmediaart-2.0-0)
(installed libkpimgapitasks5)
(installed gstreamer1.0-libav)
(installed ruby-rb-inotify)
(installed python3-uritemplate)
(installed texlive-lang-greek)
(installed vlc-plugin-base)
(installed node-shebang-regex)
(installed xserver-xorg-input-all)
(installed libdata-validate-uri-perl)
(installed firmware-zd1211)
(installed node-is-binary-path)
(installed libcfitsio10)
(installed node-is-plain-obj)
(installed libxcb-shape0)
(installed libmjpegutils-2.1-0)
(installed fuse-overlayfs)
(installed libept1.6.0)
(installed node-regjsparser)
(installed apt-cacher-ng)
(installed libexporter-tiny-perl)
(installed lsb-release)
(installed libfile-stripnondeterminism-perl)
(installed libunicode-linebreak-perl)
(installed linux-image-6.1.0-1-amd64)
(installed libqt5printsupport5)
(installed qml-module-org-kde-people)
(installed dwz)
(installed libkf5kiontlm5)
(installed libnotificationmanager1)
(installed libwinpr2-2)
(installed libtie-ixhash-perl)
(installed librist4)
(installed librsync2)
(installed debhelper)
(installed aptitude-common)
(installed python3-click)
(installed libnghttp2-14)
(installed node-log-driver)
(installed libosp5)
(installed needrestart)
(installed librabbitmq4)
(installed python3-debian)
(installed grub2-common)
(installed python3-pycurl)
(installed libgtk3-perl)
(installed libgtkmm-3.0-1v5)
(installed node-jsesc)
(installed xss-lock)
(installed libgraphite2-3)
(installed zstd)
(installed libcbor0.8)
(installed xbacklight)
(installed libnftables1)
(installed libgme0)
(installed iptables)
(installed gcr)
(installed libkf5bluezqt6)
(installed python3-docopt)
(installed timgm6mb-soundfont)
(installed fonts-gfs-porson)
(installed node-browserslist)
(installed node-errno)
(installed node-widest-line)
(installed udev)
(installed libkf5identitymanagement5)
(installed libkmod2)
(installed libopencore-amrwb0)
(installed libqt5gui5)
(installed libdatrie1)
(installed ruby-temple)
(installed libudisks2-0)
(installed libcdt5)
(installed gtk-update-icon-cache)
(installed libspeexdsp1)
(installed node-yaml)
(installed powerdevil)
(installed libkpimgapicalendar5)
(installed fonts-noto-cjk)
(installed gir1.2-gdkpixbuf-2.0)
(installed libfontembed1)
(installed mount)
(installed libgit-wrapper-perl)
(installed libokular5core10)
(installed libkf5holidays-data)
(installed libnamespace-clean-perl)
(installed python3-texttable)
(installed kwin-style-breeze)
(installed libkf5globalaccel-bin)
(installed node-depd)
(installed libnpth0)
(installed plasma-browser-integration)
(installed kio-ldap)
(installed ruby-colored)
(installed fonts-noto-color-emoji)
(installed keyboard-configuration)
(installed libscim8v5)
(installed libaprutil1)
(installed base-passwd)
(installed gnome-settings-daemon-common)
(installed libsvtav1enc1)
(installed libblockdev-crypto2)
(installed qml-module-gsettings1.0)
(installed libefiboot1)
(installed libjs-is-typedarray)
(installed libjsoncpp25)
(installed libkf5notifications5)
(installed libpolkit-agent-1-0)
(installed libbs2b0)
(installed libuno-salhelpergcc3-3)
(installed libopenal-data)
(installed libclass-method-modifiers-perl)
(installed linux-base)
(installed libpcre2-dev)
(installed libmalcontent-0-0)
(installed openssl)
(installed openjdk-17-jre)
(installed terser)
(installed firmware-libertas)
(installed node-for-own)
(installed libgsettings-qt1)
(installed node-path-dirname)
(installed network-manager)
(installed libsyntax-keyword-try-perl)
(installed libmtp-runtime)
(installed grub-efi-amd64-bin)
(installed libtss2-mu0)
(installed fuse3)
(installed software-properties-gtk)
(installed firmware-sof-signed)
(installed pbzip2)
(installed imagemagick-6.q16)
(installed ruby-builder)
(installed libzvbi-common)
(installed nodejs-doc)
(installed tk)
(installed libkf5people5)
(installed texlive-base)
(installed liblmdb0)
(installed kwin-data)
(installed liblzo2-2)
(installed libbluray2)
(installed gkbd-capplet)
(installed gnupg-utils)
(installed manpages)
(installed libomp-dev)
(installed pkg-config)
(installed node-css-selector-tokenizer)
(installed libkf5mailtransport5)
(installed openvpn)
(installed libc-dev-bin)
(installed firmware-misc-nonfree)
(installed libprocesscore9)
(installed liburi-perl)
(installed mariadb-client-core-10.6)
(installed node-jest-debbundle)
(installed libreadonly-perl)
(installed qml-module-qt-labs-platform)
(installed fonts-lato)
(installed libxcb-keysyms1)
(installed node-chrome-trace-event)
(installed cmake)
(installed linux-image-6.0.0-6-amd64)
(installed libgnutls-dane0)
(installed lua-lgi)
(installed node-set-immediate-shim)
(installed cargo)
(installed libcommons-parent-java)
(installed java-common)
(installed power-profiles-daemon)
(installed libpowerdevilui5)
(installed libkf5networkmanagerqt6)
(installed node-coveralls)
(installed libxcb-sync1)
(installed libsub-name-perl)
(installed xdelta)
(installed unattended-upgrades)
(installed libdbusmenu-gtk3-4)
(installed kf5-messagelib-data)
(installed libdata-optlist-perl)
(installed firmware-intel-sound)
(installed ibus-gtk4)
(installed libgsl27)
(installed libldap-2.5-0)
(installed python3-merge3)
(installed libkf5kiofilewidgets5)
(installed dpkg)
(installed node-esprima)
(installed node-process-nextick-args)
(installed cups-filters-core-drivers)
(installed libudev1)
(installed libges-1.0-0)
(installed r-cran-survival)
(installed pipewire-bin)
(installed p7zip)
(installed polkitd)
(installed light)
(installed systemd-timesyncd)
(installed libnss-mdns)
(installed tpm-udev)
(installed docbook-xml)
(installed node-babel-plugin-polyfill-corejs3)
(installed libcaca0)
(installed libdist-checkconflicts-perl)
(installed kdialog)
(installed publicsuffix)
(installed fakeroot)
(installed dmsetup)
(installed libkf5windowsystem5)
(installed libkf5prisonscanner5)
(installed libfile-fnmatch-perl)
(installed libwpebackend-fdo-1.0-1)
(installed libatk1.0-0)
(installed aptitude)
(installed libjq1)
(installed libyuv0)
(installed ppp)
(installed qrencode)
(installed libarchive-zip-perl)
(installed node-globby)
(installed libkf5khtml5)
(installed i3-wm)
(installed cryfs)
(installed libgmime-3.0-0)
(installed gyp)
(installed liblog-any-adapter-screen-perl)
(installed bup)
(installed python3-wrapt)
(installed gnome-settings-daemon)
(installed libkworkspace5-5)
(installed xournalpp)
(installed libx11-xcb1)
(installed libkrb5-3)
(installed shim-unsigned)
(installed sane-utils)
(installed node-read)
(installed libatopology2)
(installed python3-mypy-extensions)
(installed node-cli-cursor)
(installed libdrm2)
(installed libdrm-radeon1)
(installed libgupnp-igd-1.0-4)
(installed libnet-dbus-perl)
(installed libxcb-xrm0)
(installed liblc3-0)
(installed libref-util-perl)
(installed node-sprintf-js)
(installed libblockdev-part2)
(installed libcairo-gobject-perl)
(installed rygel)
(installed firmware-realtek)
(installed libboost-iostreams1.74.0)
(installed texlive-formats-extra)
(installed libxkbcommon-x11-0)
(installed libdevel-globaldestruction-perl)
(installed kaddressbook)
(installed libalgorithm-diff-perl)
(installed libdigest-perl-md5-perl)
(installed kbd)
(installed kio-extras)
(installed fonts-liberation)
(installed libkf5dbusaddons-data)
(installed libid3tag0)
(installed libxcomposite1)
(installed libfmt9)
(installed libabsl20220623)
(installed libstaroffice-0.0-0)
(installed fonts-noto)
(installed libip6tc2)
(installed libgphoto2-port12)
(installed perl-doc)
(installed piuparts)
(installed node-validate-npm-package-license)
(installed libgpgmepp6)
(installed libjs-underscore)
(installed libxml2)
(installed bluez)
(installed libpcre2-32-0)
(installed libsndio7.0)
(installed node-ip)
(installed autoconf)
(installed libxcb1)
(installed node-is-arrayish)
(installed glib-networking-common)
(installed libjpeg62-turbo)
(installed lsb-base)
(installed gir1.2-freedesktop)
(installed libelf1)
(installed lvm2)
(installed apt-config-icons)
(installed node-restore-cursor)
(installed libsepol2)
(installed libpython3.11)
(installed xserver-xorg-legacy)
(installed libkscreenlocker5)
(installed fonts-noto-mono)
(installed hwdata)
(installed libjansson4)
(installed gir1.2-ibus-1.0)
(installed libzbar0)
(installed libomp5-14)
(installed python3-pathspec)
(installed libgtk-4-common)
(installed libabw-0.1-1)
(installed libunicode-utf8-perl)
(installed libqt5help5)
(installed libxdelta2)
(installed libproc-processtable-perl)
(installed libstartup-notification0)
(installed libyaml-tiny-perl)
(installed pim-data-exporter)
(installed libgssdp-1.6-0)
(installed libkf5pimcommonautocorrection5)
(installed partitionmanager)
(installed qtchooser)
(installed gpgconf)
(installed libkpimgapicore5abi1)
(installed libenchant-2-2)
(installed libkf5pimcommon-data)
(installed libk3b8)
(installed xserver-xorg-video-vmware)
(installed libpipewire-0.3-0)
(installed libayatana-ido3-0.4-0)
(installed libtirpc-dev)
(installed node-promzard)
(installed poppler-utils)
(installed xserver-xorg-video-intel)
(installed libnetfilter-conntrack3)
(installed libxdmcp6)
(installed ibverbs-providers)
(installed node-npm-run-path)
(installed python3-dulwich)
(installed node-optionator)
(installed libkf5pimtextedit-data)
(installed libpskc0)
(installed automake)
(installed alsa-ucm-conf)
(installed libmm-glib0)
(installed cpio)
(installed libkf5package5)
(installed libberkeleydb-perl)
(installed liboxygenstyleconfig5-5)
(installed gvfs-common)
(installed libsmartcols1)
(installed libtomcrypt1)
(installed python3-reportbug)
(installed git)
(installed adequate)
(installed python3-apt)
(installed libxkbcommon0)
(installed liblouis-data)
(installed libubsan1)
(installed ripgrep)
(installed libkf5codecs5)
(installed plasma-desktop-data)
(installed node-tar)
(installed r-cran-spatial)
(installed libkgantt2)
(installed sysstat)
(installed libstrictures-perl)
(installed libkgantt2-l10n)
(installed firmware-ti-connectivity)
(installed libprotobuf32)
(installed librygel-core-2.8-0)
(installed usb.ids)
(installed liblirc-client0)
(installed libntlm0)
(installed python3-samba)
(installed python3-upstream-ontologist)
(installed python3-wheel-whl)
(installed libjs-sprintf-js)
(installed default-jre-headless)
(installed samba-common-bin)
(installed node-spdx-license-ids)
(installed libsub-override-perl)
(installed libpangoxft-1.0-0)
(installed ruby-rubypants)
(installed texlive-pictures)
(installed libgcr-ui-3-1)
(installed libextutils-depends-perl)
(installed node-ignore)
(installed tasksel)
(installed libkf5contacts-data)
(installed libjs-sphinxdoc)
(installed kded5)
(installed libpwquality-common)
(installed libkf5imap-data)
(installed node-clone-deep)
(installed libgts-0.7-5)
(installed bash)
(installed libicu72)
(installed golang-1.19-go)
(installed libcwidget4)
(installed libplasma-geolocation-interface5)
(installed node-is-buffer)
(installed libusbmuxd6)
(installed ruby-rouge)
(installed dictionaries-common)
(installed kwrited)
(installed libkimageannotator0)
(installed libraw20)
(installed qml-module-org-kde-draganddrop)
(installed texlive-lang-portuguese)
(installed bogofilter)
(installed libkf5akonadisearchpim5)
(installed conmon)
(installed libnftnl11)
(installed libpango-1.0-0)
(installed iw)
(installed libaccounts-qt5-1)
(installed node-encoding)
(installed node-electron-to-chromium)
(installed docutils-common)
(installed fonts-noto-core)
(installed libgslcblas0)
(installed libwavpack1)
(installed libcairomm-1.0-1v5)
(installed node-lru-cache)
(installed libkf5purpose-bin)
(installed samba-libs)
(installed librsvg2-common)
(installed coinor-libosi-dev)
(installed libtss2-tcti-swtpm0)
(installed libmime-tools-perl)
(installed hunspell-pt-br)
(installed libgcc-s1)
(installed dbus-bin)
(installed python3-requests)
(installed node-tap-mocha-reporter)
(installed qdbus-qt5)
(installed r-cran-boot)
(installed libxres1)
(installed node-p-limit)
(installed libcommons-logging-java)
(installed libxml2-dev)
(installed findutils)
(installed firmware-netronome)
(installed node-is-windows)
(installed libqpdf29)
(installed ncurses-base)
(installed libraqm0)
(installed libwayland-client0)
(installed libpolkit-gobject-1-0)
(installed qml-module-org-kde-pipewire)
(installed libregexp-assemble-perl)
(installed libio-sessiondata-perl)
(installed base-files)
(installed libnumber-compare-perl)
(installed libxdgutilsdesktopentry1.0.1)
(installed libxml-sax-expat-perl)
(installed python3-breezy)
(installed node-v8flags)
(installed exim4-daemon-light)
(installed libtalloc2)
(installed node-v8-compile-cache)
(installed net-tools)
(installed node-is-path-inside)
(installed libpackagekit-glib2-18)
(installed libipc-run3-perl)
(installed debian-archive-keyring)
(installed gsfonts)
(installed libgnome-bluetooth-ui-3.0-13)
(installed kinit)
(installed node-run-queue)
(installed libpod-constants-perl)
(installed libkf5tnef5)
(installed ffmpegthumbs)
(installed ed)
(installed libnotify4)
(installed kdeconnect)
(installed libwireplumber-0.4-0)
(installed fonts-noto-cjk-extra)
(installed libqmi-proxy)
(installed sbuild)
(installed libsidplay1v5)
(installed libgnustep-base1.28)
(installed node-write)
(installed node-for-in)
(installed libgmpxx4ldbl)
(installed libpcre3)
(installed r-cran-mass)
(installed librygel-db-2.8-0)
(installed ruby-ffi)
(installed libindidriver1)
(installed libpcap0.8)
(installed libqt5webenginecore5)
(installed libkf5grantleetheme5)
(installed libkf5mailcommon5abi2)
(installed libwebpdemux2)
(installed node-indent-string)
(installed libedit2)
(installed mailcap)
(installed libkf5dnssd5)
(installed qml-module-org-kde-activities)
(installed libkf5itemviews5)
(installed ruby-html2haml)
(installed ruby-rubygems)
(installed libfreerdp2-2)
(installed kfind)
(installed libvoikko1)
(installed p11-kit-modules)
(installed phonon4qt5)
(installed python3-colorama)
(installed liblockfile-bin)
(installed apt-rdepends)
(installed libdebuginfod-common)
(installed libssh2-1)
(installed ruby-multi-json)
(installed util-linux-extra)
(installed libdbus-1-3)
(installed task-laptop)
(installed brz-debian)
(installed libkf5configcore5)
(installed dirmngr)
(installed lzop)
(installed libgnomekbd-common)
(installed libpod-parser-perl)
(installed diffutils)
(installed node-supports-color)
(installed libfakeroot)
(installed libxatracker2)
(installed libgssglue1)
(installed vim-runtime)
(installed ksshaskpass)
(installed zathura-pdf-poppler)
(installed libgirepository-1.0-1)
(installed librygel-renderer-2.8-0)
(installed libtask-weaken-perl)
(installed mawk)
(installed libgcc-12-dev)
(installed python3-debmutate)
(installed libmbedx509-1)
(installed libkf5filemetadata-data)
(installed node-tape)
(installed ruby-net-telnet)
(installed fonts-fork-awesome)
(installed libpulse0)
(installed libkf5templateparser5)
(installed intel-microcode)
(installed libregexp-wildcards-perl)
(installed libkf5mailimporter5)
(installed libevent-core-2.1-7)
(installed libmagickcore-6.q16-6)
(installed libpoppler-glib8)
(installed kpackagelauncherqml)
(installed libfile-basedir-perl)
(installed libtimedate-perl)
(installed node-regjsgen)
(installed libprotobuf-c1)
(installed libgeoclue-2-0)
(installed libjson-xs-perl)
(installed libkf5windowsystem-data)
(installed libv4l-0)
(installed node-events)
(installed qml-module-org-kde-kio)
(installed dconf-service)
(installed libhtml-tree-perl)
(installed libidn2-0)
(installed python3-tr)
(installed distro-info)
(installed tree)
(installed libxklavier16)
(installed libass9)
(installed libeatmydata1)
(installed ocl-icd-libopencl1)
(installed libgoa-backend-1.0-1)
(installed policykit-1)
(installed libsmbios-c2)
(installed libdebuginfod1)
(installed libkf5kiowidgets5)
(installed libvolume-key1)
(installed liblist-utilsby-perl)
(installed libkf5akonadiwidgets5abi1)
(installed python3-platformdirs)
(installed eject)
(installed python3-lxml)
(installed python3-nacl)
(installed qml-module-org-kde-kconfig)
(installed node-core-util-is)
(installed libattr1)
(installed python3-lazr.uri)
(installed ruby-oj)
(installed libjs-source-map)
(installed libgsasl18)
(installed wireless-tools)
(installed libxmlsec1-openssl)
(installed libhtml-format-perl)
(installed libqgpgme15)
(installed logrotate)
(installed libkf5libkdepim5)
(installed libteckit0)
(installed akonadi-server)
(installed libpsl5)
(installed libcgi-fast-perl)
(installed lxc-templates)
(installed zsh-common)
(installed libfilesys-df-perl)
(installed libxcb-shm0)
(installed node-aproba)
(installed easy-rsa)
(installed libpcre2-8-0)
(installed liboxygenstyle5-5)
(installed node-undici)
(installed libkf5eventviews5abi1)
(installed node-has-unicode)
(installed libgssapi-krb5-2)
(installed zenity)
(installed libsort-naturally-perl)
(installed ruby-rdiscount)
(installed node-memory-fs)
(installed gnustep-base-runtime)
(installed libmanette-0.2-0)
(installed akonadi-backend-mysql)
(installed libipt2)
(installed libopenjp2-7)
(installed libnorm1)
(installed node-get-value)
(installed libxext6)
(installed libostree-1-1)
(installed x11-session-utils)
(installed libicu-dev)
(installed node-schema-utils)
(installed node-agent-base)
(installed node-path-is-inside)
(installed libhavege2)
(installed libkpmcore12)
(installed node-iconv-lite)
(installed python3-greenlet)
(installed libltdl-dev)
(installed libkf5guiaddons-bin)
(installed gimp)
(installed libqxp-0.0-0)
(installed python3-tornado)
(installed librole-tiny-perl)
(installed libkpipewirerecord5)
(installed node-builtins)
(installed libjack-jackd2-0)
(installed libpath-tiny-perl)
(installed libtk8.6)
(installed libgdk-pixbuf2.0-common)
(installed libre2-9)
(installed kimageformat-plugins)
(installed pipewire)
(installed libclass-data-inheritable-perl)
(installed libb-hooks-endofscope-perl)
(installed python3-docker)
(installed libp11-kit0)
(installed liblvm2cmd2.03)
(installed gnome-online-accounts)
(installed libfontenc1)
(installed dh-strip-nondeterminism)
(installed libfaad2)
(installed make)
(installed ruby-ruby-parser)
(installed icu-devtools)
(installed qt5-image-formats-plugins)
(installed kdegraphics-thumbnailers)
(installed libkf5crash5)
(installed enchant-2)
(installed libkf5xmlgui5)
(installed libtext-wrapi18n-perl)
(installed libxt6)
(installed libopenni2-0)
(installed libmime-charset-perl)
(installed libkf5dnssd-data)
(installed libtree-sitter0)
(installed librubberband2)
(installed libann0)
(installed r-cran-nnet)
(installed lynx)
(installed liblouis20)
(installed libiec61883-0)
(installed libpoppler-qt5-1)
(installed autopkgtest)
(installed node-wrap-ansi)
(installed ruby-addressable)
(installed node-debbundle-es-to-primitive)
(installed libxcursor1)
(installed node-cliui)
(installed python3-psycopg2)
(installed libpmem1)
(installed libpkgconf3)
(installed libopencore-amrnb0)
(installed libhttp-daemon-perl)
(installed graphviz)
(installed criu)
(installed libcpanel-json-xs-perl)
(installed libiterator-perl)
(installed librdkafka-dev)
(installed node-move-concurrently)
(installed libmbedcrypto7)
(installed libxcb-xkb1)
(installed debianutils)
(installed libkf5ldap-data)
(installed libkf5screen-bin)
(installed libkpathsea6)
(installed libqalculate22)
(installed libsub-identify-perl)
(installed libkf5widgetsaddons-data)
(installed traceroute)
(installed librdkafka++1)
(installed libreoffice-core)
(installed node-binary-extensions)
(installed libdmtx0b)
(installed liblog-dispatch-perl)
(installed grub-efi-amd64)
(installed libgeocode-glib-2-0)
(installed libgles2)
(installed libuno-purpenvhelpergcc3-3)
(installed ruby-diff-lcs)
(installed libx265-199)
(installed python3-xdg)
(installed polkitd-pkla)
(installed libmpg123-0)
(installed libuchardet0)
(installed fonts-liberation2)
(installed software-properties-kde)
(installed libnl-3-200)
(installed vim-tiny)
(installed bind9-dnsutils)
(installed firmware-bnx2)
(installed libkf5filemetadata-bin)
(installed libclucene-core1v5)
(installed libdvdread8)
(installed ruby-parallel)
(installed okular)
(installed task-desktop)
(installed node-estraverse)
(installed libzimg2)
(installed libexttextcat-2.0-0)
(installed libimobiledevice6)
(installed ruby-redcloth)
(installed libkate1)
(installed cups-client)
(installed libpam-systemd)
(installed libreoffice-calc)
(installed python3-webcolors)
(installed golang-1.19-src)
(installed cgroupfs-mount)
(installed libgfortran5)
(installed install-info)
(installed liblwp-protocol-https-perl)
(installed gnome-bluetooth-3-common)
(installed ruby-ddmetrics)
(installed libxcb-screensaver0)
(installed node-json5)
(installed texlive-latex-recommended)
(installed cryptsetup)
(installed libkf5globalaccel5)
(installed libsoxr0)
(installed libdjvulibre21)
(installed node-time-stamp)
(installed libyajl2)
(installed tk8.6)
(installed node-https-proxy-agent)
(installed libkf5akonadisearchxapian5)
(installed asciidoc)
(installed i3)
(installed node-tap-parser)
(installed init-system-helpers)
(installed libsombok3)
(installed libprocessui9)
(installed brz)
(installed plasma-wallpapers-addons)
(installed xserver-xorg-video-fbdev)
(installed bzr-builddeb)
(installed libwayland-cursor0)
(installed libtsan2)
(installed libkf5akonadicore5abi2)
(installed libdouble-conversion3)
(installed libmbedtls14)
(installed libldap-common)
(installed libfcgi-bin)
(installed node-gauge)
(installed gstreamer1.0-x)
(installed node-isobject)
(installed libkf5idletime5)
(installed python3-charset-normalizer)
(installed libparse-recdescent-perl)
(installed apt-file)
(installed libprocps8)
(installed libbdplus0)
(installed libref-util-xs-perl)
(installed libkf5purpose5)
(installed libwmflite-0.2-7)
(installed python3-oauthlib)
(installed node-ip-regex)
(installed libstring-shellquote-perl)
(installed ruby-mustache)
(installed libpipewire-0.3-modules)
(installed liblog-any-perl)
(installed libkf5akonadisearchcore5)
(installed libkf5su5)
(installed aspell)
(installed libxcb-res0)
(installed libkf5mailcommon-data)
(installed libhwy1)
(installed libcap2)
(installed libmutter-11-0)
(installed libpadwalker-perl)
(installed libqt5dbus5)
(installed python3-pyxattr)
(installed cpp-12)
(installed libgail-common)
(installed node-json-stable-stringify)
(installed libhwloc15)
(installed rustc)
(installed libatasmart4)
(installed node-pump)
(installed bubblewrap)
(installed apt-config-icons-large)
(installed shim-helpers-amd64-signed)
(installed libfile-desktopentry-perl)
(installed python3-httplib2)
(installed librdmacm1)
(installed libdjvulibre-text)
(installed libgnome-bluetooth-3.0-13)
(installed libgtk-3-bin)
(installed libtype-tiny-xs-perl)
(installed ruby-listen)
(installed libatk-wrapper-java)
(installed libpixman-1-0)
(installed librbd1)
(installed mokutil)
(installed node-negotiator)
(installed libonig5)
(installed libnotify-bin)
(installed glib-networking-services)
(installed libdata-dpath-perl)
(installed libkf5archive-data)
(installed node-npm-bundled)
(installed libgumbo1)
(installed node-define-property)
(installed libkf5quickaddons5)
(installed gnupg)
(installed ruby-memo-wise)
(installed liblist-someutils-perl)
(installed cdparanoia)
(installed node-isexe)
(installed r-cran-matrix)
(installed kdoctools5)
(installed fonts-opensymbol)
(installed catdoc)
(installed r-cran-codetools)
(installed xfonts-encodings)
(installed libayatana-indicator3-7)
(installed qml-module-org-kde-kcm)
(installed libdca0)
(installed libsecret-1-0)
(installed libsystemd-shared)
(installed dctrl-tools)
(installed node-glob-parent)
(installed libtool)
(installed malcontent-gui)
(installed patchutils)
(installed node-chalk)
(installed doxygen)
(installed libao4)
(installed libxmlsec1)
(installed firmware-realtek-rtl8723cs-bt)
(installed plasma-widgets-addons)
(installed coinor-libcoinmp1v5)
(installed node-p-map)
(installed ruby-redcarpet)
(installed libcryptsetup12)
(installed xorg)
(installed patch)
(installed python3-protobuf)
(installed baloo-kf5)
(installed node-regenerate-unicode-properties)
(installed libaudit-common)
(installed libkf5kdcraw5)
(installed libalgorithm-c3-perl)
(installed libbinutils)
(installed libvlc-bin)
(installed gir1.2-pango-1.0)
(installed libnma0)
(installed libsndfile1)
(installed libnsl2)
(installed firmware-bnx2x)
(installed libkf5calendarsupport5abi1)
(installed liblilv-0-0)
(installed libtss2-tcti-mssim0)
(installed libltc11)
(installed ruby3.1)
(installed libhtml-html5-entities-perl)
(installed fdisk)
(installed libhtml-form-perl)
(installed firmware-ath9k-htc)
(installed libchromaprint1)
(installed flex)
(installed breeze)
(installed pulseaudio)
(installed node-fill-range)
(installed python3-keyring)
(installed python3-pylibacl)
(installed libblkid1)
(installed libkf5kcmutils-data)
(installed node-deep-equal)
(installed libkf5baloowidgets-bin)
(installed libkf5codecs-data)
(installed node-is-path-cwd)
(installed coinor-libcoinutils3v5)
(installed zip)
(installed libio-socket-ssl-perl)
(installed node-uri-js)
(installed fonts-dejavu-extra)
(installed libmujs2)
(installed libjxr-tools)
(installed ruby-sass)
(installed libclucene-contribs1v5)
(installed node-source-map)
(installed libfile-touch-perl)
(installed libjavascriptcoregtk-4.1-0)
(installed libtext-charwidth-perl)
(installed libqt5core5a)
(installed libstring-copyright-perl)
(installed ruby-cri)
(installed libstring-escape-perl)
(installed libkf5newstuffcore5)
(installed plasma-vault)
(installed python3-lazr.restfulclient)
(installed libmsgpackc2)
(installed kde-cli-tools)
(installed libxcb-xinput0)
(installed libkf5contacts5)
(installed libinput-bin)
(installed libkf5coreaddons5)
(installed libxml2-utils)
(installed m4)
(installed k3b-i18n)
(installed arch-test)
(installed node-minipass)
(installed node-path-is-absolute)
(installed libgupnp-av-1.0-3)
(installed libkf5peoplebackend5)
(installed libacl1)
(installed libkf5notifyconfig-data)
(installed libplist3)
(installed perl)
(installed dbus)
(installed libxdgutilsbasedir1.0.1)
(installed synaptic)
(installed libgif7)
(installed breeze-cursor-theme)
(installed bzip2-doc)
(installed fzf)
(installed libpython3.11-stdlib)
(installed node-safe-buffer)
(installed liblouisutdml-data)
(installed node-lcov-parse)
(installed pavucontrol)
(installed libc-l10n)
(installed libgdk-pixbuf2.0-bin)
(installed libtag1v5-vanilla)
(installed libkf5dav-data)
(installed xdg-user-dirs)
(installed uidmap)
(installed libxrender1)
(installed racc)
(installed libqca-qt5-2)
(installed libperlio-gzip-perl)
(installed node-enhanced-resolve)
(installed node-n3)
(installed libcc1-0)
(installed libgpgme11)
(installed liblua5.3-0)
(installed coinor-libcoinutils-dev)
(installed libpulsedsp)
(installed libkuserfeedbackwidgets1)
(installed debconf-i18n)
(installed node-base)
(installed libsexp1)
(installed libxs-parse-keyword-perl)
(installed plasma-discover-common)
(installed libdata-dump-perl)
(installed libpangomm-1.4-1v5)
(installed korganizer)
(installed libapache2-mod-dnssd)
(installed libkpimaddressbookimportexport5)
(installed libxxf86vm1)
(installed isc-dhcp-client)
(installed ktexteditor-data)
(installed lintian)
(installed libdevmapper-event1.02.1)
(installed firmware-myricom)
(installed rsync)
(installed libglu1-mesa)
(installed libfstrm0)
(installed libtime-moment-perl)
(installed dex)
(installed ark)
(installed mariadb-server-core-10.6)
(installed breeze-gtk-theme)
(installed ruby-progressbar)
(installed usb-modeswitch)
(installed libqt5virtualkeyboard5)
(installed firmware-amd-graphics)
(installed node-fs-write-stream-atomic)
(installed libkf5plasmaquick5)
(installed node-async)
(installed libfile-mimeinfo-perl)
(installed node-css-loader)
(installed qml-module-qtquick-layouts)
(installed libssh-gcrypt-4)
(installed xserver-xorg-video-all)
(installed gir1.2-atk-1.0)
(installed libeval-closure-perl)
(installed qml-module-qt-labs-settings)
(installed libconvert-binhex-perl)
(installed libclass-xsaccessor-perl)
(installed libxtst6)
(installed libhttp-negotiate-perl)
(installed libkf5prison5)
(installed geoclue-2.0)
(installed python3-pyqt5.sip)
(installed akregator)
(installed libpocketsphinx3)
(installed hostname)
(installed libdata-validate-domain-perl)
(installed libidn12)
(installed libpq5)
(installed coinor-libosi1v5)
(installed libkf5messagelist5abi1)
(installed node-p-locate)
(installed node-is-typedarray)
(installed xorg-docs-core)
(installed libcdparanoia0)
(installed gdisk)
(installed libgomp1)
(installed libxmlb2)
(installed libepub0)
(installed libkf5sysguard-data)
(installed libmythes-1.2-0)
(installed intel-media-va-driver)
(installed indi-dsi)
(installed dh-autoreconf)
(installed libigdgmm12)
(installed libreoffice-draw)
(installed libxaw7)
(installed libkf5mime5abi1)
(installed isync)
(installed node-is-extendable)
(installed libfile-listing-perl)
(installed w3m)
(installed wdiff)
(installed handlebars)
(installed libaio1)
(installed libpgm-5.3-0)
(installed python3-distro-info)
(installed libnotmuch5)
(installed libcdr-0.1-1)
(installed libkf5declarative5)
(installed libtirpc3)
(installed libglibmm-2.4-1v5)
(installed shim-signed)
(installed python3-dns)
(installed node-color-convert)
(installed libdrm-amdgpu1)
(installed libstemmer0d)
(installed libtirpc-common)
(installed grub-efi-amd64-signed)
(installed liblzma5)
(installed libmd0)
(installed openjade)
(installed hdmi2usb-fx2-firmware)
(installed libpowerdevilcore2)
(installed librtmp1)
(installed node-lowercase-keys)
(installed gawk)
(installed libqaccessibilityclient-qt5-0)
(installed kwin-common)
(installed libjxr0)
(installed libfwupd2)
(installed libgcrypt20)
(installed libkf5activitiesstats1)
(installed ruby-asciidoctor)
(installed libxcb-ewmh2)
(installed autotools-dev)
(installed debootstrap)
(installed libsystemd0)
(installed rsyslog)
(installed node-unset-value)
(installed libaom3)
(installed libxml-sax-perl)
(installed librasqal3)
(installed libio-html-perl)
(installed libxapian30)
(installed lua-luv)
(installed libjxl0.7)
(installed python3-tomli)
(installed kaddressbook-data)
(installed libtss2-tcti-cmd0)
(installed node-micromatch)
(installed libopenmpt-modplug1)
(installed neovim-runtime)
(installed python3-lib2to3)
(installed libmbim-utils)
(installed python3-websocket)
(installed kitty-doc)
(installed libsoup2.4-common)
(installed node-gyp)
(installed libwrap0)
(installed libmpc3)
(installed libluajit-5.1-2)
(installed libbz2-1.0)
(installed libjs-typedarray-to-buffer)
(installed libical3)
(installed cups-server-common)
(installed gzip)
(installed libkf5akonadiagentbase5)
(installed parted)
(installed ranger)
(installed abook)
(installed libldacbt-enc2)
(installed ntfs-3g)
(installed libmetis5)
(installed libtype-tiny-perl)
(installed libwebkit2gtk-4.1-0)
(installed node-babel-plugin-polyfill-corejs2)
(installed bup-doc)
(installed libbz2-dev)
(installed libpaper1)
(installed node-has-value)
(installed bsd-mailx)
(installed libboost-program-options1.74.0)
(installed libgfrpc0)
(installed libx264-164)
(installed node-unique-filename)
(installed libavahi-common3)
(installed node-error-ex)
(installed libkf5kdelibs4support5-bin)
(installed libwayland-egl1)
(installed libpfm4)
(installed python3-software-properties)
(installed neomutt)
(installed libtokyocabinet9)
(installed ruby-nanoc-checking)
(installed libkf5auth-data)
(installed libwoff1)
(installed qml-module-qtquick-shapes)
(installed libccolamd2)
(installed libkf5activities5)
(installed libntirpc-dev)
(installed libgpm2)
(installed apparmor)
(installed node-cache-base)
(installed libkf5incidenceeditor5abi1)
(installed libsoup-3.0-0)
(installed eatmydata)
(installed libkf5guiaddons5)
(installed libkf5ksieve5)
(installed libglib2.0-data)
(installed libappimage1.0abi1)
(installed libmagic1)
(installed libopenmpt0)
(installed libqt5webengine5)
(installed libkf5su-bin)
(installed fonts-noto-extra)
(installed libkf5su-data)
(installed libdata-validate-ip-perl)
(installed libio-prompter-perl)
(installed node-concat-stream)
(installed libvisio-0.1-1)
(installed libkf5akonadiprivate5abi2)
(installed libhtml-parser-perl)
(installed node-punycode)
(installed libptexenc1)
(installed node-minimatch)
(installed tex-gyre)
(installed gsasl-common)
(installed libasound2)
(installed samba-dsdb-modules)
(installed vdpau-driver-all)
(installed gnome-user-share)
(installed libkf5screen7)
(installed mime-support)
(installed libfreezethaw-perl)
(installed node-find-cache-dir)
(installed exim4-base)
(installed libkf5filemetadata3)
(installed wamerican)
(installed xauth)
(installed libkcolorpicker0)
(installed libnss-systemd)
(installed libxfont2)
(installed libkf5bluezqt-data)
(installed busybox-static)
(installed node-has-flag)
(installed dvd+rw-tools)
(installed node-copy-concurrently)
(installed efibootmgr)
(installed libclang-cpp14)
(installed libmd4c0)
(installed libksysguardsystemstats1)
(installed libwww-robotrules-perl)
(installed git-man)
(installed isc-dhcp-common)
(installed python3-bs4)
(installed qml-module-org-kde-kcoreaddons)
(installed libintl-xs-perl)
(installed latexmk)
(installed fonts-dejavu-core)
(installed libmro-compat-perl)
(installed iio-sensor-proxy)
(installed node-find-up)
(installed node-flatted)
(installed node-require-directory)
(installed perl-openssl-defaults)
(installed python3-jsonschema)
(installed libkf5solid5-data)
(installed opensp)
(installed coinor-libclp1)
(installed gvfs-backends)
(installed node-convert-source-map)
(installed node-imurmurhash)
(installed libpolkit-qt5-1-1)
(installed liblapack3)
(installed gcc)
(installed libsm6)
(installed firmware-ivtv)
(installed groff-base)
(installed libfastjson4)
(installed pixz)
(installed qml-module-qtquick-templates2)
(installed tar)
(installed gsettings-desktop-schemas)
(installed qml-module-qtqml)
(installed libev-perl)
(installed node-istanbul)
(installed libkf5mbox5)
(installed dvisvgm)
(installed libqt5sensors5)
(installed libxcb-xv0)
(installed libkf5krosscore5)
(installed kwayland-integration)
(installed libwpd-0.10-10)
(installed libodfgen-0.1-1)
(installed node-mime-types)
(installed libregexp-ipv6-perl)
(installed libavcodec59)
(installed atmel-firmware)
(installed libcups2)
(installed x11-xserver-utils)
(installed libext2fs2)
(installed i3lock)
(installed libtss2-sys1)
(installed liblsan0)
(installed libwacom9)
(installed libkpimimportwizard5)
(installed libreoffice-style-colibre)
(installed libexpat1-dev)
(installed libmath-base85-perl)
(installed libpangocairo-1.0-0)
(installed libss2)
(installed liblangtag1)
(installed libgexiv2-2)
(installed default-jre)
(installed libsdl-image1.2)
(installed ruby-erubi)
(installed qml-module-org-kde-kcmutils)
(installed libclass-c3-xs-perl)
(installed libmbim-glib4)
(installed distro-info-data)
(installed pulseaudio-module-gsettings)
(installed libkf5baloowidgets5)
(installed libkf5ldap5abi1)
(installed libhtml-tagset-perl)
(installed python3-dateutil)
(installed libkdsoap1)
(installed python3-gitlab)
(installed node-foreground-child)
(installed libglapi-mesa)
(installed libjs-events)
(installed podman)
(installed libntfs-3g89)
(installed libpam-runtime)
(installed texlive)
(installed firmware-intelwimax)
(installed node-file-entry-cache)
(installed gpg)
(installed ruby-sdbm)
(installed default-mysql-client-core)
(installed node-iferr)
(installed libmtdev1)
(installed bind9-host)
(installed coinor-libcgl-dev)
(installed libsensors-config)
(installed libxml-xpathengine-perl)
(installed vlc-data)
(installed libqt5multimediaquick5)
(installed libasan8)
(installed libgphoto2-l10n)
(installed libjs-util)
(installed cups-browsed)
(installed libgtk-3-0)
(installed libpcre16-3)
(installed libsoap-lite-perl)
(installed libftdi1-2)
(installed libqt5keychain1)
(installed qml-module-org-kde-sonnet)
(installed libllvm15)
(installed firmware-iwlwifi)
(installed quilt)
(installed libbpf1)
(installed libdconf1)
(installed libkf5itemviews-data)
(installed libnuma-dev)
(installed pristine-tar)
(installed libsodium23)
(installed libpam-modules)
(installed libvterm0)
(installed coinor-libcbc3)
(installed libperlio-utf8-strict-perl)
(installed libglx-mesa0)
(installed libkf5wallet-data)
(installed node-ampproject-remapping)
(installed libxvmc1)
(installed qml-module-org-kde-qqc2desktopstyle)
(installed libarchive13)
(installed libboost-filesystem1.74.0)
(installed gnome-bluetooth-sendto)
(installed libfluidsynth3)
(installed pkgconf)
(installed libpython3.11-dev)
(installed qtspeech5-speechd-plugin)
(installed apt-config-icons-hidpi)
(installed libmail-sendmail-perl)
(installed node-columnify)
(installed par2)
(installed rpcbind)
(installed libdynaloader-functions-perl)
(installed node-typedarray-to-buffer)
(installed strace)
(installed fontconfig)
(installed acl)
(installed libntirpc4.0)
(installed libsoup2.4-1)
(installed libvpx7)
(installed libssl3)
(installed node-shell-quote)
(installed node-json-parse-better-errors)
(installed g++)
(installed packagekit-tools)
(installed gvfs-libs)
(installed wodim)
(installed qml-module-org-kde-runnermodel)
(installed libspdlog1.10)
(installed libkf5gravatar5abi2)
(installed node-util)
(installed libvte-2.91-0)
(installed libxxhash0)
(installed libkf5wallet-bin)
(installed libbabl-0.1-0)
(installed libgirara-gtk3-3)
(installed vim-common)
(installed packagekit)
(installed libev4)
(installed libipc-system-simple-perl)
(installed ibus-data)
(installed libfftw3-single3)
(installed bsdextrautils)
(installed libdevel-lexalias-perl)
(installed node-jsonparse)
(installed libdate-calc-perl)
(installed libpipewire-0.3-common)
(installed libxvidcore4)
(installed systemd)
(installed liblangtag-common)
(installed libgdk-pixbuf-2.0-0)
(installed keditbookmarks)
(installed akonadi-mime-data)
(installed dconf-gsettings-backend)
(installed libatspi2.0-0)
(installed ruby)
(installed libwayland-server0)
(installed w3m-img)
(installed libmpich12)
(installed node-to-fast-properties)
(installed node-unicode-canonical-property-names-ecmascript)
(installed mysql-common)
(installed libfcgi-perl)
(installed docker)
(installed libkf5unitconversion5)
(installed libdaemon0)
(installed librados2)
(installed mariadb-common)
(installed libxcb-util1)
(installed vim)
(installed p11-kit)
(installed bogofilter-bdb)
(installed libgphoto2-6)
(installed node-typedarray)
(installed libv4lconvert0)
(installed node-glob)
(installed libqt5sql5)
(installed grub-common)
(installed node-postcss)
(installed libqt5qmlmodels5)
(installed ure)
(installed slirp4netns)
(installed libsuitesparseconfig5)
(installed libcontextual-return-perl)
(installed node-ms)
(installed libqt5webenginewidgets5)
(installed libcurl3-gnutls)
(installed libmpeg2-4)
(installed plasma-integration)
(installed libisl23)
(installed libgegl-common)
(installed libcgi-pm-perl)
(installed ncurses-term)
(installed r-cran-rpart)
(installed libtinfo6)
(installed dbus-daemon)
(installed python3-launchpadlib)
(installed python3-jwt)
(installed liblerc4)
(installed libproxy1v5)
(installed python3-idna)
(installed init)
(installed node-type-check)
(installed libnspr4)
(installed python3-filelock)
(installed libmagickcore-6.q16-6-extra)
(installed libmnl0)
(installed piuparts-common)
(installed libmysofa1)
(installed libsidplay2)
(installed node-neo-async)
(installed node-union-value)
(installed libdolphinvcs5)
(installed python3-fuse)
(installed libsnapd-glib-2-1)
(installed opensc)
(installed libpulse-mainloop-glib0)
(installed libcairo-script-interpreter2)
(installed libibverbs1)
(installed node-slash)
(installed libmusicbrainz5cc2v5)
(installed libpython3.10-stdlib)
(installed python3-jaraco.classes)
(installed libkf5threadweaver5)
(installed libxkbregistry0)
(installed libxcb-image0)
(installed libkf5mime-data)
(installed cron)
(installed libgcr-base-3-1)
(installed libdecor-0-plugin-1-cairo)
(installed xfonts-75dpi)
(installed mmdebstrap)
(installed pkexec)
(installed libkf5webengineviewer5abi1)
(installed diffstat)
(installed libsord-0-0)
(installed node-color-name)
(installed libtag1v5)
(installed libkf5konq6)
(installed libsbc1)
(installed node-text-table)
(installed libaprutil1-ldap)
(installed node-brace-expansion)
(installed media-player-info)
(installed node-colors)
(installed libapparmor1)
(installed libseccomp2)
(installed node-json-schema)
(installed kactivities-bin)
(installed libsasl2-modules-kdexoauth2)
(installed xserver-xorg-video-nouveau)
(installed libkf5globalaccelprivate5)
(installed libfontconfig1)
(installed libucx0)
(installed libzzip-0-13)
(installed genisoimage)
(installed libguard-perl)
(installed libpangoft2-1.0-0)
(installed libgfortran-12-dev)
(installed libkf5newstuff-data)
(installed ubuntu-dev-tools)
(installed kde-style-oxygen-qt5)
(installed libnfs13)
(installed libmfx1)
(installed python3-dbus)
(installed node-to-regex-range)
(installed poppler-data)
(installed libgitlab-api-v4-perl)
(installed libkf5sonnet5-data)
(installed libqt5quicktemplates2-5)
(installed libncursesw6)
(installed node-defined)
(installed node-spdx-correct)
(installed libkf5authcore5)
(installed libjpeg-dev)
(installed libtiff6)
(installed dolphin)
(installed liblog-log4perl-perl)
(installed libxs-parse-sublike-perl)
(installed mupdf-tools)
(installed libxstring-perl)
(installed python3-secretstorage)
(installed libarchive-cpio-perl)
(installed vlc-plugin-video-output)
(installed libdvdnav4)
(installed hicolor-icon-theme)
(installed konqueror)
(installed node-regenerate)
(installed libebml5)
(installed libsub-install-perl)
(installed wbrazilian)
(installed gpg-wks-server)
(installed libqmobipocket2)
(installed gir1.2-malcontent-0)
(installed libkf5syntaxhighlighting-data)
(installed libc6)
(installed node-postcss-value-parser)
(installed sensible-utils)
(installed cdrdao)
(installed libsratom-0-0)
(installed node-normalize-path)
(installed libcdio-paranoia2)
(installed libapr1)
(installed pocketsphinx-en-us)
(installed liborc-0.4-0)
(installed node-graceful-fs)
(installed pci.ids)
(installed bluedevil)
(installed kinfocenter)
(installed acpi)
(installed kross)
(installed libdbusmenu-glib4)
(installed libfile-homedir-perl)
(installed libnfnetlink0)
(installed qt5-gtk-platformtheme)
(installed xclip)
(installed libgtk-4-1)
(installed python3-docutils)
(installed libdebconfclient0)
(installed software-properties-common)
(installed kitty)
(installed libglib-perl)
(installed llvm-14-dev)
(installed node-mkdirp)
(installed realmd)
(installed cron-daemon-common)
(installed kwin-x11)
(installed debian-faq)
(installed cowdancer)
(installed libva-drm2)
(installed libkwineffects14)
(installed libpopt0)
(installed kmail)
(installed node-esquery)
(installed sane-airscan)
(installed firmware-linux-free)
(installed libxcvt0)
(installed fonts-noto-hinted)
(installed libkf5kdelibs4support-data)
(installed libc-ares2)
(installed libqt5quick5)
(installed libkf5sonnetui5)
(installed texlive-latex-extra)
(installed libfile-dirlist-perl)
(installed libudfread0)
(installed libzmq5)
(installed libminizip1)
(installed kde-spectacle)
(installed libqt5svg5)
(installed libsasl2-2)
(installed r-base-dev)
(installed texlive-plain-generic)
(installed liblognorm5)
(installed cryptsetup-bin)
(installed libcarp-clan-perl)
(installed xserver-xorg)
(installed libkf5khtml-bin)
(installed libexif12)
(installed node-rimraf)
(installed node-npm-package-arg)
(installed libxtables12)
(installed libzip4)
(installed bzr)
(installed qml-module-org-kde-kwindowsystem)
(installed firmware-linux-nonfree)
(installed libqca-qt5-2-plugins)
(installed libgl1)
(installed node-prelude-ls)
(installed libspatialaudio0)
(installed node-isarray)
(installed x11-common)
(installed discover)
(installed libole-storage-lite-perl)
(installed pigz)
(installed libavif15)
(installed ruby-tty-which)
(installed netpbm)
(installed qml-module-qtqml-models2)
(installed libcjson1)
(installed libkf5xmlgui-data)
(installed fonts-texgyre-math)
(installed libkf5newstuff5)
(installed libijs-0.35)
(installed libc6-dev)
(installed libkf5pty-data)
(installed node-levn)
(installed python3.10)
(installed libkf5archive5)
(installed pinentry-qt)
(installed python3-cffi-backend)
(installed zathura)
(installed libstoken1)
(installed libsamplerate0)
(installed gnome-keyring)
(installed libcolord2)
(installed libpostproc56)
(installed libjs-inherits)
(installed libkf5khtml-data)
(installed libkf5package-data)
(installed node-defaults)
(installed libkolabxml1v5)
(installed gfortran-12)
(installed dracut-core)
(installed libkf5akonadicontact5)
(installed python3-pynvim)
(installed binutils)
(installed libspecio-perl)
(installed r-cran-nlme)
(installed wget)
(installed khotkeys)
(installed grep)
(installed libnewt0.52)
(installed libboost-locale1.74.0)
(installed libupower-glib3)
(installed node-delegates)
(installed kio-extras-data)
(installed libunistring2)
(installed python3-typing-extensions)
(installed libswscale6)
(installed libksysguardsensorfaces1)
(installed xsettingsd)
(installed firmware-qcom-soc)
(installed libkf5mimetreeparser5abi1)
(installed python3.11-dev)
(installed pim-sieve-editor)
(installed libkf5kcmutils-bin)
(installed krb5-locales)
(installed libthai0)
(installed bzip2)
(installed libsigc++-2.0-0v5)
(installed libxcb-dri3-0)
(installed libjcat1)
(installed libcurl4)
(installed python3-msgpack)
(installed libjemalloc2)
(installed libkf5kontactinterface5)
(installed librest-1.0-0)
(installed ncurses-bin)
(installed liblist-someutils-xs-perl)
(installed python3-gi)
(installed libeot0)
(installed cups-bsd)
(installed node-regenerator-runtime)
(installed libaribb24-0)
(installed node-lodash)
(installed fonts-urw-base35)
(installed libblockdev-swap2)
(installed virtualenv)
(installed python3-urllib3)
(installed node-pascalcase)
(installed node-js-tokens)
(installed libvdpau-va-gl1)
(installed liburcu8)
(installed node-es-abstract)
(installed python3-pcre)
(installed libcdio-cdda2)
(installed libwebpmux3)
(installed libwpe-1.0-1)
(installed modemmanager)
(installed libyaml-libyaml-perl)
(installed xdg-desktop-portal)
(installed ssl-cert)
(installed libunibilium4)
(installed libkf5grantleetheme-plugins)
(installed emacsen-common)
(installed r-cran-mgcv)
(installed node-tap)
(installed libpcre2-16-0)
(installed libqt5multimediawidgets5)
(installed node-wcwidth.js)
(installed libdpkg-perl)
(installed podman-compose)
(installed node-caniuse-lite)
(installed libucx-dev)
(installed libgnome-bg-4-2)
(installed libio-stringy-perl)
(installed node-fs.realpath)
(installed libblockdev-part-err2)
(installed libgoa-1.0-common)
(installed bogofilter-common)
(installed liblayershellqtinterface5)
(installed libmime-lite-perl)
(installed qml-module-org-kde-kitemmodels)
(installed task-brazilian-portuguese)
(installed libkf5libkleo5)
(installed ifupdown)
(installed network-manager-gnome)
(installed libkf5libkdepim-data)
(installed libavahi-core7)
(installed libnet-smtp-ssl-perl)
(installed mobile-broadband-provider-info)
(installed node-kind-of)
(installed ruby-tilt)
(installed neovim)
(installed python3-pyrsistent)
(installed node-readdirp)
(installed qml-module-qtgraphicaleffects)
(installed libhogweed6)
(installed ruby-nanoc-deploying)
(installed libkf5imap5)
(installed libndctl6)
(installed suckless-tools)
(installed gir1.2-glib-2.0)
(installed libhyphen0)
(installed libpwquality1)
(installed libmldbm-perl)
(installed less)
(installed libanyevent-perl)
(installed webp-pixbuf-loader)
(installed libmplex2-2.1-0)
(installed libavahi-client3)
(installed sed)
(installed libuv1)
(installed libuv1-dev)
(installed node-ieee754)
(installed fwupd-amd64-signed)
(installed fontconfig-config)
(installed bash-completion)
(installed libcamd2)
(installed libio-interactive-perl)
(installed libjson-c5)
(installed libgweather-4-0)
(installed node-js-yaml)
(installed python3-rfc3987)
(installed node-progress)
(installed libkf5ksieveui5)
(installed node-cli-truncate)
(installed gir1.2-packagekitglib-1.0)
(installed libkf5newstuffwidgets5)
(installed khotkeys-data)
(installed libmad0)
(installed libmhash2)
(installed libsqlite3-0)
(installed libvdpau1)
(installed manpages-pt-br)
(installed libkf5kiogui5)
(installed libjim0.81)
(installed libfido2-1)
(installed util-linux-locales)
(installed node-loader-runner)
(installed libossp-uuid-perl)
(installed libappstream4)
(installed node-optimist)
(installed libgav1-1)
(installed libkf5kiocore5)
(installed node-err-code)
(installed liborcus-0.17-0)
(installed libjson-glib-1.0-0)
(installed libxmu6)
(installed node-function-bind)
(installed po-debconf)
(installed libinstpatch-1.0-2)
(installed libitm1)
(installed libneon27-gnutls)
(installed liblocale-gettext-perl)
(installed libruby)
(installed libgtk-3-common)
(installed sudo)
(installed libgs-common)
(installed zlib1g-dev)
(installed libkrb5support0)
(installed libmagic-mgc)
(installed libfile-chdir-perl)
(installed node-shebang-command)
(installed nanoc)
(installed libwant-perl)
(installed accountsservice)
(installed intltool-debian)
(installed x11-utils)
(installed node-unicode-match-property-ecmascript)
(installed libhttp-tiny-multipart-perl)
(installed libqt5sql5-sqlite)
(installed libieee1284-3)
(installed liblz4-1)
(installed node-data-uri-to-buffer)
(installed lxc)
(installed libsocket6-perl)
(installed libqt5waylandcompositor5)
(installed libwebrtc-audio-processing1)
(installed libsbuild-perl)
(installed login)
(installed python3-uno)
(installed logsave)
(installed bsdutils)
(installed dmraid)
(installed gimp-data)
(installed xdg-utils)
(installed node-ansi-styles)
(installed libreoffice-common)
(installed r-doc-html)
(installed gettext-base)
(installed node-globals)
(installed libheif1)
(installed libpotrace0)
(installed node-eslint-scope)
(installed libcrypt-rc4-perl)
(installed gvfs-daemons)
(installed libkpimitinerary-data)
(installed libxml-parser-perl)
(installed libreoffice-base-core)
(installed node-es-module-lexer)
(installed libusb-1.0-0)
(installed scrot)
(installed kde-config-sddm)
(installed node-slice-ansi)
(installed liborcus-parser-0.17-0)
(installed llvm-14-runtime)
(installed node-balanced-match)
(installed bolt)
(installed libtime-duration-perl)
(installed firmware-ast)
(installed libqt5quickwidgets5)
(installed ruby-slim)
(installed libfeature-compat-class-perl)
(installed liberror-perl)
(installed libc-bin)
(installed node-through)
(installed xserver-xorg-video-vesa)
(installed libsensors5)
(installed python3-soupsieve)
(installed libkf5dav5)
(installed node-uuid)
(installed pinentry-gnome3)
(installed golang-src)
(installed libnetpbm11)
(installed libsereal-decoder-perl)
(installed node-is-glob)
(installed libgvpr2)
(installed node-eslint-utils)
(installed librhash0)
(installed libjcode-pm-perl)
(installed node-esutils)
(installed node-mime)
(installed libgtop2-common)
(installed dahdi-firmware-nonfree)
(installed python3-tdb)
(installed qttranslations5-l10n)
(installed wireless-regdb)
(installed libopenconnect5)
(installed alsa-utils)
(installed libopus0)
(installed libgoa-1.0-0b)
(installed libhttp-message-perl)
(installed python3-requests-toolbelt)
(installed qml-module-org-kde-quickcharts)
(installed x11-xkb-utils)
(installed buildah)
(installed libgmp-dev)
(installed libnet-netmask-perl)
(installed libvisual-0.4-0)
(installed sgml-base)
(installed xserver-xorg-input-wacom)
(installed libzstd1)
(installed libjson-perl)
(installed nano)
(installed libkf5baloo5)
(installed libencode-locale-perl)
(installed node-path-type)
(installed libkf5jobwidgets-data)
(installed node-semver)
(installed r-recommended)
(installed python3-github)
(installed libdebconf-kde1)
(installed node-rechoir)
(installed libfishcamp1)
(installed bluetooth)
(installed ruby-kramdown)
(installed zenity-common)
(installed rubygems-integration)
(installed smartmontools)
(installed libwps-0.4-4)
(installed plzip)
(installed libcanberra0)
(installed apt-utils)
(installed man-db)
(installed layer-shell-qt)
(installed libdate-manip-perl)
(installed node-debug)
(installed node-jest-worker)
(installed libdaxctl1)
(installed libcolorcorrect5)
(installed at-spi2-core)
(installed libshine3)
(installed im-config)
(installed libphonon4qt5-data)
(installed libhfstospell11)
(installed libkpimgapi-data)
(installed lsof)
(installed kate5-data)
(installed wmdocker)
(installed libuno-cppuhelpergcc3-3)
(installed libpam-cgfs)
(installed ruby-pastel)
(installed gettext)
(installed libnet-ipv6addr-perl)
(installed libkf5akonadi-data)
(installed webpack)
(installed kwalletmanager)
(installed libkf5calendarutils5)
(installed python3-blinker)
(installed thin-provisioning-tools)
(installed ucf)
(installed libmoox-aliases-perl)
(installed pciutils)
(installed node-minimist)
(installed os-prober)
(installed kaccounts-providers)
(installed libqt5qmlworkerscript5)
(installed libkf5pty5)
(installed xserver-xorg-core)
(installed libefivar1)
(installed libbit-vector-perl)
(installed k3b-data)
(installed khelpcenter)
(installed libreadline-dev)
(= (total-cost) 0))
(:goal (and (installed 0ad-data-common)))
(:metric minimize (total-cost)))

(define (problem install-debian-packages)
(:domain dependencies-manager)
(:objects package1 package2 package3 package4 package5
          package6 package7 package8 package9 package10
          package11 package12 package13 package14 package15
          package16 package17 package18 package19 package20
          package21 package22 package23 package24 package25
          package26 package27 package28 package29 package30
          package31 package32 package33 package34 package35
          package36 package37 package38 package39 package40
          package41 package42 package43 package44 package45
          package46 package47 package48 package49 package50
          package51 package52 package53 package54 package55
          package56 package57 package58 package59 package60
          package61 package62 package63 package64 package65
          package66 package67 package68 package69 package70
          package71 package72 package73 package74 package75
          package76 package77 package78 package79 package80
          package81 package82 package83 package84 package85
          package86 package87 package88 package89 package90
          package91 package92 package93 package94 package95
          package96 package97 package98 package99 package100
          package101 package102 package103 package104 package105
          package106 package107 package108)
(:init (package package1) (package package2) (package package3)
       (package package4) (package package5) (package package6)
       (package package7) (package package8) (package package9)
       (package package10) (package package11) (package package12)
       (package package13) (package package14) (package package15)
       (package package16) (package package17) (package package18)
       (package package19) (package package20) (package package21)
       (package package22) (package package23) (package package24)
       (package package25) (package package26) (package package27)
       (package package28) (package package29) (package package30)
       (package package31) (package package32) (package package33)
       (package package34) (package package35) (package package36)
       (package package37) (package package38) (package package39)
       (package package40) (package package41) (package package42)
       (package package43) (package package44) (package package45)
       (package package46) (package package47) (package package48)
       (package package49) (package package50) (package package51)
       (package package52) (package package53) (package package54)
       (package package55) (package package56) (package package57)
       (package package58) (package package59) (package package60)
       (package package61) (package package62) (package package63)
       (package package64) (package package65) (package package66)
       (package package67) (package package68) (package package69)
       (package package70) (package package71) (package package72)
       (package package73) (package package74) (package package75)
       (package package76) (package package77) (package package78)
       (package package79) (package package80) (package package81)
       (package package82) (package package83) (package package84)
       (package package85) (package package86) (package package87)
       (package package88) (package package89) (package package90)
       (package package91) (package package92) (package package93)
       (package package94) (package package95) (package package96)
       (package package97) (package package98) (package package99)
       (package package100) (package package101) (package package102)
       (package package103) (package package104) (package package105)
       (package package106) (package package107) (package package108)
       (depends-on package3 package97)
       (depends-on package56 package100)
       (depends-on package41 package63)
       (depends-on package41 package47)
       (depends-on package5 package21)
       (depends-on package66 package52)
       (depends-on package23 package19)
       (depends-on package33 package42)
       (depends-on package10 package92)
       (depends-on package19 package30)
       (depends-on package55 package57)
       (depends-on package56 package2)
       (depends-on package12 package20)
       (depends-on package24 package50)
       (depends-on package15 package71)
       (depends-on package45 package17)
       (depends-on package59 package8)
       (depends-on package9 package99)
       (depends-on package70 package49)
       (depends-on package54 package91)
       (depends-on package85 package11)
       (depends-on package50 package107)
       (depends-on package45 package98)
       (depends-on package56 package54)
       (depends-on package97 package75)
       (depends-on package99 package44)
       (depends-on package23 package46)
       (depends-on package45 package50)
       (depends-on package81 package84)
       (depends-on package7 package96)
       (depends-on package46 package52)
       (= (total-cost) 0))
(:goal (and (installed package60) (installed package96)
            (installed package100) (installed package75)
            (installed package11) (installed package106)
            (installed package4) (installed package41)
            (installed package81) (installed package76)))
(:metric minimize (total-cost)))

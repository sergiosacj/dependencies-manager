"""
Para gerar novos casos de teste utilizando este script, o seguinte passo a passo deverá ser seguido:
1 - Especificar um ou mais pacotes a serem instalados no arquivo `packages.txt`. Ex.: 0ad-data-common/jammy 0.0.25b-1 all
2 - Especificar todos os pacotes instalados no sistema no arquivo `installed_packages.txt`. Isso pode ser feito com o comando `apt list > installed_packages.txt`
3 - Rodar o script python generator.py
"""

import os

packages = set()
installed_packages = set()
depends_on = {}

with open('packages.txt', 'rb') as f:
  for line in f:
    line = line.decode('ascii')
    if (line == 'Listing...'):
      continue
    line = line.split('/')[0]
    packages.add(line)

    os.system(f'apt-rdepends {line} > dependency-tree.txt')
    last_package = ''

    with open('dependency-tree.txt', 'rb') as dt:
      for l in dt:
        l = l.decode('ascii')
        if(l[0] != ' '):
          last_package = l.replace('\n', '').replace(' ', '')
          depends_on[last_package] = []
        else:
          dependency = l.split('Depends: ')[1].split('(')[0].replace('\n', '').replace(' ', '')
          packages.add(dependency)
          if not dependency in depends_on[last_package]:
            depends_on[last_package].append(dependency)

with open('installed_packages.txt', 'rb') as f:
  for line in f:
    line = line.decode('ascii')
    line = line.split('/')[0]
    packages.add(line)
    installed_packages.add(line)

print('(define (problem install-debian-packages)')
print('(:domain dependencies-manager)')
print('(:objects')

for k in packages:
  print(k)

print(')')
print('(:init')
for k in packages:
  print(f'(package {k})')

for k in depends_on:
  for j in depends_on[k]:
    print(f'(depends-on {k} {j})')

for k in installed_packages:
  print(f'(installed {k})')

print('(= (total-cost) 0))')
print('(:goal (and (installed 0ad-data-common)))')
print('(:metric minimize (total-cost)))')